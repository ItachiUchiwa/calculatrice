package com.c2e.back.entity;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@DiscriminatorValue(value = "ExperienceProfessionnelle")
public class ExperienceProfessionnelle extends Projet implements Serializable {

	private Integer nombreJour;
	private String entrepriseHote;
	private Duree typeDuree;


	public ExperienceProfessionnelle() {
		super();
	}

	public ExperienceProfessionnelle(Builder builder) {
		this.id = builder.id;
		this.intitule = builder.intitule;
		this.description = builder.description;
		this.nombreJour = builder.nombreJour;
		this.entrepriseHote = builder.entrepriseHote;
		this.typeDuree = builder.typeDuree;
	}

	public Integer getNombreJour() {
		return nombreJour;
	}

	public void setNombreJour(Integer nombreJour) {
		this.nombreJour = nombreJour;
	}

	public String getEntrepriseHote() {
		return entrepriseHote;
	}

	public void setEntrepriseHote(String entrepriseHote) {
		this.entrepriseHote = entrepriseHote;
	}
	
	public Duree getTypeDuree() {
		return typeDuree;
	}

	public void setTypeDuree(Duree typeDuree) {
		this.typeDuree = typeDuree;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public static class Builder {
		private Long id;
		private String intitule;
		private String description;
		private Integer nombreJour;
		private String entrepriseHote;
		private Duree typeDuree;

		public Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withIntitule(String intitule) {
			this.intitule = intitule;
			return this;
		}

		public Builder withDescription(String description) {
			this.description = description;
			return this;
		}

		public Builder withNombreJour(Integer nombreJour) {
			this.nombreJour = nombreJour;
			return this;
		}
		
		public Builder withEntrepriseHote(String entrepriseHote) {
			this.entrepriseHote = entrepriseHote;
			return this;
		}
		
		public Builder withTypeDuree(Duree typeDuree) {
			this.typeDuree = typeDuree;
			return this;
		}
		
		public ExperienceProfessionnelle build() {
			return new ExperienceProfessionnelle(this);
		}
	}

}
