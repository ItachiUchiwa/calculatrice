package com.c2e.back.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.c2e.back.entity.Competence.Builder;

@Entity
public class NiveauEtude {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String intitule;

	public NiveauEtude() {
	}

	public NiveauEtude(Builder builder) {
		this.id = builder.id;
		this.intitule = builder.intitule;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public static class Builder {
		private Long id;
		private String intitule;

		public Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withIntitule(String intitule) {
			this.intitule = intitule;
			return this;
		}

		public NiveauEtude build() {
			return new NiveauEtude(this);
		}
	}

}
