package com.c2e.back.entity;

public class ResetPasswordForm {

	private String token;
	private String password;
	private String repassword;
	
	public ResetPasswordForm() {
		// TODO Auto-generated constructor stub
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRepassword() {
		return repassword;
	}

	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}

	@Override
	public String toString() {
		return "ResetPasswordForm [token=" + token + ", password=" + password + ", repassword=" + repassword + "]";
	}
	
	
}
