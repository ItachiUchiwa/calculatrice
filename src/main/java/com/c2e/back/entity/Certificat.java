package com.c2e.back.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
public class Certificat  implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String intitule;
	private LocalDate dateObtention;

	public Certificat() {
	}

	public Certificat(Builder builder) {
		this.id = builder.id;
		this.intitule = builder.intitule;
		this.dateObtention = builder.dateObtention;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public LocalDate getDateObtention() {
		return dateObtention;
	}

	public void setDateObtention(LocalDate dateObtention) {
		this.dateObtention = dateObtention;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public static class Builder {
		private Long id;
		private String intitule;
		private LocalDate dateObtention;

		public Builder() {
			
		}
		
		public Builder withId(Long id) {
			this.id = id;
			return this;
		}
		
		public Builder withIntitule(String intitule) {
			this.intitule = intitule;
			return this;
		}
		
		public Builder withDateObtention(LocalDate dateObtention) {
			this.dateObtention = dateObtention;
			return this;
		}
		
		public Certificat build() {
			return new Certificat(this);
		}
		
	}

}