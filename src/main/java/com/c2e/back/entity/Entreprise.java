package com.c2e.back.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@DiscriminatorValue(value = "ENTREPRISE")
public class Entreprise extends AppUser implements Serializable {

	private String nom;
	@OneToOne
	private Domaine domaine;
	@OneToOne(cascade = CascadeType.REMOVE)
	private Adresse adresse;
	private LocalDate dateCreation;
	private Integer nombreCollaborateur;
	private Float chiffreAffaire;
	private String lienSiteWeb;
	private String email; // A ne pas oublier d'ajouter sur la page entreprise profil
	private String telephone;
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	private Collection<Projet> projets;
	@OneToMany(cascade = CascadeType.REMOVE)
	@OrderBy("datePublication DESC")
	private Collection<Offre> offres;
	@OneToMany(cascade = CascadeType.REMOVE)
	private List<EtapeRecrutement> etapeRecrutements;
	@OneToMany(cascade = CascadeType.REMOVE)
	private Collection<Partenariat> partenariats;
	@OneToMany(cascade = CascadeType.REMOVE)
	private Collection<Manager> managers;
	@ManyToMany
	private Collection<AppUser> followers;
	private String typeRechercheNaturel = "Entreprise";// Ce champ est là juste pour faciliter la recherche et va
														// toujours contenir la chaine "Entreprise"

	public Entreprise() {
	}

	public Entreprise(Builder builder) {
		this.nom = builder.nom;
		this.domaine = builder.domaine;
		this.adresse = builder.adresse;
		this.dateCreation = builder.dateCreation;
		this.nombreCollaborateur = builder.nombreCollaborateur;
		this.chiffreAffaire = builder.chiffreAffaire;
		this.lienSiteWeb = builder.lienSiteWeb;
		this.projets = builder.projets;
		this.offres = builder.offres;
		this.etapeRecrutements = builder.etapeRecrutements;
		this.partenariats = builder.partenariats;
		this.email = builder.email;
		this.telephone = builder.telephone;
		this.managers = builder.managers;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Domaine getDomaine() {
		return domaine;
	}

	public void setDomaine(Domaine domaine) {
		this.domaine = domaine;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public LocalDate getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDate dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Integer getNombreCollaborateur() {
		return nombreCollaborateur;
	}

	public void setNombreCollaborateur(Integer nombreCollaborateur) {
		this.nombreCollaborateur = nombreCollaborateur;
	}

	public Float getChiffreAffaire() {
		return chiffreAffaire;
	}

	public void setChiffreAffaire(Float chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	public String getLienSiteWeb() {
		return lienSiteWeb;
	}

	public void setLienSiteWeb(String lienSiteWeb) {
		this.lienSiteWeb = lienSiteWeb;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
		this.username = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Collection<Projet> getProjets() {
		return projets;
	}

	public void setProjets(Collection<Projet> projets) {
		this.projets = projets;
	}

	public Collection<Offre> getOffres() {
		return offres;
	}

	public void setOffres(Collection<Offre> offres) {
		this.offres = offres;
	}
	
	public List<EtapeRecrutement> getEtapeRecrutement() {
		return etapeRecrutements;
	}

	public void setEtapeRecrutement(List<EtapeRecrutement> etapeRecrutements) {
		this.etapeRecrutements = etapeRecrutements;
	}

	public Collection<Partenariat> getPartenariats() {
		return partenariats;
	}

	public void setPartenariats(Collection<Partenariat> partenariats) {
		this.partenariats = partenariats;
	}

	public Collection<Manager> getManagers() {
		return managers;
	}

	public void setManagers(Collection<Manager> managers) {
		this.managers = managers;
	}
	
	public List<EtapeRecrutement> getEtapeRecrutements() {
		return etapeRecrutements;
	}

	public void setEtapeRecrutements(List<EtapeRecrutement> etapeRecrutements) {
		this.etapeRecrutements = etapeRecrutements;
	}

	public Collection<AppUser> getFollowers() {
		return followers;
	}

	public void setFollowers(Collection<AppUser> followers) {
		this.followers = followers;
	}

	public String getTypeRechercheNaturel() {
		return typeRechercheNaturel;
	}

	public void setTypeRechercheNaturel(String typeRechercheNaturel) {
		this.typeRechercheNaturel = typeRechercheNaturel;
	}


	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public static class Builder {

		private Long id;
		private String nom;
		private Domaine domaine;
		private Adresse adresse;
		private LocalDate dateCreation;
		private Integer nombreCollaborateur;
		private Float chiffreAffaire;
		private String lienSiteWeb;
		private String email;
		private String telephone;
		private Collection<Projet> projets;
		private Collection<Offre> offres;
		private List<EtapeRecrutement> etapeRecrutements;
		private Collection<Partenariat> partenariats;
		private Collection<Manager> managers;

		public Builder() {

		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withNom(String nom) {
			this.nom = nom;
			return this;
		}

		public Builder withDomaine(Domaine domaine) {
			this.domaine = domaine;
			return this;
		}

		public Builder withAdresse(Adresse adresse) {
			this.adresse = adresse;
			return this;
		}

		public Builder withDateCreation(LocalDate dateCreation) {
			this.dateCreation = dateCreation;
			return this;
		}

		public Builder withNombreCollaborateur(Integer nombreCollaborateur) {
			this.nombreCollaborateur = nombreCollaborateur;
			return this;
		}

		public Builder withChiffreAffaire(Float chiffreAffaire) {
			this.chiffreAffaire = chiffreAffaire;
			return this;
		}

		public Builder withLienSiteWeb(String lienSiteWeb) {
			this.lienSiteWeb = lienSiteWeb;
			return this;
		}

		public Builder withEmail(String email) {
			this.email = email;
			return this;
		}

		public Builder withTelephone(String telephone) {
			this.telephone = telephone;
			return this;
		}

		public Builder withProjets(Collection<Projet> projets) {
			this.projets = projets;
			return this;
		}

		public Builder withOffres(Collection<Offre> offres) {
			this.offres = offres;
			return this;
		}

		public Builder withEtapeRecrutement(List<EtapeRecrutement> etapeRecrutements) {
			this.etapeRecrutements = etapeRecrutements;
			return this;
		}

		public Builder withPartenariats(Collection<Partenariat> partenariats) {
			this.partenariats = partenariats;
			return this;
		}

		public Builder withManagers(Collection<Manager> managers) {
			this.managers = managers;
			return this;
		}

		public Entreprise build() {
			return new Entreprise(this);
		}
	}
}