package com.c2e.back.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
public class Ecole implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	private String nomComplet;
	private LocalDate dateCreation;
	private Integer nombreEtudiant;
	@OneToMany
	private Collection<Filiere> filieres;

	public Ecole() {
	}

	public Ecole(Builder builder) {
		this.id = builder.id;
		this.nom = builder.nom;
		this.dateCreation = builder.dateCreation;
		this.nombreEtudiant = builder.nombreEtudiant;
		this.filieres = builder.filieres;
		this.nomComplet = builder.nomComplet;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNomComplet() {
		return nomComplet;
	}

	public void setNomComplet(String nomComplet) {
		this.nomComplet = nomComplet;
	}

	public LocalDate getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDate dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Integer getNombreEtudiant() {
		return nombreEtudiant;
	}

	public void setNombreEtudiant(Integer nombreEtudiant) {
		this.nombreEtudiant = nombreEtudiant;
	}

	public Collection<Filiere> getFilieres() {
		return filieres;
	}

	public void setFilieres(Collection<Filiere> filieres) {
		this.filieres = filieres;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public static class Builder {
		private Long id;
		private String nom;
		private String nomComplet;
		private LocalDate dateCreation;
		private Integer nombreEtudiant;
		private Collection<Filiere> filieres;

		public Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withNom(String nom) {
			this.nom = nom;
			return this;
		}
		
		public Builder withNomComplet(String nomComplet) {
			this.nomComplet = nomComplet;
			return this;
		}

		public Builder withDateCreation(LocalDate dateCreation) {
			this.dateCreation = dateCreation;
			return this;
		}

		public Builder withNombreEtudiant(Integer nombreEtudiant) {
			this.nombreEtudiant = nombreEtudiant;
			return this;
		}
		
		public Builder withFilieres(Collection<Filiere> filieres) {
			this.filieres = filieres;
			return this;
		}
		
		public Ecole build() {
			return new Ecole(this);
		}
	}
}
