package com.c2e.back.entity;

public enum TypeOffre {
	STAGE,
	ALTERNANCE,
	EMPLOI,
	APPEL_OFFRE,
	SPONTANEE
}
