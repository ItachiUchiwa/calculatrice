package com.c2e.back.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;

@Entity
@Inheritance(strategy =InheritanceType.SINGLE_TABLE )
@DiscriminatorColumn(name = "TYPE_USER")
public class AppUser{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;	
	protected String username;
	protected String password;
	private String resetToken;
	private LocalDateTime dateExpiredToken;
	private boolean isEnabled;
	private String registerToken;
	private LocalDateTime registerDate;
	@Column(length = 2000)
	protected String image;
	
    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<AppRole> roles = new ArrayList<>();
    @Column(length = 1200)
	private String idOffresPostulees;
    
    public AppUser() {
		// TODO Auto-generated constructor stub
	}

    public AppUser(String username, String password,Collection<AppRole> roles) {
        this.username = username;
        this.password = password;
        this.roles=roles;
    }
    
    public AppUser(String username, String password) {
        this.username = username;
        this.password = password;
    }


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<AppRole> getRoles() {
		return roles;
	}

	public void setRoles(Collection<AppRole> role) {
		this.roles = role;
	}

	public String getResetToken() {
		return resetToken;
	}

	public void setResetToken(String resetToken) {
		this.resetToken = resetToken;
	}

	public LocalDateTime getDateExpiredToken() {
		return dateExpiredToken;
	}

	public void setDateExpiredToken(LocalDateTime dateExpiredToken) {
		this.dateExpiredToken = dateExpiredToken;
	}

	public boolean isEnabled() {
		return isEnabled;
	}

	public void setEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	public String getRegisterToken() {
		return registerToken;
	}

	public void setRegisterToken(String registerToken) {
		this.registerToken = registerToken;
	}

	public LocalDateTime getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(LocalDateTime registerDate) {
		this.registerDate = registerDate;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getIdOffresPostulees() { 
		return idOffresPostulees;
	}

	public void setIdOffresPostulees(String idOffresPostulees) {
		this.idOffresPostulees = idOffresPostulees;
	}

	@Override
	public String toString() {
		return "AppUser [id=" + id + ", username=" + username + ", password=" + password + ", resetToken=" + resetToken
				+ ", dateExpiredToken=" + dateExpiredToken + ", isEnabled=" + isEnabled + ", registerToken="
				+ registerToken + ", registerDate=" + registerDate + ", image=" + image + ", roles=" + roles
				+ ", idOffresPostulees=" + idOffresPostulees + "]";
	}
	
}