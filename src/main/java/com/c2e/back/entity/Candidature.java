package com.c2e.back.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type_candidature")
public class Candidature implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;
	protected LocalDateTime dateCandidature;
	@OneToOne
	protected EtapeRecrutement etapeRecrutement;
	@OneToOne
	protected Etudiant etudiant;
	protected Long idManager;
	protected Long idEntreprise;

	protected String intituleOffre;

	protected String nomEntreprise;

	protected String imageEntreprise;
	@OneToMany
	protected Collection<Commentaire> commentaires;
	
	protected Integer point;
	
	protected Long idEntrepriseHote;
	
	protected TypeOffre typeOffre;

	public Candidature() {
	}

	public Candidature(Builder builder) {
		this.id = builder.id;
		this.dateCandidature = builder.dateCandidature;
		this.etapeRecrutement = builder.etapeRecrutement;
		this.etudiant = builder.etudiant;
		this.idEntreprise = builder.idEntreprise;
		this.intituleOffre = builder.intituleOffre;
		this.nomEntreprise = builder.nomEntreprise;
		this.imageEntreprise = builder.imageEntreprise;
		this.commentaires = builder.commentaires;
		this.point = builder.point;
		this.idEntrepriseHote = builder.idEntrepriseHote;
		this.typeOffre = builder.typeOffre;
		this.idManager = builder.idManager;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDateCandidature() {
		return dateCandidature;
	}

	public void setDateCandidature(LocalDateTime dateCandidature) {
		this.dateCandidature = dateCandidature;
	}

	public EtapeRecrutement getEtatCandidature() {
		return etapeRecrutement;
	}

	public void setEtatCandidature(EtapeRecrutement etapeRecrutement) {
		this.etapeRecrutement = etapeRecrutement;
	}

	public Etudiant getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}

	public Long getIdEntreprise() {
		return idEntreprise;
	}

	public void setIdEntreprise(Long idEntreprise) {
		this.idEntreprise = idEntreprise;
	}
	
	

	public EtapeRecrutement getEtapeRecrutement() {
		return etapeRecrutement;
	}

	public void setEtapeRecrutement(EtapeRecrutement etapeRecrutement) {
		this.etapeRecrutement = etapeRecrutement;
	}

	public String getIntituleOffre() {
		return intituleOffre;
	}

	public void setIntituleOffre(String intituleOffre) {
		this.intituleOffre = intituleOffre;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	public String getImageEntreprise() {
		return imageEntreprise;
	}

	public void setImageEntreprise(String imageEntreprise) {
		this.imageEntreprise = imageEntreprise;
	}

	public Collection<Commentaire> getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(Collection<Commentaire> commentaires) {
		this.commentaires = commentaires;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

	public Long getIdEntrepriseHote() {
		return idEntrepriseHote;
	}

	public void setIdEntrepriseHote(Long idEntrepriseHote) {
		this.idEntrepriseHote = idEntrepriseHote;
	}

	public TypeOffre getTypeOffre() {
		return typeOffre;
	}

	public void setTypeOffre(TypeOffre typeOffre) {
		this.typeOffre = typeOffre;
	}

	public Long getIdManager() {
		return idManager;
	}

	public void setIdManager(Long idManager) {
		this.idManager = idManager;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public static class Builder {
		private Long id;
		private LocalDateTime dateCandidature;
		private EtapeRecrutement etapeRecrutement;
		private Etudiant etudiant;
		private Long idEntreprise;
		private String intituleOffre;
		private String nomEntreprise;
		private String imageEntreprise;
		private Collection<Commentaire> commentaires;
		private Integer point;
		private Long idEntrepriseHote;
		private TypeOffre typeOffre;
		private Long idManager;
		
		public Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withDateCandidature(LocalDateTime dateCandidature) {
			this.dateCandidature = dateCandidature;
			return this;
		}

		public Builder withEtat(EtapeRecrutement etapeRecrutement) {
			this.etapeRecrutement = etapeRecrutement;
			return this;
		}

		public Builder withEtudiant(Etudiant etudiant) {
			this.etudiant = etudiant;
			return this;
		}

		public Builder withIntituleOffre(String intituleOffre) {
			this.intituleOffre = intituleOffre;
			return this;
		}
		
		public Builder withIdEntreprise(Long idEntreprise) {
			this.idEntreprise = idEntreprise;
			return this;
		}

		public Builder withNomEntreprise(String nomEntreprise) {
			this.nomEntreprise = nomEntreprise;
			return this;
		}

		public Builder withImageEntreprise(String imageEntreprise) {
			this.imageEntreprise = imageEntreprise;
			return this;
		}
		
		public Builder withCommentaires(Collection<Commentaire> commentaires) {
			this.commentaires = commentaires;
			return this;
		}
		
		public Builder withPoint(Integer point) {
			this.point = point;
			return this;
		}
		
		public Builder withIdEntrepriseHote(Long idEntrepriseHote) {
			this.idEntrepriseHote = idEntrepriseHote;
			return this;
		}
		
		public Builder withTypeOffre(TypeOffre typeOffre) {
			this.typeOffre = typeOffre;
			return this;
		}
		
		public Builder withIdManager(Long idManager) {
			this.idManager = idManager;
			return this;
		}
		
		public Candidature build() {
			return new Candidature(this);
		}
	}
}