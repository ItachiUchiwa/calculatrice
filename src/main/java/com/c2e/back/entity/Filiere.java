package com.c2e.back.entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
public class Filiere implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String intitule;
	@OneToMany
	private Collection<Etudiant> etudiants;

	public Filiere() {
		super();
	}

	public Filiere(Builder builder) {
		this.id = builder.id;
		this.intitule = builder.intitule;
		this.etudiants = builder.etudiants;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public Collection<Etudiant> getEtudiants() {
		return etudiants;
	}

	public void setEtudiants(Collection<Etudiant> etudiants) {
		this.etudiants = etudiants;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	public static class Builder{
		private Long id;
		private String intitule;
		private Collection<Etudiant> etudiants;
		
		public Builder() {
		}
		
		public Builder withId(Long id) {
			this.id = id;
			return this;
		}
		
		public Builder withIntitule(String intitule) {
			this.intitule = intitule;;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
			return this;
		}
		
		public Builder withEtudiant(Collection<Etudiant> etudiants) {
			this.etudiants = etudiants;
			return this;
		}
		
		public Filiere build() {
			return new Filiere(this);
		}
	}
}
