package com.c2e.back.entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue(value = "MANAGER")
public class Manager extends AppUser implements Serializable {

	private String nom;
	private String prenom;
	private Long idEntreprise;
	@OneToMany
	private Collection<Candidature> candidatures;
	@OneToMany
	private Collection<Etudiant> favoris;

	public Manager() {
		// TODO Auto-generated constructor stub
	}

	public Manager(String nom, String prenom, Long idEntreprise) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.idEntreprise = idEntreprise;
	}

	public Collection<Candidature> getCandidatures() {
		return candidatures;
	}

	public void setCandidatures(Collection<Candidature> candidatures) {
		this.candidatures = candidatures;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Long getIdEntreprise() {
		return idEntreprise;
	}

	public void setIdEntreprise(Long idEntreprise) {
		this.idEntreprise = idEntreprise;
	}

	public Collection<Etudiant> getFavoris() {
		return favoris;
	}

	public void setFavoris(Collection<Etudiant> favoris) {
		this.favoris = favoris;
	}


	@Override
	public String toString() {
		return "Manager [nom=" + nom + ", prenom=" + prenom + ", candidatures=" + candidatures + "]";
	}

}
