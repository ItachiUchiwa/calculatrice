package com.c2e.back.entity;

public enum Duree {

	JOUR,
	MOIS,
	ANNEE,
}
