package com.c2e.back.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
public class Partenariat implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@OneToOne
	private Entreprise entrepriseExpediteur;
	private TypePartenariat typePartenariat;
	private String objectif;
	@Column(length = 1000)
	private String description;

	public Partenariat() {
		super();
	}

	public Partenariat(Builder builder) {
		this.id = builder.id;
		this.entrepriseExpediteur = builder.entrepriseExpediteur;
		this.typePartenariat = builder.typePartenariat;
		this.objectif = builder.objectif;
		this.description = builder.description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Entreprise getEntrepriseExpediteur() {
		return entrepriseExpediteur;
	}

	public void setEntrepriseExpediteur(Entreprise entrepriseExpediteur) {
		this.entrepriseExpediteur = entrepriseExpediteur;
	}

	public String getObjectif() {
		return objectif;
	}

	public void setObjectif(String objectif) {
		this.objectif = objectif;
	}

	public TypePartenariat getTypePartenariat() {
		return typePartenariat;
	}

	public void setTypePartenariat(TypePartenariat typePartenariat) {
		this.typePartenariat = typePartenariat;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public static class Builder {
		private Long id;
		private Entreprise entrepriseExpediteur;
		private TypePartenariat typePartenariat;
		private String objectif;
		private String description;
		
		public Builder() {
		
		}
		
		public Builder withId(Long id) {
			this.id = id;
			return this;
		}
		
		public Builder withEntrepriseExpediteur(Entreprise entreprise) {
			this.entrepriseExpediteur = entreprise;
			return this;
		}
		
		public Builder withTypePartenariat(TypePartenariat typePartenariat) {
			this.typePartenariat = typePartenariat;
			return this;
		}
		
		public Builder withObjectif(String objectif) {
			this.objectif = objectif;
			return this;
		}
		
		public Builder withDescription(String description) {
			this.description = description;
			return this;
		}
		
		public Partenariat build() {
			return new Partenariat(this);
		}
	}
}
