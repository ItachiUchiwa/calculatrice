package com.c2e.back.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Inheritance(strategy =InheritanceType.SINGLE_TABLE )
@DiscriminatorColumn(name = "TYPE_PROJET")
public class Projet implements Serializable {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
	@Column(length = 10000)
	protected String intitule;
	@Column(length = 1000000)
	protected String description;
	
	public Projet() {
	}

	public Projet(Builder builder) {
		this.id = builder.id;
		this.intitule = builder.intitule;
		this.description = builder.description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
	
	public static class Builder{
		private Long id;
		private String intitule;
		private String description;
		
		public Builder() {
		}
		
		public Builder withId(Long id) {
			this.id = id;
			return this;
		}
		
		public Builder withIntitule(String intitule) {
			this.intitule = intitule;
			return this;
		}
		
		public Builder withDescription(String description) {
			this.description = description;
			return this;
		}
		
		public Projet build() {
			return new Projet(this);
		}
	}
}
