package com.c2e.back.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
public class Adresse implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String pays;
	private String ville;
	private String adresse;

	public Adresse() {
		super();
	}

	public Adresse(Builder builder) {
		this.id =  builder.id;
		this.pays =  builder.pays;
		this.ville =  builder.ville;
		this.adresse =  builder.adresse;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
	@Override
	public boolean equals(Object obj) {
		 if (this == obj)
	            return true;
	        if (obj == null)
	            return false;
	        if (getClass() != obj.getClass())
	            return false;
	        Adresse other = (Adresse) obj;

	        return new EqualsBuilder().append(pays, other.pays)
	                .append(ville, other.ville)
	                .append(adresse, other.adresse)
	                .isEquals();

	}
	
	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

	public static class Builder {
		private Long id;
		private String pays;
		private String ville;
		private String adresse;
		
		public Builder() {
		}
		
		public Builder withId(Long id) {
			this.id = id;
			return this;
		}
		
		public Builder withPays(String pays) {
			this.pays = pays;
			return this;
		}
		
		public Builder withVille(String ville) {
			this.ville = ville;
			return this;
		}
		
		public Builder withAdresse(String adresse) {
			this.adresse = adresse;
			return this;
		}
		
		public Adresse build() {
			return new Adresse(this);
		}
	}
}
