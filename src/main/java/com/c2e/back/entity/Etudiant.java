package com.c2e.back.entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@DiscriminatorValue(value = "ETUDIANT")
public class Etudiant  extends AppUser implements Serializable {
	
	private String nom;
	private String prenom;
	private String email;
	private String telephone ;
	@OneToOne(cascade = CascadeType.REMOVE)
	private Adresse adresse;
	private String nomEcole;
	private String intituleFiliere;
	@OneToOne
	private NiveauEtude niveauEtudes;
	@OneToMany
	private Collection<Certificat> certificats;
	@OneToMany
	private Collection<Competence> competences;
	@OneToMany
	private Collection<Langue> langues;
	@OneToMany(cascade = CascadeType.REMOVE)
	private Collection<ProjetPersonnel> projetsPersonnels;
	@OneToMany(cascade = CascadeType.REMOVE)
	private Collection<ExperienceProfessionnelle> experiencesProfessionnelles;
	private String typeRechercheNaturel = "Etudiant";//Ce champ est là juste pour faciliter la recherche et va toujours contenir la chaine "ETUDIANT"

	public Etudiant() {
	}
 
	public Etudiant(Builder builder) {
		this.adresse = builder.adresse;
		this.certificats = builder.certificats;
		this.competences = builder.competences;
		this.email = builder.email;
		this.experiencesProfessionnelles = builder.experiencesProfessionnelles;
		this.langues = builder.langues;
		this.nom = builder.nom;
		this.prenom = builder.prenom;
		this.projetsPersonnels = builder.projetsPersonnels;
		this.telephone = builder.telephone;
		this.niveauEtudes = builder.niveauEtudes;
		this.nomEcole = builder.nomEcole;
		this.intituleFiliere = builder.intituleFiliere;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
		this.username = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public String getNomEcole() {
		return nomEcole;
	}

	public void setNomEcole(String nomEcole) {
		this.nomEcole = nomEcole;
	}

	public String getIntituleFiliere() {
		return intituleFiliere;
	}

	public void setIntituleFiliere(String intituleFiliere) {
		this.intituleFiliere = intituleFiliere;
	}

	public NiveauEtude getNiveauEtudes() {
		return niveauEtudes;
	}

	public void setNiveauEtudes(NiveauEtude niveauEtudes) {
		this.niveauEtudes = niveauEtudes;
	}

	public Collection<Certificat> getCertificats() {
		return certificats;
	}

	public void setCertificats(Collection<Certificat> certificats) {
		this.certificats = certificats;
	}

	public Collection<Competence> getCompetences() {
		return competences;
	}

	public void setCompetences(Collection<Competence> competences) {
		this.competences = competences;
	}

	public Collection<Langue> getLangues() {
		return langues;
	}

	public void setLangues(Collection<Langue> langues) {
		this.langues = langues;
	}

	public Collection<ProjetPersonnel> getProjetsPersonnels() {
		return projetsPersonnels;
	}

	public void setProjetsPersonnels(Collection<ProjetPersonnel> projetsPersonnels) {
		this.projetsPersonnels = projetsPersonnels;
	}

	public Collection<ExperienceProfessionnelle> getExperiencesProfessionnelles() {
		return experiencesProfessionnelles;
	}

	public void setExperiencesProfessionnelles(Collection<ExperienceProfessionnelle> experiencesProfessionnelles) {
		this.experiencesProfessionnelles = experiencesProfessionnelles;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	 @Override
	    public int hashCode() {
	        return new HashCodeBuilder().append(id)
	                .append(nom)
	                .append(prenom)	                
	                .toHashCode();
	    }

	    @Override
	    public boolean equals(Object obj) {
	        if (this == obj)
	            return true;
	        if (obj == null)
	            return false;
	        if (getClass() != obj.getClass())
	            return false;
	        Etudiant other = (Etudiant) obj;

	        return new EqualsBuilder().append(id, other.id)
	                .append(nom, other.nom)
	                .append(prenom, other.prenom)
	                .isEquals();
	    }


	public static class Builder {
		private String nom;
		private String prenom;
		private String email;
		private String telephone;
		private Adresse adresse;
		private String nomEcole;
		private String intituleFiliere;
		private NiveauEtude niveauEtudes;
		private Collection<Certificat> certificats;
		private Collection<Competence> competences;
		private Collection<Langue> langues;
		private Collection<ProjetPersonnel> projetsPersonnels;
		private Collection<ExperienceProfessionnelle> experiencesProfessionnelles;

		public Builder() {
		}


		public Builder withNom(String nom) {
			this.nom = nom;
			return this;
		}

		public Builder withPrenom(String prenom) {
			this.prenom = prenom;
			return this;
		}

		public Builder withEmail(String email) {
			this.email = email;
			return this;
		}

		public Builder withTelephone(String telephone) {
			this.telephone = telephone;
			return this;
		}

		public Builder withAdresse(Adresse adresse) {
			this.adresse = adresse;
			return this;
		}

		public Builder withCertificats(Collection<Certificat> certificats) {
			this.certificats = certificats;
			return this;
		}

		public Builder withCompetences(Collection<Competence> competences) {
			this.competences = competences;
			return this;
		}

		public Builder withLangues(Collection<Langue> langues) {
			this.langues = langues;
			return this;
		}

		public Builder withProjetsPersonnels(Collection<ProjetPersonnel> projetsPersonnels) {
			this.projetsPersonnels = projetsPersonnels;
			return this;
		}

		public Builder withExperiencesProfessionnelles(
				Collection<ExperienceProfessionnelle> experiencesProfessionnelles) {
			this.experiencesProfessionnelles = experiencesProfessionnelles;
			return this;
		}

		public Builder withNomEcole(String nomEcole) {
			this.nomEcole = nomEcole;
			return this;
		}
		
		public Builder withIntituleFiliere(String intituleFiliere) {
			this.intituleFiliere = intituleFiliere;
			return this;
		}
		
		public Builder withNiveauEtudes(NiveauEtude niveauEtudes) {
			this.niveauEtudes = niveauEtudes;
			return this;
		}

		public Etudiant build() {
			return new Etudiant(this);
		}
	}
}
