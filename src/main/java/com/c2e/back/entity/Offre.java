package com.c2e.back.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
public class Offre implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@OneToMany(cascade = CascadeType.REMOVE)
	@OrderBy("dateCandidature DESC")
	private Collection<Candidature> candidatures;
	private String intitule;
	private LocalDateTime datePublication;
	private LocalDateTime dateFin;
	@OneToOne
	private Adresse adresseOffre; 
	@Column(length = 1000000)
	private String descriptionPoste;
	@Column(length = 1000000)
	private String descriptionProfil;
	private TypeOffre typeOffre;
	@OneToMany
	private Collection<Ecole> ecolesConcernees;
	@OneToMany
	private Collection<NiveauEtude> niveauxEtudes;
	@OneToMany
	private Collection<Competence> competences;
	private String nomEntreprise;
	private String imageEntreprise;
	private Long idEntreprise;
	@Column(columnDefinition = "boolean default false")
	private boolean ouvert;
	private String link;
    @Column(columnDefinition = "boolean default false")
	private boolean shared;
	
	public Offre() {
	}

	public Offre(Builder builder) {
		this.id = builder.id;
		this.candidatures = builder.candidatures;
		this.intitule = builder.intitule;
		this.datePublication = builder.datePublication;
		this.dateFin = builder.dateFin;
		this.adresseOffre = builder.adresseOffre;
		this.descriptionPoste = builder.descriptionPoste;
		this.descriptionProfil = builder.descriptionProfil;
		this.typeOffre = builder.typeOffre;
		this.ecolesConcernees = builder.ecolesConcernees;
		this.niveauxEtudes = builder.niveauxEtudes;
		this.competences = builder.competences;
		this.nomEntreprise = builder.nomEntreprise;
		this.imageEntreprise = builder.imageEntreprise;
		this.idEntreprise = builder.idEntreprise;
		this.ouvert = builder.ouvert;
		this.link = builder.link;
		this.shared = builder.shared;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Collection<Candidature> getCandidatures() {
		return candidatures;
	}

	public void setCandidatures(Collection<Candidature> candidatures) {
		this.candidatures = candidatures;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public LocalDateTime getDatePublication() {
		return datePublication;
	}

	public void setDatePublication(LocalDateTime datePublication) {
		this.datePublication = datePublication;
	}

	public LocalDateTime getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDateTime dateFin) {
		this.dateFin = dateFin;
	}

	public Adresse getAdresseOffre() {
		return adresseOffre;
	}

	public void setAdresseOffre(Adresse adresseOffre) {
		this.adresseOffre = adresseOffre;
	}

	public String getDescriptionPoste() {
		return descriptionPoste;
	}

	public void setDescriptionPoste(String descriptionPoste) {
		this.descriptionPoste = descriptionPoste;
	}

	public String getDescriptionProfil() {
		return descriptionProfil;
	}

	public void setDescriptionProfil(String descriptionProfil) {
		this.descriptionProfil = descriptionProfil;
	}

	public TypeOffre getTypeOffre() {
		return typeOffre;
	}

	public void setTypeOffre(TypeOffre typeOffre) {
		this.typeOffre = typeOffre;
	}

	public Collection<Ecole> getEcolesConcernees() {
		return ecolesConcernees;
	}

	public void setEcolesConcernees(Collection<Ecole> ecolesConcernees) {
		this.ecolesConcernees = ecolesConcernees;
	}

	public Collection<NiveauEtude> getNiveauxEtudes() {
		return niveauxEtudes;
	}

	public void setNiveauxEtudes(Collection<NiveauEtude> niveauxEtudes) {
		this.niveauxEtudes = niveauxEtudes;
	}

	public Collection<Competence> getCompetences() {
		return competences;
	}

	public void setCompetences(Collection<Competence> competences) {
		this.competences = competences;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	public String getImageEntreprise() {
		return imageEntreprise;
	}

	public void setImageEntreprise(String imageEntreprise) {
		this.imageEntreprise = imageEntreprise;
	}

	public Long getIdEntreprise() {
		return idEntreprise;
	}

	public void setIdEntreprise(Long idEntreprise) {
		this.idEntreprise = idEntreprise;
	}
	

	public boolean isOuvert() {
		return ouvert;
	}

	public void setOuvert(boolean ouvert) {
		this.ouvert = ouvert;
	}
	

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	
	public boolean isShared() {
		return shared;
	}

	public void setShared(boolean isShared) {
		this.shared = isShared;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public static class Builder {
		private Long id;
		private Collection<Candidature> candidatures;
		private String intitule;
		private LocalDateTime datePublication;
		private LocalDateTime dateFin;
		private Adresse adresseOffre;
		private String descriptionPoste;
		private String descriptionProfil;
		private TypeOffre typeOffre;
		private Collection<Ecole> ecolesConcernees;
		private Collection<NiveauEtude> niveauxEtudes;
		private Collection<Competence> competences;
		private String nomEntreprise;
		private String imageEntreprise;
		private Long idEntreprise;
		private boolean ouvert;
		private String link;
		private boolean shared;
		
		public Builder() {
			// TODO Auto-generated constructor stub
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withCandidatures(Collection<Candidature> candidatures) {
			this.candidatures = candidatures;
			return this;
		}

		public Builder withIntitule(String intitule) {
			this.intitule = intitule;
			return this;
		}

		public Builder withDatePublication(LocalDateTime datePublication) {
			this.datePublication = datePublication;
			return this;
		}

		public Builder withDateFin(LocalDateTime dateFin) {
			this.dateFin = dateFin;
			return this;
		}

		public Builder withAdresseOffre(Adresse adresseOffre) {
			this.adresseOffre = adresseOffre;
			return this;
		}

		public Builder withDescriptionPoste(String descriptionPoste) {
			this.descriptionPoste = descriptionPoste;
			return this;
		}

		public Builder withDescriptionProfil(String descriptionProfil) {
			this.descriptionProfil = descriptionProfil;
			return this;
		}

		public Builder withTypeOffre(TypeOffre typeOffre) {
			this.typeOffre = typeOffre;
			return this;
		}

		public Builder withEcolesConcernees(Collection<Ecole> ecolesConcernées) {
			this.ecolesConcernees = ecolesConcernées;
			return this;
		}

		public Builder withNiveauxEtudes(Collection<NiveauEtude> niveauxEtudes) {
			this.niveauxEtudes = niveauxEtudes;
			return this;
		}

		public Builder withCompetences(Collection<Competence> competences) {
			this.competences = competences;
			return this;
		}

		public Builder withNomEntreprise(String nomEntreprise) {
			this.nomEntreprise = nomEntreprise;
			return this;
		}

		public Builder withImageEntreprise(String imageEntreprise) {
			this.imageEntreprise = imageEntreprise;
			return this;
		}
		
		public Builder withIdEntreprise(Long idEntreprise) {
			this.idEntreprise = idEntreprise;
			return this;
		}
		
		public Builder withOuvert(boolean ouvert) {
			this.ouvert = ouvert;
			return this;
		}
		
		public Builder withLink(String link) {
			this.link = link;
			return this;
		}
		
		public Builder withIsShared(boolean isShared) {
			this.shared = isShared;
			return this;
		}
		
		public Offre build() {
			return new Offre(this);
		}
	}
}