package com.c2e.back.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
public class Competence implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@OneToOne
	private IntituleCompetence intituleCompetence;
	private boolean estSevenSkills; // Il ne doit pas y avoir plus de 7 sevenSkills
	private int pourcentageDeMaitrise; //Cette valeur doit être sur 100(entre 1 et 100)
	
	public Competence() {
	}

	public Competence(Builder builder) {
		this.id = builder.id;
		this.intituleCompetence = builder.intituleCompetence;
		this.estSevenSkills = builder.estSevenSkills; 
		this.pourcentageDeMaitrise = builder.pourcentageDeMaitrise;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public IntituleCompetence getIntituleCompetence() {
		return intituleCompetence;
	}

	public void setIntituleCompetence(IntituleCompetence intitiuleCompetence) {
		this.intituleCompetence = intitiuleCompetence;
	}

	public boolean isEstSevenSkills() {
		return estSevenSkills;
	}

	public void setEstSevenSkills(boolean estSevenSkills) {
		this.estSevenSkills = estSevenSkills;
	}

	public int getPourcentageDeMaitrise() {
		return pourcentageDeMaitrise;
	}

	public void setPourcentageDeMaitrise(int pourcentageDeMaitrise) {
		this.pourcentageDeMaitrise = pourcentageDeMaitrise;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public static class Builder {
		private Long id;
		private IntituleCompetence intituleCompetence;
		private boolean estSevenSkills;
		private int pourcentageDeMaitrise;
		
		public Builder() {
			
		}
		
		public Builder withId(Long id) {
			this.id = id;
			return this;
		}
		
		public Builder withIntituleCompetence(IntituleCompetence intituleCompetence) {
			this.intituleCompetence = intituleCompetence;
			return this;
		}
		
		public Builder withEstSevenSkills(boolean estSevenSkills) {
			this.estSevenSkills = estSevenSkills;
			return this;
		}
		
		public Builder withPourcentageDeMaitrise(int pourcentageDeMaitrise) {
			this.pourcentageDeMaitrise = pourcentageDeMaitrise;
			return this;
		}
		
		public Competence build() {
			return new Competence(this);
		}
		
	}
}
