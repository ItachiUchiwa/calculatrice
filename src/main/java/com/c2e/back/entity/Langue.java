package com.c2e.back.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
public class Langue implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	private Integer niveau; // Niveau sur 100;

	public Langue() {
	}
	
	

	public Langue(Builder builder) {
		this.id = builder.id;
		this.nom = builder.nom;
		this.niveau = builder.niveau;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Integer getNiveau() {
		return niveau;
	}

	public void setNiveau(Integer niveau) {
		this.niveau = niveau;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public static class Builder {
		private Long id;
		private String nom;
		private Integer niveau; 
		
		public Builder() {
			
		}
		
		public Builder withId(Long id) {
			this.id = id;
			return this;
		}
		
		public Builder withNom(String nom) {
			this.nom = nom;
			return this;
		}
		
		public Builder withNiveau(Integer niveau) {
			this.niveau = niveau;
			return this;
		}
		
		public Langue build() {
			return new Langue(this);
		}
		
	}


}
