package com.c2e.back.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.c2e.back.entity.Candidature.Builder;
import static com.c2e.back.configuration.ValeurGlobale.*;

@Entity
@DiscriminatorValue("Spontanee")
public class CandidatureSpontanee extends Candidature implements Serializable {

	@Column(length = MAX_MOTIVATION_TEXT)
	private String motivation;

	public CandidatureSpontanee() {
		super();
	}

	public CandidatureSpontanee(Builder builder) {
		this.id = builder.id;
		this.dateCandidature = builder.dateCandidature;
		this.etapeRecrutement = builder.etapeRecrutement;
		this.etudiant = builder.etudiant;
		this.motivation = motivation;
	}

	public String getMotivation() {
		return motivation;
	}

	public void setMotivation(String motivation) {
		this.motivation = motivation;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public static class Builder {
		private Long id;
		private LocalDateTime dateCandidature;
		private EtapeRecrutement etapeRecrutement;
		private Etudiant etudiant;
		private String motivation;
		
		public Builder() {
		}
		
		public Builder withId(Long id) {
			this.id = id;
			return this;
		}
		
		public Builder withDateCandidature(LocalDateTime dateCandidature) {
			this.dateCandidature = dateCandidature;
			return this;
		}
		
		public Builder withEtat(EtapeRecrutement etapeRecrutement) {
			this.etapeRecrutement = etapeRecrutement;
			return this;
		}
		
		public Builder withEtudiant(Etudiant etudiant) {
			this.etudiant = etudiant;
			return this;
		}
		
		public Builder withMotivation(String motivation) {
			this.motivation = motivation;
			return this;
		}
		
		public CandidatureSpontanee build() {
			return new CandidatureSpontanee(this);
		}
	}
}
