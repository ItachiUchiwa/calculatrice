package com.c2e.back.exception;

public class EntrepriseException extends Exception {

	public EntrepriseException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EntrepriseException(String arg0) {
		super(arg0);
	}

	public static class NombreCollaborateurNotCorrect extends EntrepriseException {

		public NombreCollaborateurNotCorrect() {
			super();
		}

		public NombreCollaborateurNotCorrect(String arg0) {
			super(arg0);
		}
		
	}

	public static class EmailExists extends EntrepriseException {

		public EmailExists() {
			super();
		}

		public EmailExists(String arg0) {
			super(arg0);
		}
		
	}
	
	public static class EmailNotCorrect extends EntrepriseException {

		public EmailNotCorrect() {
			super();
		}

		public EmailNotCorrect(String arg0) {
			super(arg0);
		}
		
	}
	public static class LienSiteNotCorrect extends EntrepriseException {

		public LienSiteNotCorrect() {
			super();
		}

		public LienSiteNotCorrect(String arg0) {
			super(arg0);
		}
		
	}
	
	public static class NomNotCorrect extends EntrepriseException {

		public NomNotCorrect() {
			super();
		}

		public NomNotCorrect(String arg0) {
			super(arg0);
		}
		
	}
	
	public static class TelephoneNotCorrect extends EntrepriseException {

		public TelephoneNotCorrect() {
			super();
		}

		public TelephoneNotCorrect(String arg0) {
			super(arg0);
		}
		
	}
	
}
