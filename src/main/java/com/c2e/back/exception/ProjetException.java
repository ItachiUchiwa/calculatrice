package com.c2e.back.exception;

public class ProjetException extends Exception {

	public ProjetException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProjetException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public static class IntituleNotCorrect extends ProjetException{

		public IntituleNotCorrect() {
			super();
		}

		public IntituleNotCorrect(String message) {
			super(message);
		}
		
	}
	
	public static class DescriptionNotCorrect extends ProjetException{

		public DescriptionNotCorrect() {
			super();
		}

		public DescriptionNotCorrect(String message) {
			super(message);
		}
		
	}
	
	public static class EntrepriseHoteNotCorrect extends ProjetException{

		public EntrepriseHoteNotCorrect() {
			super();
		}

		public EntrepriseHoteNotCorrect(String message) {
			super(message);
		}
		
	}
	
	public static class NombreJourException extends EntrepriseException {

		public NombreJourException() {
			super();
		}

		public NombreJourException(String arg0) {
			super(arg0);
		}
		
	}
}
