package com.c2e.back.exception;

public class EcoleException extends Exception{

	public EcoleException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EcoleException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public static class EcoleNotExist extends EcoleException{

		public EcoleNotExist() {
			super();
		}

		public EcoleNotExist(String message) {
			super(message);
		}
		
	}
	
	public static class FiliereNullException extends EcoleException{

		public FiliereNullException() {
			super();
		}

		public FiliereNullException(String message) {
			super(message);
		}
		
	}
}
