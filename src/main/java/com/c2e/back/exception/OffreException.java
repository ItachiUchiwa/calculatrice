package com.c2e.back.exception;

public class OffreException extends Exception {

	public OffreException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OffreException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public static class IntituleOffreNotCorrect extends OffreException {

		public IntituleOffreNotCorrect() {
			super();
			// TODO Auto-generated constructor stub
		}

		public IntituleOffreNotCorrect(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}
		
	}
	
	public static class DescriptionPosteOffreNotCorrect extends OffreException {

		public DescriptionPosteOffreNotCorrect() {
			super();
			// TODO Auto-generated constructor stub
		}

		public DescriptionPosteOffreNotCorrect(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}
	
	}
	
	public static class DescriptionProfilOffreNotCorrect extends OffreException {

		public DescriptionProfilOffreNotCorrect() {
			super();
			// TODO Auto-generated constructor stub
		}

		public DescriptionProfilOffreNotCorrect(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}
		
	}
	
	public static class TypeOffreNotCorrect extends OffreException {

		public TypeOffreNotCorrect() {
			super();
			// TODO Auto-generated constructor stub
		}

		public TypeOffreNotCorrect(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}
		
	}
}
