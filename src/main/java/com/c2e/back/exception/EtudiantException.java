package com.c2e.back.exception;

public class EtudiantException extends Exception {

	public EtudiantException() {
		super();
	}

	public EtudiantException(String message) {
		super(message);
	}

	public static class NomException extends EtudiantException {

		public NomException() {
			super();
			// TODO Auto-generated constructor stub
		}

		public NomException(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}
	}

	public static class PrenomException extends EtudiantException {

		public PrenomException() {
			super();
		}

		public PrenomException(String message) {
			super(message);
		}
	}

	public static class EmailExists extends EtudiantException {

		public EmailExists() {
			super();
		}

		public EmailExists(String arg0) {
			super(arg0);
		}

	}

	public static class EmailNotCorrect extends EtudiantException {

		public EmailNotCorrect() {
			super();
		}

		public EmailNotCorrect(String arg0) {
			super(arg0);
		}

	}

	public static class TelephoneNotCorrect extends EtudiantException {

		public TelephoneNotCorrect() {
			super();
		}

		public TelephoneNotCorrect(String arg0) {
			super(arg0);
		}

	}

	public static class ToMuchSevenSkills extends EntrepriseException {

		public ToMuchSevenSkills() {
			super();
		}

		public ToMuchSevenSkills(String arg0) {
			super(arg0);
		}

	}
	
	public static class DejaCandidateException extends EtudiantException {

		public DejaCandidateException() {
			super();
		}

		public DejaCandidateException(String arg0) {
			super(arg0);
		}

	}
}
