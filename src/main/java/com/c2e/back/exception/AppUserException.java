package com.c2e.back.exception;

public class AppUserException extends Exception{

	public AppUserException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AppUserException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public static class UserAlreadyExistsException extends AppUserException{

		public UserAlreadyExistsException() {
			super();
			// TODO Auto-generated constructor stub
		}

		public UserAlreadyExistsException(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}
	}
	

	public static class PasswordDontEqualsToRepassword extends AppUserException{

		public PasswordDontEqualsToRepassword() {
			super();
			// TODO Auto-generated constructor stub
		}

		public PasswordDontEqualsToRepassword(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}
		
	}
	
	public static class PasswordSyntaxeException extends AppUserException{

		public PasswordSyntaxeException() {
			super();
			// TODO Auto-generated constructor stub
		}

		public PasswordSyntaxeException(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}
		
	}
	
	public static class UserNotExist extends AppUserException{

		public UserNotExist() {
			super();
			// TODO Auto-generated constructor stub
		}

		public UserNotExist(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}
		
	}
	
	public static class TokenExpiredException extends AppUserException{

		public TokenExpiredException() {
			super();
			// TODO Auto-generated constructor stub
		}

		public TokenExpiredException(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}
	}
	public static class TokenNotExistsException extends AppUserException{

		public TokenNotExistsException() {
			super();
			// TODO Auto-generated constructor stub
		}

		public TokenNotExistsException(String message) {
			super(message);
			// TODO Auto-generated constructor stub
		}
	}
}
