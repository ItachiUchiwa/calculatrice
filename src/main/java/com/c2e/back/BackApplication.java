package com.c2e.back;

import static com.c2e.back.configuration.ValeurGlobale.OFFRE_SPONTANEE;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.c2e.back.dao.AdresseRepository;
import com.c2e.back.dao.AppRoleRepository;
import com.c2e.back.dao.AppUserRepository;
import com.c2e.back.dao.CertificatRepository;
import com.c2e.back.dao.CompetenceRepository;
import com.c2e.back.dao.DomaineRepository;
import com.c2e.back.dao.EcoleRepository;
import com.c2e.back.dao.EntrepriseRepository;
import com.c2e.back.dao.EtudiantRepository;
import com.c2e.back.dao.ExperienceProfessionnelleRepository;
import com.c2e.back.dao.FiliereRepository;
import com.c2e.back.dao.IntituleCompetenceRepository;
import com.c2e.back.dao.LangueRepository;
import com.c2e.back.dao.NiveauEtudeRepository;
import com.c2e.back.dao.OffreRepository;
import com.c2e.back.dao.ProjetPersonnelRepository;
import com.c2e.back.dao.ProjetRepository;
import com.c2e.back.entity.Adresse;
import com.c2e.back.entity.AppRole;
import com.c2e.back.entity.AppUser;
import com.c2e.back.entity.Certificat;
import com.c2e.back.entity.Competence;
import com.c2e.back.entity.Domaine;
import com.c2e.back.entity.Ecole;
import com.c2e.back.entity.Entreprise;
import com.c2e.back.entity.Etudiant;
import com.c2e.back.entity.ExperienceProfessionnelle;
import com.c2e.back.entity.IntituleCompetence;
import com.c2e.back.entity.Langue;
import com.c2e.back.entity.NiveauEtude;
import com.c2e.back.entity.Offre;
import com.c2e.back.entity.Projet;
import com.c2e.back.entity.ProjetPersonnel;
import com.c2e.back.entity.TypeOffre;
import com.c2e.back.service.AccountService;

@SpringBootApplication
public class BackApplication extends SpringBootServletInitializer implements CommandLineRunner {

	@Autowired
	private AdresseRepository adresseRepository;
	@Autowired
	private DomaineRepository domaineRepository;
	@Autowired
	private EntrepriseRepository entrepriseRepository;
	@Autowired
	private ProjetRepository projetRepository;
	@Autowired
	private OffreRepository offreRepository;
	@Autowired
	private EtudiantRepository etudiantRepository;
	@Autowired
	private LangueRepository langueRepository;
	@Autowired
	private CertificatRepository certificatRepository;
	@Autowired
	private ProjetPersonnelRepository projetPersonnelRepository;
	@Autowired
	private ExperienceProfessionnelleRepository experienceProfessionnelleRepository;
	@Autowired
	private CompetenceRepository competenceRepository;
	@Autowired
	private NiveauEtudeRepository niveauEtudeRepository;
	@Autowired
	private EcoleRepository ecoleRepository;
	@Autowired
	private FiliereRepository filiereRepository;
	@Autowired
	private IntituleCompetenceRepository intituleCompetenceRepository;
	@Autowired
	private AccountService accountService;
	@Autowired
	private AppRoleRepository appRoleRepository;
	@Autowired
	private AppUserRepository appUserRepository;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(BackApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(BackApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		createNiveau();
		createEcole();
		createRole();
		createOwner();
	}

	public void createRole() {
		if (!appRoleRepository.existsByRole("OWNER")) {
			accountService.saveRole(new AppRole(null, "OWNER"));
		}
		if (!appRoleRepository.existsByRole("DEV")) {
			accountService.saveRole(new AppRole(null, "DEV"));
		}
		if (!appRoleRepository.existsByRole("USER")) {
			accountService.saveRole(new AppRole(null, "USER"));
		}
		if (!appRoleRepository.existsByRole("ENTREPRISE")) {
			accountService.saveRole(new AppRole(null, "ENTREPRISE"));
		}
		if (!appRoleRepository.existsByRole("MANAGER")) {
			accountService.saveRole(new AppRole(null, "MANAGER"));
		}
		if (!appRoleRepository.existsByRole("ETUDIANT")) {
			accountService.saveRole(new AppRole(null, "ETUDIANT"));
		}

	}

	public void createOwner() {
		AppUser owner = new AppUser("binkeissa@protonmail.com", "1@1MID23d&$d5d8");
		if (!appUserRepository.existsByUsername(owner.getUsername())) {
			accountService.saveOwner(owner);
			accountService.addRoleToUser(owner.getUsername(), "OWNER");
			accountService.addRoleToUser(owner.getUsername(), "USER");
			accountService.addRoleToUser(owner.getUsername(), "ENTREPRISE");
			accountService.addRoleToUser(owner.getUsername(), "MANAGER");
			accountService.addRoleToUser(owner.getUsername(), "DEV");
			appUserRepository.save(owner);
		}
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	public void saveEntreprise() {
		Adresse adresse1 = new Adresse.Builder().withPays("France").withVille("Antony")
				.withAdresse("9 A venue Jeanne d'arc").build();
		adresseRepository.save(adresse1);

		Domaine domaine1 = new Domaine.Builder().withIntitule("Nettoyage & Aménagement").build();
		domaineRepository.save(domaine1);

		Projet projet1 = new Projet.Builder().withIntitule("Nettoyage Boulevard Hassan 2").withDescription(
				"Nous sommmes les responsables de l'entretien du boulevard Hassan 2 depuis 2005. Le contrat dvrait normmalement finir en juillet 2010 mais le contrat fut renouvellé 2 fois d'affilé")
				.build();
		Projet projet2 = new Projet.Builder().withIntitule("Nettoyage Hotel Hassan 2").withDescription(
				"Nous sommmes les responsables de l'entretien du boulevard Hassan 2 depuis 2005. Le contrat dvrait normmalement finir en juillet 2010 mais le contrat fut renouvellé 2 fois d'affilé")
				.build();
		projetRepository.save(projet1);
		projetRepository.save(projet2);
		ArrayList<Projet> projets = new ArrayList<Projet>();
		projets.add(projet1);
		projets.add(projet2);

		// toutes les entreprises ont par défaut cet offre:
		Offre SPONTANNEE = new Offre.Builder().withIntitule(OFFRE_SPONTANEE).withTypeOffre(TypeOffre.SPONTANEE).build();
		offreRepository.save(SPONTANNEE);

		Offre offre1 = new Offre.Builder().withAdresseOffre(adresse1)
				.withDatePublication((LocalDateTime.of(1990, Month.DECEMBER, 12, 12, 11)))
				.withDescriptionPoste("Ce poste est fait pour les experts en nettoyage")
				.withDescriptionProfil("Bac +5 en nettoyage et 2 ans d'expérience").withAdresseOffre(adresse1)
				.withNomEntreprise("Cleanitrine").withIntitule("FVBQN").withTypeOffre(TypeOffre.STAGE).build();
		offreRepository.save(offre1);

		Offre offre2 = new Offre.Builder().withAdresseOffre(adresse1)
				.withDatePublication((LocalDateTime.of(2020, Month.SEPTEMBER, 9, 12, 11)))
				.withDescriptionPoste("Ce poste est fait pour les experts en nettoyage")
				.withDescriptionProfil("Bac +5 en nettoyage et 2 ans d'expérience").withTypeOffre(TypeOffre.ALTERNANCE)
				.withNomEntreprise("Cleanitrine").withAdresseOffre(adresse1).withIntitule("Alternance de nettoyage")
				.build();
		offreRepository.save(offre2);

		Offre offre3 = new Offre.Builder().withAdresseOffre(adresse1)
				.withDatePublication(LocalDateTime.of(1990, Month.DECEMBER, 12, 12, 11))
				.withDescriptionPoste("Ce poste est fait pour les experts en nettoyage")
				.withDescriptionProfil("Bac +5 en nettoyage et 2 ans d'expérience").withIntitule("Emploi de nettoyage")
				.withNomEntreprise("Cleanitrine").withAdresseOffre(adresse1).withTypeOffre(TypeOffre.EMPLOI).build();
		offreRepository.save(offre3);

		Offre offre4 = new Offre.Builder().withAdresseOffre(adresse1)
				.withDatePublication(LocalDateTime.of(1990, Month.DECEMBER, 12, 12, 11))
				.withDescriptionPoste("Cet appel d'offre est fait pour les sociétés de vente de détergent")
				.withDescriptionProfil("Bac +5 en nettoyage et 2 ans d'expérience")
				.withIntitule("Appel d'offre de nettoyage").withAdresseOffre(adresse1).withNomEntreprise("Cleanitrine")
				.withTypeOffre(TypeOffre.APPEL_OFFRE).build();
		offreRepository.save(offre4);

		ArrayList<Offre> offres = new ArrayList<>();
		offres.add(offre1);
		offres.add(offre2);
		offres.add(offre3);
		offres.add(offre4);
		offres.add(SPONTANNEE);

		Entreprise entreprise = new Entreprise.Builder().withAdresse(adresse1).withChiffreAffaire(1000000f)
				.withDateCreation(LocalDate.now()).withDomaine(domaine1).withEmail("binkeissa@protonmail.com")
				.withLienSiteWeb("http://binkeissa-ingenieur.com/").withNom("Cleanitrine").withNombreCollaborateur(100)
				.withOffres(offres).withProjets(projets).withTelephone("028738339393").build();
		entrepriseRepository.save(entreprise);
		entrepriseRepository.save(new Entreprise());

		Domaine domaine2 = new Domaine.Builder().withIntitule("Santé").build();
		domaineRepository.save(domaine2);
		Domaine domaine3 = new Domaine.Builder().withIntitule("Education").build();
		domaineRepository.save(domaine3);
		Domaine domaine4 = new Domaine.Builder().withIntitule("Consulting").build();
		domaineRepository.save(domaine4);

	}

	public void saveEtudiant() {
		Adresse adresse2 = new Adresse.Builder().withPays("France").withVille("Evry").withAdresse("25 RUE Cathédrale")
				.build();
		adresseRepository.save(adresse2);

		Certificat certificat1 = new Certificat.Builder().withIntitule("Oracle Certified & Associate").build();
		Certificat certificat2 = new Certificat.Builder().withIntitule("Oracle Certified Professional").build();
		Certificat certificat3 = new Certificat.Builder().withIntitule("TOEIC").build();
		certificatRepository.save(certificat1);
		certificatRepository.save(certificat2);
		certificatRepository.save(certificat3);
		ArrayList<Certificat> certificats = new ArrayList();
		certificats.add(certificat1);
		certificats.add(certificat2);
		certificats.add(certificat3);

		Langue langue1 = new Langue.Builder().withNom("English").build();
		Langue langue2 = new Langue.Builder().withNom("Arabe").build();
		Langue langue3 = new Langue.Builder().withNom("Français").build();
		Langue langue4 = new Langue.Builder().withNom("Deushland").build();
		langueRepository.save(langue1);
		langueRepository.save(langue2);
		langueRepository.save(langue3);
		langueRepository.save(langue4);
		ArrayList<Langue> langues = new ArrayList();
		langues.add(langue1);
		langues.add(langue2);
		langues.add(langue3);
		langues.add(langue4);

		IntituleCompetence intituleCompetence1 = new IntituleCompetence.Builder().withIntitule("java").build();
		intituleCompetenceRepository.save(intituleCompetence1);
		IntituleCompetence intituleCompetence2 = new IntituleCompetence.Builder().withIntitule("oracle").build();
		intituleCompetenceRepository.save(intituleCompetence2);
		IntituleCompetence intituleCompetence3 = new IntituleCompetence.Builder().withIntitule("Python").build();
		intituleCompetenceRepository.save(intituleCompetence3);
		IntituleCompetence intituleCompetence4 = new IntituleCompetence.Builder().withIntitule("Comptabilité").build();
		intituleCompetenceRepository.save(intituleCompetence4);
		IntituleCompetence intituleCompetence5 = new IntituleCompetence.Builder().withIntitule("C#").build();
		intituleCompetenceRepository.save(intituleCompetence5);
		IntituleCompetence intituleCompetence6 = new IntituleCompetence.Builder().withIntitule("Analyse de données")
				.build();
		intituleCompetenceRepository.save(intituleCompetence6);
		IntituleCompetence intituleCompetence7 = new IntituleCompetence.Builder().withIntitule("Adobe Reader").build();
		intituleCompetenceRepository.save(intituleCompetence7);
		IntituleCompetence intituleCompetence8 = new IntituleCompetence.Builder().withIntitule("Scala").build();
		intituleCompetenceRepository.save(intituleCompetence8);
		IntituleCompetence intituleCompetence9 = new IntituleCompetence.Builder().withIntitule("Google Cloud").build();
		intituleCompetenceRepository.save(intituleCompetence9);
		IntituleCompetence intituleCompetence10 = new IntituleCompetence.Builder().withIntitule("BI").build();
		intituleCompetenceRepository.save(intituleCompetence10);
		IntituleCompetence intituleCompetence11 = new IntituleCompetence.Builder().withIntitule("Web sémantique")
				.build();
		intituleCompetenceRepository.save(intituleCompetence11);
		IntituleCompetence intituleCompetence12 = new IntituleCompetence.Builder().withIntitule("AI").build();
		intituleCompetenceRepository.save(intituleCompetence12);

		Competence competence1 = new Competence.Builder().withIntituleCompetence(intituleCompetence1)
				.withEstSevenSkills(true).withPourcentageDeMaitrise(80).build();
		Competence competence2 = new Competence.Builder().withIntituleCompetence(intituleCompetence2)
				.withEstSevenSkills(true).withPourcentageDeMaitrise(50).build();
		Competence competence3 = new Competence.Builder().withIntituleCompetence(intituleCompetence3)
				.withEstSevenSkills(true).withPourcentageDeMaitrise(25).build();
		Competence competence4 = new Competence.Builder().withIntituleCompetence(intituleCompetence4)
				.withEstSevenSkills(true).withPourcentageDeMaitrise(65).build();
		Competence competence5 = new Competence.Builder().withIntituleCompetence(intituleCompetence5)
				.withEstSevenSkills(true).withPourcentageDeMaitrise(80).build();
		Competence competence6 = new Competence.Builder().withIntituleCompetence(intituleCompetence6)
				.withEstSevenSkills(true).withPourcentageDeMaitrise(10).build();
		Competence competence7 = new Competence.Builder().withIntituleCompetence(intituleCompetence7)
				.withEstSevenSkills(true).withPourcentageDeMaitrise(15).build();
		Competence competence8 = new Competence.Builder().withIntituleCompetence(intituleCompetence8).build();
		Competence competence9 = new Competence.Builder().withIntituleCompetence(intituleCompetence9).build();
		Competence competence10 = new Competence.Builder().withIntituleCompetence(intituleCompetence10).build();
		Competence competence11 = new Competence.Builder().withIntituleCompetence(intituleCompetence11).build();
		Competence competence12 = new Competence.Builder().withIntituleCompetence(intituleCompetence12).build();
		competenceRepository.save(competence1);
		competenceRepository.save(competence2);
		competenceRepository.save(competence3);
		competenceRepository.save(competence4);
		competenceRepository.save(competence5);
		competenceRepository.save(competence6);
		competenceRepository.save(competence9);
		competenceRepository.save(competence7);
		competenceRepository.save(competence8);
		competenceRepository.save(competence9);
		competenceRepository.save(competence10);
		competenceRepository.save(competence11);
		competenceRepository.save(competence12);

		ArrayList<Competence> competences = new ArrayList();
		competences.add(competence1);
		competences.add(competence2);
		competences.add(competence3);
		competences.add(competence4);
		competences.add(competence5);
		competences.add(competence6);
		competences.add(competence7);
		competences.add(competence8);
		competences.add(competence9);
		competences.add(competence10);
		competences.add(competence11);
		competences.add(competence12);

		ProjetPersonnel projetPersonnel1 = new ProjetPersonnel.Builder()
				.withIntitule("Mise en place système de repotting").withNombreJour(12)
				.withDescription("Organisation D'une plateforme de e-lecture collecte suffisamement de doùmêaL"
						+ "KD APOkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk")
				.build();
		ProjetPersonnel projetPersonnel2 = new ProjetPersonnel.Builder().withIntitule("Nettoyage Hotel Hassan 2")
				.withDescription(
						"Nous sommmes les responsables de l'entretien du boulevard Hassan 2 depuis 2005. Le contrat dvrait normmalement finir en juillet 2010 mais le contrat fut renouvellé 2 fois d'affilé")
				.build();
		projetPersonnelRepository.save(projetPersonnel1);
		projetPersonnelRepository.save(projetPersonnel2);
		ArrayList<ProjetPersonnel> projetsPersonnels = new ArrayList<ProjetPersonnel>();
		projetsPersonnels.add(projetPersonnel1);
		projetsPersonnels.add(projetPersonnel2);

		ExperienceProfessionnelle experienceProfessionnelle1 = new ExperienceProfessionnelle.Builder()
				.withIntitule("Mise en place système de repotting")
				.withDescription("Organisation D'une plateforme de e-lecture collecte suffisamement de doùmêaL"
						+ "KD APOkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk")
				.withEntrepriseHote("C2E").build();

		ExperienceProfessionnelle experienceProfessionnelle2 = new ExperienceProfessionnelle.Builder()
				.withIntitule("Nettoyage Hotel Hassan 2")
				.withDescription(
						"Nous sommmes les responsables de l'entretien du boulevard Hassan 2 depuis 2005. Le contrat dvrait normmalement finir en juillet 2010 mais le contrat fut renouvellé 2 fois d'affilé")
				.build();

		experienceProfessionnelleRepository.save(experienceProfessionnelle1);
		experienceProfessionnelleRepository.save(experienceProfessionnelle2);
		ArrayList<ExperienceProfessionnelle> experiencesProfessionnelles = new ArrayList<ExperienceProfessionnelle>();
		experiencesProfessionnelles.add(experienceProfessionnelle1);
		experiencesProfessionnelles.add(experienceProfessionnelle2);

		NiveauEtude niveauEtude1 = new NiveauEtude.Builder().withIntitule("BAgC+1").build();
		niveauEtudeRepository.save(niveauEtude1);
		Etudiant etudiant = new Etudiant.Builder().withAdresse(adresse2).withCertificats(certificats)
				.withCompetences(competences).withEmail("binkeiissa@protonmail.com")
				.withExperiencesProfessionnelles(experiencesProfessionnelles).withLangues(langues)
				.withNiveauEtudes(niveauEtude1).withNom("TABBAT").withPrenom("Zeïnab")
				.withProjetsPersonnels(projetsPersonnels).withTelephone("0627749531").build();
		etudiantRepository.save(etudiant);
		Etudiant etudiant2 = new Etudiant.Builder().withEmail("binkeiissa@protonmail.com").build();
		etudiantRepository.save(etudiant2);
		Etudiant etudiant3 = new Etudiant.Builder().withEmail("binkeiissa@protonmail.com").build();
		etudiantRepository.save(etudiant3);

	}

	public void createNiveau() {
		NiveauEtude niveauEtude1 = new NiveauEtude.Builder().withIntitule("BAC+1").build();
		NiveauEtude niveauEtude2 = new NiveauEtude.Builder().withIntitule("BAC+2").build();
		NiveauEtude niveauEtude3 = new NiveauEtude.Builder().withIntitule("BAC+3").build();
		NiveauEtude niveauEtude4 = new NiveauEtude.Builder().withIntitule("BAC+4").build();
		NiveauEtude niveauEtude5 = new NiveauEtude.Builder().withIntitule("BAC+5").build();
		NiveauEtude niveauEtude6 = new NiveauEtude.Builder().withIntitule("BAC+6").build();
		NiveauEtude niveauEtude7 = new NiveauEtude.Builder().withIntitule("BAC+7").build();
		NiveauEtude niveauEtude8 = new NiveauEtude.Builder().withIntitule("BAC+8").build();
		NiveauEtude niveauEtude9 = new NiveauEtude.Builder().withIntitule("BAC+ >9ans").build();
		NiveauEtude niveauEtude10 = new NiveauEtude.Builder().withIntitule("1A experience").build();
		NiveauEtude niveauEtude11 = new NiveauEtude.Builder().withIntitule("2A experience").build();
		NiveauEtude niveauEtude12 = new NiveauEtude.Builder().withIntitule("3A experience").build();
		NiveauEtude niveauEtude13 = new NiveauEtude.Builder().withIntitule("4A experience").build();
		NiveauEtude niveauEtude14 = new NiveauEtude.Builder().withIntitule("5A experience").build();
		NiveauEtude niveauEtude15 = new NiveauEtude.Builder().withIntitule("6A experience").build();
		NiveauEtude niveauEtude16 = new NiveauEtude.Builder().withIntitule("+ 6A experience").build();

		if (!niveauEtudeRepository.existsByIntitule(niveauEtude1.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude1);
		}
		if (!niveauEtudeRepository.existsByIntitule(niveauEtude2.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude2);
		}
		if (!niveauEtudeRepository.existsByIntitule(niveauEtude3.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude3);
		}
		if (!niveauEtudeRepository.existsByIntitule(niveauEtude4.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude4);
		}
		if (!niveauEtudeRepository.existsByIntitule(niveauEtude5.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude5);
		}
		if (!niveauEtudeRepository.existsByIntitule(niveauEtude6.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude6);
		}
		if (!niveauEtudeRepository.existsByIntitule(niveauEtude7.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude7);
		}
		if (!niveauEtudeRepository.existsByIntitule(niveauEtude8.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude8);
		}
		if (!niveauEtudeRepository.existsByIntitule(niveauEtude9.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude9);
		}
		if (!niveauEtudeRepository.existsByIntitule(niveauEtude10.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude10);
		}
		if (!niveauEtudeRepository.existsByIntitule(niveauEtude11.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude11);
		}
		if (!niveauEtudeRepository.existsByIntitule(niveauEtude12.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude12);
		}
		if (!niveauEtudeRepository.existsByIntitule(niveauEtude13.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude13);
		}
		if (!niveauEtudeRepository.existsByIntitule(niveauEtude14.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude14);
		}
		if (!niveauEtudeRepository.existsByIntitule(niveauEtude15.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude15);
		}
		if (!niveauEtudeRepository.existsByIntitule(niveauEtude16.getIntitule())) {
			niveauEtudeRepository.save(niveauEtude16);
		}
	}

	public void createEcole() {
		// http://hades-presse.com/formations/grandes_ecoles_marocaines.shtml
		if (!ecoleRepository.existsByNom("AIAC")) {
			Ecole ecoleAIMAC = new Ecole.Builder().withNom("AIAC").build();
			ecoleRepository.save(ecoleAIMAC);
		}
		if (!ecoleRepository.existsByNom("ECC")) {
			Ecole ecoleAIMAC = new Ecole.Builder().withNom("ECC").build();
			ecoleRepository.save(ecoleAIMAC);
		}
		if (!ecoleRepository.existsByNom("EHTP")) {
			Ecole ecoleEHTP = new Ecole.Builder().withNom("EHTP").build();
			ecoleRepository.save(ecoleEHTP);
		}
		if (!ecoleRepository.existsByNom("EMI")) {
			Ecole ecoleEMI = new Ecole.Builder().withNom("EMI").build();
			ecoleRepository.save(ecoleEMI);
		}
		if (!ecoleRepository.existsByNom("EMM")) {
			Ecole ecoleEMM = new Ecole.Builder().withNom("EMM").build();
			ecoleRepository.save(ecoleEMM);
		}
		if (!ecoleRepository.existsByNom("EMSI")) {
			Ecole ecoleEMSI = new Ecole.Builder().withNom("EMSI").build();
			ecoleRepository.save(ecoleEMSI);
		}
		if (!ecoleRepository.existsByNom("ENAM")) {
			Ecole ecoleENAM = new Ecole.Builder().withNom("ENAM").build();
			ecoleRepository.save(ecoleENAM);
		}
		if (!ecoleRepository.existsByNom("ENIM")) {
			Ecole ecoleENAM = new Ecole.Builder().withNom("ENIM").build();
			ecoleRepository.save(ecoleENAM);
		}
		if (!ecoleRepository.existsByNom("ENFI")) {
			Ecole ecoleENFI = new Ecole.Builder().withNom("ENFI").build();
			ecoleRepository.save(ecoleENFI);
		}
		if (!ecoleRepository.existsByNom("ENSMR")) {
			Ecole ecoleENSMR = new Ecole.Builder().withNom("ENSMR").build();
			ecoleRepository.save(ecoleENSMR);
		}
		if (!ecoleRepository.existsByNom("ENPL")) {
			Ecole ecole = new Ecole.Builder().withNom("ENPL").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ENSA Marrakech")) {
			Ecole ecole = new Ecole.Builder().withNom("ENSA Marrakech").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ENSA Agadir")) {
			Ecole ecole = new Ecole.Builder().withNom("ENSA Agadir").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ENSA Fès")) {
			Ecole ecole = new Ecole.Builder().withNom("ENSA Fès").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ENSAM")) {
			Ecole ecole = new Ecole.Builder().withNom("ENSAM").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ENSA Oujda")) {
			Ecole ecole = new Ecole.Builder().withNom("ENSA Oujda").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ENSA Safi")) {
			Ecole ecole = new Ecole.Builder().withNom("ENSA Safi").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ENSEM")) {
			Ecole ecole = new Ecole.Builder().withNom("ENSEM").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ENSIAS")) {
			Ecole ecole = new Ecole.Builder().withNom("ENSIAS").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("EPSIEL")) {
			Ecole ecole = new Ecole.Builder().withNom("EPSIEL").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ERA")) {
			Ecole ecole = new Ecole.Builder().withNom("ERA").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ERN")) {
			Ecole ecole = new Ecole.Builder().withNom("ERN").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ESITH")) {
			Ecole ecole = new Ecole.Builder().withNom("ESITH").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ESTA")) {
			Ecole ecole = new Ecole.Builder().withNom("ESTA").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ESTC")) {
			Ecole ecole = new Ecole.Builder().withNom("ESTC").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("EST-Essaouira")) {
			Ecole ecole = new Ecole.Builder().withNom("EST-Essaouira").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ESTEM")) {
			Ecole ecole = new Ecole.Builder().withNom("ESTEM").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("EST-Fès")) {
			Ecole ecole = new Ecole.Builder().withNom("EST-Fès").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ESTM")) {
			Ecole ecole = new Ecole.Builder().withNom("ESTM").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ESTO")) {
			Ecole ecole = new Ecole.Builder().withNom("ESTO").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ESTS")) {
			Ecole ecole = new Ecole.Builder().withNom("ESTS").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("High Tech ingénierie")) {
			Ecole ecole = new Ecole.Builder().withNom("High Tech ingénierie").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("IAV")) {
			Ecole ecole = new Ecole.Builder().withNom("IAV").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("IIHEM")) {
			Ecole ecole = new Ecole.Builder().withNom("IIHEM").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("IMM")) {
			Ecole ecole = new Ecole.Builder().withNom("IMM").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("IGA")) {
			Ecole ecole = new Ecole.Builder().withNom("IGA").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("INPT")) {
			Ecole ecole = new Ecole.Builder().withNom("INPT").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("INSEA")) {
			Ecole ecole = new Ecole.Builder().withNom("INSEA").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("Institut Supérieur Vinci")) {
			Ecole ecole = new Ecole.Builder().withNom("Institut Supérieur Vinci").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("INSUP")) {
			Ecole ecole = new Ecole.Builder().withNom("INSUP").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("ISFORT")) {
			Ecole ecole = new Ecole.Builder().withNom("ISFORT").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("SUPEMIR")) {
			Ecole ecole = new Ecole.Builder().withNom("SUPEMIR").build();
			ecoleRepository.save(ecole);
		}
		if (!ecoleRepository.existsByNom("Sup Télécom")) {
			Ecole ecole = new Ecole.Builder().withNom("Sup Télécom").build();
			ecoleRepository.save(ecole);
		}
	}
}
