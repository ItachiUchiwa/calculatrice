package com.c2e.back.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.dto.EntrepriseDtoLabel;
import com.c2e.back.dto.EtudiantDtoLabel;
import com.c2e.back.dto.OffreDtoProfil;
import com.c2e.back.entity.Entreprise;
import com.c2e.back.entity.Etudiant;
import com.c2e.back.entity.Offre;
import com.c2e.back.mapper.EntrepriseMapper;
import com.c2e.back.mapper.EtudiantMapper;
import com.c2e.back.mapper.OffreMapper;
import com.c2e.back.service.RechercheService;
import com.c2e.back.utils.Recherche;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/recherches")
public class RechercheController {

	private RechercheService rechercheService;

	public RechercheController(RechercheService rechercheService) {
		this.rechercheService = rechercheService;
	}

	@ApiOperation(value = "rechercher les élements parmi les stages")
	@GetMapping("/stages/{elementAchercher}")
	public Recherche searchStages(@PathVariable("elementAchercher") String elementAchercher,
			@RequestParam(value = "nombreElementParPage", defaultValue = "1", required = false) Integer nombreElementParPage,
			@RequestParam(value = "iterationActuelle", defaultValue = "0", required = false) int iterationActuelle) {

		Pageable pageable = PageRequest.of(iterationActuelle, nombreElementParPage);
		Page<Offre> page = this.rechercheService.searchStages(elementAchercher, pageable);

		List<OffreDtoProfil> stageTrouvees = page.getContent().stream().map(OffreMapper::offreToOffreDtoProfil)
				.collect(Collectors.toList());

		return new Recherche.Builder().withListeObjetCherche(stageTrouvees).withIterationActuelle(page.getNumber())
				.withNombreElementParPage(page.getNumberOfElements()).withNombreElementTrouve(page.getTotalElements())
				.withNombreTotalPage(page.getTotalPages()).build();
	}

	@ApiOperation(value = "rechercher les élements parmi les alternances")
	@GetMapping("/alternances/{elementAchercher}")
	public Recherche searchAlternances(@PathVariable("elementAchercher") String elementAchercher,
			@RequestParam(value = "nombreElementParPage", defaultValue = "1", required = false) Integer nombreElementParPage,
			@RequestParam(value = "iterationActuelle", defaultValue = "0", required = false) int iterationActuelle) {

		Pageable pageable = PageRequest.of(iterationActuelle, nombreElementParPage);
		Page<Offre> page = this.rechercheService.searchAlternances(elementAchercher, pageable);
		List<OffreDtoProfil> alternancesTrouves = page.getContent().stream().map(OffreMapper::offreToOffreDtoProfil)
				.collect(Collectors.toList());

		return new Recherche.Builder().withListeObjetCherche(alternancesTrouves).withIterationActuelle(page.getNumber())
				.withNombreElementParPage(page.getNumberOfElements()).withNombreElementTrouve(page.getTotalElements())
				.withNombreTotalPage(page.getTotalPages()).build();
	}

	@ApiOperation(value = "rechercher les élements parmi les emplois")
	@GetMapping("/emplois/{elementAchercher}")
	public Recherche searchEmplois(@PathVariable("elementAchercher") String elementAchercher,
			@RequestParam(value = "nombreElementParPage", defaultValue = "1", required = false) Integer nombreElementParPage,
			@RequestParam(value = "iterationActuelle", defaultValue = "0", required = false) int iterationActuelle) {

		Pageable pageable = PageRequest.of(iterationActuelle, nombreElementParPage);
		Page<Offre> page = this.rechercheService.searchEmplois(elementAchercher, pageable);
		List<OffreDtoProfil> emploisTrouves = page.getContent().stream().map(OffreMapper::offreToOffreDtoProfil)
				.collect(Collectors.toList());

		return new Recherche.Builder().withListeObjetCherche(emploisTrouves).withIterationActuelle(page.getNumber())
				.withNombreElementParPage(page.getNumberOfElements()).withNombreElementTrouve(page.getTotalElements())
				.withNombreTotalPage(page.getTotalPages()).build();
	}

	@ApiOperation(value = "rechercher les élements parmi les appels d'offres")
	@GetMapping("/appelsOffres/{elementAchercher}")
	public Recherche searchAppelsOffres(@PathVariable("elementAchercher") String elementAchercher,
			@RequestParam(value = "nombreElementParPage", defaultValue = "1", required = false) Integer nombreElementParPage,
			@RequestParam(value = "iterationActuelle", defaultValue = "0", required = false) int iterationActuelle) {

		Pageable pageable = PageRequest.of(iterationActuelle, nombreElementParPage);
		Page<Offre> page = this.rechercheService.searchAppelsOffres(elementAchercher, pageable);

		List<OffreDtoProfil> appelsOffresTrouves  = page.getContent().stream()
				.map(OffreMapper::offreToOffreDtoProfil).collect(Collectors.toList());

		return new Recherche.Builder().withListeObjetCherche( appelsOffresTrouves )
				.withIterationActuelle(page.getNumber())
				.withNombreElementParPage(page.getNumberOfElements())
				.withNombreElementTrouve(page.getTotalElements())
				.withNombreTotalPage(page.getTotalPages()).build();
	}
	@ApiOperation(value = "rechercher les élements parmi les étudiants")
	@GetMapping("/linkOffre/{link}")
	public OffreDtoProfil getOffreByLink(@PathVariable("link") String link) {
		Offre offre= this.rechercheService.getOffreByLink(link);
		System.out.println(offre);
		return OffreMapper.offreToOffreDtoProfil(offre);
	}
	@ApiOperation(value = "rechercher les élements parmi les étudiants")
	@GetMapping("/etudiants/{elementAchercher}")
	public Recherche searchEtudiants(@PathVariable("elementAchercher") String elementAchercher,
			@RequestParam(value = "nombreElementParPage", defaultValue = "1", required = false) Integer nombreElementParPage,
			@RequestParam(value = "iterationActuelle", defaultValue = "0", required = false) int iterationActuelle) {

		Pageable pageable = PageRequest.of(iterationActuelle, nombreElementParPage);

		Page<Etudiant> page= this.rechercheService.searchEtudiants(elementAchercher, pageable);
				List<EtudiantDtoLabel> etudiantsTrouves   = page.getContent().stream()
				.map(EtudiantMapper::etudiantToEtudiantDtoLabel).collect(Collectors.toList());

		return new Recherche.Builder().withListeObjetCherche(etudiantsTrouves).withIterationActuelle(page.getNumber())
				.withNombreElementParPage(page.getNumberOfElements()).withNombreElementTrouve(page.getTotalElements())
				.withNombreTotalPage(page.getTotalPages()).build();
	}

	@ApiOperation(value = "rechercher les élements parmi les entreprises")
	@GetMapping("/entreprises/{elementAchercher}")
	public Recherche searchEntreprises(@PathVariable("elementAchercher") String elementAchercher,
			@RequestParam(value = "nombreElementParPage", defaultValue = "1", required = false) Integer nombreElementParPage,
			@RequestParam(value = "iterationActuelle", defaultValue = "0", required = false) int iterationActuelle) {

		Pageable pageable = PageRequest.of(iterationActuelle, nombreElementParPage);

		Page<Entreprise> page = this.rechercheService.searchEntreprises(elementAchercher, pageable);
		List<EntrepriseDtoLabel> entreprisesTrouvees = page.getContent().stream()
				.map(EntrepriseMapper::entrepriseToEntrepriseDtoLabel).collect(Collectors.toList());

		return new Recherche.Builder().withListeObjetCherche(entreprisesTrouvees).withIterationActuelle(page.getNumber())
				.withNombreElementParPage(page.getNumberOfElements()).withNombreElementTrouve(page.getTotalElements())
				.withNombreTotalPage(page.getTotalPages()).build();
	}
}