package com.c2e.back.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.entity.Certificat;
import com.c2e.back.service.CertificatService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/certificats")
public class CertificatController {

	private CertificatService certificatService;

	public CertificatController(CertificatService certificatService) {
		this.certificatService = certificatService;
	}

	@ApiOperation(value = "Recuperer la liste des certificats qui contiennent 'mot'  dans leur intitule")
	@GetMapping("/findByMot/{mot}")
	public List<Certificat> findWichContainsMot(@PathVariable("mot") String mot) {

		List<Certificat> certificats = this.certificatService.findAll().stream()
				.filter(certificat -> certificat.getIntitule().toLowerCase().contains(mot.toLowerCase()))
				.collect(Collectors.toList());
		
		if(certificats.size()>2) {
			certificats=certificats.subList(0, 2);
		}
		return certificats;
	}
}
