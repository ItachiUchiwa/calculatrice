package com.c2e.back.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.dto.OffreDtoLabel;
import com.c2e.back.entity.Offre;
import com.c2e.back.mapper.OffreMapper;
import com.c2e.back.service.OffreService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/offres")
public class OffreController {

	private OffreService offreService;
	
	public OffreController(OffreService offreService) {
		this.offreService = offreService;
	}
	
	@ApiOperation("Recupère toutes les offres")
	@GetMapping("")
	public List<Offre> findAll() {
		return this.offreService.findAll();
	}
	 
	@ApiOperation(value = "Supprimer un offre par son id")
	@DeleteMapping("/{id}")
	public void deleteOffreById(@PathVariable("id")Long id){
		this.offreService.deleteById(id);
	}
	
	@ApiOperation(value = "Recuperer un offre par son id")
	@GetMapping("/{id}")
	public OffreDtoLabel getOffreById(@PathVariable("id")Long id){
		return OffreMapper.offreToOffreDtoLabel(this.offreService.getOffreById(id));
	}
	
}