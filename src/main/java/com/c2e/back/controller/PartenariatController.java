package com.c2e.back.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.entity.Partenariat;
import com.c2e.back.service.PartenariatService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/partenariats")
public class PartenariatController {

	private PartenariatService partenariatService;
	
	public PartenariatController(PartenariatService partenariatService) {
		this.partenariatService = partenariatService;
	}
	
	@ApiOperation(value = "Créer une nouveau partenariat")
	@PostMapping("/{idEntrepriseDestinataire}/{idEntrepriseExpediteur}")
	private void demanderPartenariat(@RequestBody Partenariat partenariat,
			@PathVariable("idEntrepriseDestinataire")Long idEntrepriseDestinataire,
			@PathVariable("idEntrepriseExpediteur")Long idEntrepriseExpediteur) {
		
		this.partenariatService.demanderPartenariat(partenariat,idEntrepriseDestinataire,idEntrepriseExpediteur);
		
	}
	
	@ApiOperation(value = "Supprimer un partenariat")
	@DeleteMapping("{idPartenariat}")
	private void deletePartenariatById(@PathVariable("idPartenariat")Long idPartenariat) {
		this.partenariatService.deletePartenariatById(idPartenariat);
	}
}
