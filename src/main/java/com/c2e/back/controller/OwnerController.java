package com.c2e.back.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/owner")
public class OwnerController {

	public OwnerController() {
		// TODO Auto-generated constructor stub
	}
	
	@ApiOperation(value = "voir si le rôle est owner")
	@GetMapping("/role")
	public boolean isOwner() {
		return true;
	}
}
