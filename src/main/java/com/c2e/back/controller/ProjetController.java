package com.c2e.back.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.service.ProjetService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/projets")
public class ProjetController {

	private ProjetService projetService;
	
	public ProjetController(ProjetService projetService) {
		this.projetService = projetService;
	}
	
	@ApiOperation(value = "Supprimer un projet par son id")
	@DeleteMapping("/{id}")
	public void deleteProjectById(@PathVariable("id")Long id){
		this.projetService.deleteById(id);
	}
	
	@ApiOperation(value = "Supprimer un projet personnel par son id")
	@DeleteMapping("/projetsPersonnels/{idProjetPersonnel}")
	public void deleteProjetPersonnelById(@PathVariable("idProjetPersonnel")Long idProjetPersonnel){
		this.projetService.deleteProjetPersonnelById(idProjetPersonnel);
	}
	
	@ApiOperation(value = "Supprimer un une experience professionnelle par son id")
	@DeleteMapping("/experiencesProfessionnelles/{idExperienceProfessionnelle}")
	public void deleteExperienceProfessionnelleById(
			@PathVariable("idExperienceProfessionnelle")Long idExperienceProfessionnelle){
		this.projetService.deleteExperienceProfessionnelleById(idExperienceProfessionnelle);
	}
}
