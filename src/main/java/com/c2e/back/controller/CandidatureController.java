package com.c2e.back.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.entity.Candidature;
import com.c2e.back.exception.EtudiantException.DejaCandidateException;
import com.c2e.back.service.CandidatureService;
import com.c2e.back.validator.EtudiantValidator;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/candidatures")
public class CandidatureController {

	private CandidatureService candidatureService;
	private EtudiantValidator etudiantValidator;

	public CandidatureController(CandidatureService candidatureService,
			EtudiantValidator etudiantValidator) {
		this.candidatureService = candidatureService;
		this.etudiantValidator = etudiantValidator;
	}
	

	@ApiOperation(value = "Recuperer toutes les candidatures")
	@GetMapping("")
	public List<Candidature> findAll() {
		 return this.candidatureService.findAll();
	}
	
	@ApiOperation(value = "Candidater à une offre par id Etudiant et id Offre")
	@PostMapping("/{idOffre}/{idEtudiant}")
	public ResponseEntity<Object> candidater(@PathVariable("idOffre") Long idOffre, @PathVariable("idEtudiant") Long idEtudiant) {
		Map<String, String> message = new HashMap<>();
		try {
			etudiantValidator.validateCandidature(idOffre,idEtudiant);
			this.candidatureService.creerCandidature(idOffre, idEtudiant);
			message.put("message", "APPLIED_SUCCESSFULLY");
			return new ResponseEntity<Object>(message, HttpStatus.ACCEPTED);
		}catch(DejaCandidateException e) {
			message.put("message", e.getMessage());
			return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation(value = "trouver une candidature par son id")
	@GetMapping("/{idCandidature}")
	public Candidature getCandidatureById(@PathVariable("idCandidature") Long idCandidature) {
		return this.candidatureService.getCandidatureById(idCandidature);
	}

	@ApiOperation(value = "Supprimer une candidature par idOffre et idEtudiant")
	@DeleteMapping("/{idOffre}/{idEtudiant}")
	public void deleteCandidatureByIdOffreAndIdEtudiant(@PathVariable("idOffre") Long idOffre,
			@PathVariable("idEtudiant") Long idEtudiant) {
		    this.candidatureService.deleteCandidatureByIdOffreAndIdEtudiant(idOffre, idEtudiant);
	}
	
	@ApiOperation(value = "Supprimer une candidature par son id")
	@DeleteMapping("/{idCandidature}")
	public void deleteCandidatureById(@PathVariable("idCandidature")Long idCandidature) {
		this.candidatureService.deleteCandidatureByIdCandidature(idCandidature);
	}
	
}