package com.c2e.back.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.entity.Competence;
import com.c2e.back.entity.IntituleCompetence;
import com.c2e.back.service.CompetenceService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/competences")
public class CompetenceController {

	private CompetenceService competenceService;
	
	public CompetenceController(CompetenceService competenceService) {
		this.competenceService = competenceService;
	}
	
	@ApiOperation(value = "Recuperer la liste des compétences")
	@GetMapping
	public List<Competence> findAll() {
		return this.competenceService.findAll();
	}
	
	
	@ApiOperation(value = "Recuperer la liste des compétences qui contiennent 'mot'")
	@GetMapping("/findByMot/{mot}")
	public List<IntituleCompetence> findWichContainsMot(@PathVariable("mot")String mot) {
		return this.competenceService.findAllIntituleCompetence().stream()
				.filter(intituleCompetence -> 
				intituleCompetence.getIntitule().toLowerCase().contains(mot.toLowerCase())
				)
				.collect(Collectors.toList());
	}
	
	@ApiOperation("supprimer par idCompetence")
	@DeleteMapping("/{idCompetence}/{idEtudiant}")
	public void deleteCompetence(@PathVariable("idCompetence") Long idCompetence,
			@PathVariable("idEtudiant") Long idEtudiant) {
		this.competenceService.deleteCompetence(idCompetence, idEtudiant);
	}
	
	@ApiOperation("rendre une compétence sevenSkill ou modifier son pourcentage")
	@PatchMapping("/updateSevenSkills/{idCompetenceSevenSkill}/{pourcentageDeMaitrise}")
	public void updateSevenSkills(@PathVariable("idCompetenceSevenSkill")Long idCompetenceSevenSkill,
			@PathVariable("pourcentageDeMaitrise")int pourcentageDeMaitrise) {
		
		this.competenceService.updateSevenSkills(idCompetenceSevenSkill,pourcentageDeMaitrise);
	}
	
	@ApiOperation(value = "Supprimer une competence parmi la liste des sevenSkills de l'étudiant")
	@PatchMapping("/enleverSevenSkills/{idCompetence}")
	public Competence deleteCompetenceFromSevenSkills(@PathVariable("idCompetence")Long idCompetence) {
		return this.competenceService.deleteCompetenceFromSevenSkills(idCompetence);
	}
}
