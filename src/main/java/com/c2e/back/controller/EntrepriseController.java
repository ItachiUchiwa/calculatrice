package com.c2e.back.controller;

import static com.c2e.back.configuration.ValeurGlobale.ABOUT_OFFER;
import static com.c2e.back.configuration.ValeurGlobale.BASE_URL_OF_FRONT;
import static com.c2e.back.configuration.ValeurGlobale.CITY_SKILL_MAIL;
import static com.c2e.back.configuration.ValeurGlobale.LOGIN_PATH;
import static com.c2e.back.configuration.ValeurGlobale.MESSAGE_AFFECTATION_CANDIDATURE;
import static com.c2e.back.configuration.ValeurGlobale.MESSAGE_REGISTRATION_ON_CITY_SKILL_MANAGER;
import static com.c2e.back.configuration.ValeurGlobale.PASSWORD_IS;
import static com.c2e.back.configuration.ValeurGlobale.SUJET_AFFECTATION_CANDIDATURE;
import static com.c2e.back.configuration.ValeurGlobale.SUJET_AJOUT_MANAGER;
import static com.c2e.back.configuration.ValeurGlobale.USERNAME_IS;
import static com.c2e.back.configuration.ValeurGlobale.WITH_IDENTIFIANTS;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.dto.EntrepriseDtoLabel;
import com.c2e.back.dto.EntrepriseDtoProfil;
import com.c2e.back.dto.OffreDtoLabel;
import com.c2e.back.dto.OffreDtoProfil;
import com.c2e.back.entity.Candidature;
import com.c2e.back.entity.Entreprise;
import com.c2e.back.entity.EtapeRecrutement;
import com.c2e.back.entity.Manager;
import com.c2e.back.entity.Offre;
import com.c2e.back.entity.Projet;
import com.c2e.back.entity.TypeOffre;
import com.c2e.back.exception.AppUserException.UserAlreadyExistsException;
import com.c2e.back.exception.EntrepriseException.EmailExists;
import com.c2e.back.exception.EntrepriseException.EmailNotCorrect;
import com.c2e.back.exception.EntrepriseException.LienSiteNotCorrect;
import com.c2e.back.exception.EntrepriseException.NomNotCorrect;
import com.c2e.back.exception.EntrepriseException.NombreCollaborateurNotCorrect;
import com.c2e.back.exception.EntrepriseException.TelephoneNotCorrect;
import com.c2e.back.exception.OffreException.DescriptionPosteOffreNotCorrect;
import com.c2e.back.exception.OffreException.DescriptionProfilOffreNotCorrect;
import com.c2e.back.exception.OffreException.IntituleOffreNotCorrect;
import com.c2e.back.exception.OffreException.TypeOffreNotCorrect;
import com.c2e.back.exception.ProjetException.DescriptionNotCorrect;
import com.c2e.back.exception.ProjetException.IntituleNotCorrect;
import com.c2e.back.mapper.EntrepriseMapper;
import com.c2e.back.mapper.OffreMapper;
import com.c2e.back.service.AccountService;
import com.c2e.back.service.EmailSenderService;
import com.c2e.back.service.EntrepriseService;
import com.c2e.back.validator.AppUserValidator;
import com.c2e.back.validator.EntrepriseValidator;
import com.c2e.back.validator.OffreValidator;
import com.c2e.back.validator.ProjetValidator;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/entreprises")
public class EntrepriseController {

	private EntrepriseService entrepriseService;
	private EntrepriseValidator entrepriseValidator;
	private ProjetValidator projetValidator;
	private OffreValidator offreValidator;
	private AccountService accountService;
	private AppUserValidator appUserValidator;
	private EmailSenderService emailSenderService;

	public EntrepriseController(EntrepriseService entrepriseService, EntrepriseValidator entrepriseValidator,
			ProjetValidator projetValidator, OffreValidator offreValidator, AppUserValidator appUserValidator,
			AccountService accountService, EmailSenderService emailSenderService) {
		this.entrepriseService = entrepriseService;
		this.entrepriseValidator = entrepriseValidator;
		this.projetValidator = projetValidator;
		this.offreValidator = offreValidator;
		this.accountService = accountService;
		this.appUserValidator = appUserValidator;
		this.emailSenderService = emailSenderService;
	}

	@ApiOperation(value = "Récupère une entreprise par son id")
	@GetMapping("/{id}")
	public Entreprise getEntreprise(@PathVariable("id") Long id) {
		return entrepriseService.getEntrepriseById(id);
	}

	@ApiOperation(value = "Récupère une entreprise par son id : EntrepriseDtoProfil")
	@GetMapping("/entrepriseDtoProfil/{id}")
	public EntrepriseDtoProfil getEntrepriseDtoProfilById(@PathVariable("id") Long id) {
		return EntrepriseMapper.entrepriseToEntrepriseDToProfil(entrepriseService.getEntrepriseById(id));
	}

	@ApiOperation(value = "Modifier la partie generalité de l'entreprise")
	@PatchMapping(value="/generalities/{idEntreprise}")
	public ResponseEntity<Object> updateGeneralities(@RequestBody Entreprise entreprise,
			@PathVariable("idEntreprise") Long idEntreprise) {

		Map<String, String> message = new HashMap<>();
		try {
			entrepriseValidator.validate(entreprise);
			entrepriseService.updateGeneralities(idEntreprise, entreprise);
			message.put("message", "GENERALITIES_UPDATED_MESSAGE");
			return new ResponseEntity<Object>(message, HttpStatus.ACCEPTED);
		} catch (EmailExists e) {
			message.put("message", e.getMessage());
		} catch (EmailNotCorrect e) {
			message.put("message", e.getMessage());
		} catch (NombreCollaborateurNotCorrect e) {
			message.put("message", e.getMessage());
		} catch (LienSiteNotCorrect e) {
			message.put("message", e.getMessage());
		} catch (NomNotCorrect e) {
			message.put("message", e.getMessage());
		} catch (TelephoneNotCorrect e) {
			message.put("message", e.getMessage());
		}
		return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ApiOperation(value = "Créer un projet pour l'entreprise avec idEntreprise")
	@PostMapping("/projets/{idEntreprise}")
	public ResponseEntity<Object> creerProjet(@RequestBody Projet projet,
			@PathVariable("idEntreprise") Long idEntreprise) {

		Map<String, String> message = new HashMap<>();

		try {
			projetValidator.validate(projet);
			projet = entrepriseService.creerProjet(projet, idEntreprise);
			return new ResponseEntity<Object>(projet, HttpStatus.ACCEPTED);
		} catch (IntituleNotCorrect e) {
			message.put("message", e.getMessage());
			return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (DescriptionNotCorrect e) {
			message.put("message", e.getMessage());
			return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation(value = "Créer un offre pour l'entreprise avec idEntreprise")
	@PostMapping("/offres/{idEntreprise}")
	public ResponseEntity<Object> creerOffre(@RequestBody Offre offre,
			@PathVariable("idEntreprise") Long idEntreprise) {

		Map<String, String> message = new HashMap<>();
		try {
			offreValidator.validate(offre);
			offre = entrepriseService.creerOffre(offre, idEntreprise);
			return new ResponseEntity<Object>(offre, HttpStatus.ACCEPTED);
		} catch (IntituleOffreNotCorrect e) {
			message.put("message", e.getMessage());
			return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (DescriptionPosteOffreNotCorrect e) {
			message.put("message", e.getMessage());
			return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (DescriptionProfilOffreNotCorrect e) {
			message.put("message", e.getMessage());
			return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (TypeOffreNotCorrect e) {
			message.put("message", e.getMessage());
			return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
		} 

	}

	@ApiOperation(value = "Recuperer les offres d'une entreprise")
	@GetMapping("{idEntreprise}/offres")
	public List<OffreDtoLabel> getOffresByIdEntreprise(@PathVariable("idEntreprise") Long idEntreprise) {
		return this.entrepriseService.findAllOffre(idEntreprise).stream().map(OffreMapper::offreToOffreDtoLabel)
				.collect(Collectors.toList());
	}

	@ApiOperation(value = "Recuperer les offres d'un type donnée d'une entreprise")
	@GetMapping("{idEntreprise}/{typeoffre}")
	public List<OffreDtoLabel> findOffresByTypeAndIdEntreprise(@PathVariable("idEntreprise") Long idEntreprise,
			@PathVariable("typeoffre") TypeOffre typeOffre) {
		return this.entrepriseService.findOffresByTypeAndIdEntreprise(idEntreprise, typeOffre).stream()
				.map(OffreMapper::offreToOffreDtoLabel).collect(Collectors.toList());
	}

	@ApiOperation(value = "Recuperer une entreprise mais en DTO: EntrepriseDtoLabel")
	@GetMapping("/entrepriseDtoLabels/{idEntreprise}")
	public EntrepriseDtoLabel getEntrepriseDtoLabelById(@PathVariable("idEntreprise") Long idEntreprise) {
		return EntrepriseMapper.entrepriseToEntrepriseDtoLabel(entrepriseService.getEntrepriseById(idEntreprise));
	}

	/*
	 * Etapes de recrutements:
	 */

	@ApiOperation(value = "créer une étape de recrutement")
	@PostMapping("/etapeRecrutement/{idEntreprise}/{etapeRecrutement}")
	public EtapeRecrutement creerEtapeDeRecrutement(@PathVariable("idEntreprise") Long idEntreprise,
			@PathVariable("etapeRecrutement") String etapeRecrutement) {
		return this.entrepriseService.creerEtapeDeRecrutement(idEntreprise, etapeRecrutement);
	}

	@ApiOperation(value = "Modifier une une étape de recrutement")
	@PatchMapping("/etapeRecrutement/{idEtapeRecrutement}/{intituleEtapeRecrutement}")
	public EtapeRecrutement modifierEtapeDeRecrutement(@PathVariable("idEtapeRecrutement") Long idEtapeRecrutement,
			@PathVariable("intituleEtapeRecrutement") String intituleEtapeRecrutement) {
		return this.entrepriseService.modifierEtapeDeRecrutement(idEtapeRecrutement, intituleEtapeRecrutement);
	}

	@ApiOperation(value = "Supprimer une une étape de recrutement")
	@DeleteMapping("/etapeRecrutement/{idEtapeRecrutement}")
	public void supprimerEtapeDeRecrutement(@PathVariable("idEtapeRecrutement") Long idEtapeRecrutement) {
		this.entrepriseService.supprimerEtapeDeRecrutement(idEtapeRecrutement);
	}

	@ApiOperation(value = "Recuperer la liste des étapes de recrutement")
	@GetMapping("/etapeRecrutement/{idEntreprise}")
	public List<EtapeRecrutement> getEtapesDeRecrutement(@PathVariable("idEntreprise") Long idEntreprise) {
		List<EtapeRecrutement> etapesRecrutement = this.entrepriseService.getEtapesDeRecrutement(idEntreprise);
		Collections.sort(etapesRecrutement);
		return etapesRecrutement;
	}

	/*
	 * Ajout suppression de manager
	 */
	
	@ApiOperation(value = "Ajouter un nouveau manager")
	@PostMapping("/manager/{idEntreprise}")
	public ResponseEntity<Object> ajouterManager(@PathVariable("idEntreprise") Long idEntreprise, @RequestBody Manager manager) {
		Map<String, String> message = new HashedMap();

		String username = manager.getUsername();
		try {
			appUserValidator.validateEmailSyntaxe(username);
			appUserValidator.validateUsername(username);
			String temporaryPassword = UUID.randomUUID().toString();
			manager.setPassword(temporaryPassword);

			entrepriseService.saveManager(manager,idEntreprise);
			this.entrepriseService.ajouterManager(idEntreprise, manager);
			accountService.addRoleToUser(username, "USER");
			accountService.addRoleToUser(username, "MANAGER");
			

			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setTo(manager.getUsername());
			mailMessage.setSubject(SUJET_AJOUT_MANAGER);
			mailMessage.setFrom(CITY_SKILL_MAIL);
			mailMessage.setText(MESSAGE_REGISTRATION_ON_CITY_SKILL_MANAGER + BASE_URL_OF_FRONT + LOGIN_PATH 
					+ WITH_IDENTIFIANTS + USERNAME_IS + manager.getUsername() + PASSWORD_IS + temporaryPassword);

			emailSenderService.sendEmail(mailMessage);
			return new ResponseEntity<Object>(manager, HttpStatus.ACCEPTED);
		} catch (UserAlreadyExistsException e) {
			message.put("message", e.getMessage());
		} catch (com.c2e.back.exception.EtudiantException.EmailNotCorrect e) {
			message.put("message", e.getMessage());
		}
		return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ApiOperation(value = "Supprimer un manager")
	@DeleteMapping("/manager/{idManager}")
	public void supprimerManager(@PathVariable("idManager") Long idManager) {
		this.entrepriseService.supprimerManager(idManager);
	}
	
	@ApiOperation(value = "liste des managers d'une entreprise")
	@GetMapping("/manager/{idEntreprise}")
	public Collection<Manager> getManagers(@PathVariable("idEntreprise") Long idEntreprise) {
		return this.entrepriseService.getManagers(idEntreprise);
	}
	
	@ApiOperation(value = "Assigner une candidature à un manager")
	@PostMapping("/manager/{idManager}/{idCandidature}")
	public void assignerCandidatureAUnManager(@PathVariable("idManager") Long idManager,
			@PathVariable("idCandidature") Long idCandidature) {
		this.entrepriseService.assignerCandidatureAuManager(idCandidature, idManager);
		
		Manager manager = this.entrepriseService.getManager(idManager);
		Candidature candidature = this.entrepriseService.getCandidature(idCandidature);
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(manager.getUsername());
		mailMessage.setSubject(SUJET_AFFECTATION_CANDIDATURE);
		mailMessage.setFrom(CITY_SKILL_MAIL);
		mailMessage.setText(MESSAGE_AFFECTATION_CANDIDATURE + candidature.getEtudiant().getNom() + ABOUT_OFFER + candidature.getIntituleOffre());

		emailSenderService.sendEmail(mailMessage);

	}
	
	@ApiOperation(value = "get le manager d'un candidat")
	@GetMapping("/manager/candidature/{idManager}")
	public Manager getManager(@PathVariable("idManager") Long idCandidature) {
		Manager manager = this.entrepriseService.getManager(idCandidature);
		manager.setCandidatures(null);
		return manager;
	}
	
	@ApiOperation(value = "get la liste des candidats d'un manager pour une offre donnée")
	@GetMapping("/manager/{idEntreprise}/{idOffre}/{idManager}/")
	public Collection<Manager> getListCandidats(@PathVariable("idEntreprise") Long idEntreprise) {
		return this.entrepriseService.getManagers(idEntreprise);
	}
	
	@ApiOperation(value = "Recuperer les offres d'une entreprise en OffreDtoProfil")
	@GetMapping("/OffreDtoProfil/{idEntreprise}")
	public List<OffreDtoProfil> getOffreDtoProfilByIdEntreprise(@PathVariable("idEntreprise") Long idEntreprise) {
		return this.entrepriseService.findAllOffre(idEntreprise).stream().map(OffreMapper::offreToOffreDtoProfil)
				.collect(Collectors.toList());
	}
	
	@ApiOperation(value = "Fermer une offre")
	@PatchMapping("/offres/fermer/{idEntreprise}/{idOffre}")
	public Offre fermerOffre(@RequestBody Offre offre,
			@PathVariable("idEntreprise") Long idEntreprise,
			@PathVariable("idOffre") Long idOffre) {
		return this.entrepriseService.fermerOffre(idEntreprise,idOffre);
	}
	
	@ApiOperation(value = "ouvrir une offre")
	@PatchMapping("/offres/ouvrir/{idEntreprise}/{idOffre}")
	public Offre ouvrirOffre(@RequestBody Offre offre,
			@PathVariable("idEntreprise") Long idEntreprise,
			@PathVariable("idOffre") Long idOffre) {
		return this.entrepriseService.ouvrirOffre(idEntreprise,idOffre);
	}
	
	@GetMapping("/follow/{idEntreprise}/{idUser}")
	public void follow(@PathVariable("idEntreprise") Long idEntreprise,
			@PathVariable("idUser") Long idUser) {
		this.entrepriseService.follow(idEntreprise,idUser);
	}
	
	@GetMapping("/unFollow/{idEntreprise}/{idUser}")
	public void unFollow(@PathVariable("idEntreprise") Long idEntreprise,
			@PathVariable("idUser") Long idUser) {
		this.entrepriseService.unFollow(idEntreprise,idUser);
	}
	
	@GetMapping("/isFollowed/{idEntreprise}/{idUser}")
	public boolean isFollowed(@PathVariable("idEntreprise") Long idEntreprise,
			@PathVariable("idUser") Long idUser) {
		return this.entrepriseService.isFollowed(idEntreprise,idUser);
	}
	
	@ApiOperation(value = "partager avec ses followers")
	@GetMapping("/share/offers/{idEntreprise}/{idOffer}")
	public void shareWithMyFollowers(@PathVariable("idEntreprise") Long idEntreprise,
			@PathVariable("idOffer") Long idOffer){
		this.entrepriseService.shareWIthMyFollowers(idEntreprise,idOffer);
	}
	
	@ApiOperation(value = "voir si le rôle est manager")
	@GetMapping("/manager/role")
	public boolean isManager() {
		return true;
	}	
	
	@ApiOperation(value = "voir si le rôle est entreprises")
	@GetMapping("/role")
	public boolean isEntreprise() {
		return true;
	}

	
}
