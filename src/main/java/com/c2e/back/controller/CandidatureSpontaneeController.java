package com.c2e.back.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.entity.Candidature;
import com.c2e.back.entity.CandidatureSpontanee;
import com.c2e.back.service.CandidatureService;
import com.c2e.back.service.CandidatureSpontaneeService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/candidatureSpontanees")
public class CandidatureSpontaneeController {

	private CandidatureSpontaneeService candidatureSpontaneeService;

	public CandidatureSpontaneeController(CandidatureSpontaneeService candidatureSpontaneeService) {
		this.candidatureSpontaneeService = candidatureSpontaneeService;
	}

	@ApiOperation(value = "Recuperer toutes les candidatures spontanées")
	@GetMapping("")
	public List<CandidatureSpontanee> findAll() {
		 return this.candidatureSpontaneeService.findAll();
	}
	
	@ApiOperation(value = "Candidater de manière spontanée par id offre et id Etudiant")
	@PostMapping("{idOffre}/{idEtudiant}")
	public void candidater(@RequestBody CandidatureSpontanee candidatureSpontanee,
		@PathVariable("idOffre") Long idOffre,@PathVariable("idEtudiant") Long idEtudiant) {
		
		this.candidatureSpontaneeService.creerCandidature(candidatureSpontanee, idOffre, idEtudiant);
	}

	@ApiOperation(value = "trouver une candidature spontanee par son id")
	@GetMapping("/{idCandidature}")
	public Candidature getCandidatureById(@PathVariable("idCandidature") Long idCandidature) {
		return this.candidatureSpontaneeService.getCandidatureById(idCandidature);
	}

	@ApiOperation(value = "Supprimer une candidature spontanee par idEtudiant")
	@DeleteMapping("/{idOffre}/{idEtudiant}")
	public void deleteCandidatureByIdOffreAndIdEtudiant(@PathVariable("idEtudiant") Long idEtudiant) {
		 this.candidatureSpontaneeService.deleteCandidatureByIdEtudiant(idEtudiant);
	}

}
