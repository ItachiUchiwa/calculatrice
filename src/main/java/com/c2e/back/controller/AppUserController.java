package com.c2e.back.controller;

import static com.c2e.back.configuration.ValeurGlobale.BASE_URL_OF_FRONT;
import static com.c2e.back.configuration.ValeurGlobale.CITY_SKILL_MAIL;
import static com.c2e.back.configuration.ValeurGlobale.GOOGLE_PUBLIC_LINK;
import static com.c2e.back.configuration.ValeurGlobale.LOGIN_PATH;
import static com.c2e.back.configuration.ValeurGlobale.MESSAGE_REGISTRATION_ON_CITY_SKILL_ENTERPRISE;
import static com.c2e.back.configuration.ValeurGlobale.MESSAGE_REGISTRATION_ON_CITY_SKILL_ETUDIANT;
import static com.c2e.back.configuration.ValeurGlobale.OFFRE_SPONTANEE;
import static com.c2e.back.configuration.ValeurGlobale.PASSWORD_RESETED_SUCCESSFULY;
import static com.c2e.back.configuration.ValeurGlobale.RESET_PASSWORD1;
import static com.c2e.back.configuration.ValeurGlobale.RESET_PASSWORD2;
import static com.c2e.back.configuration.ValeurGlobale.SUJET_INSCRIPTION_ENTREPRISE;
import static com.c2e.back.configuration.ValeurGlobale.SUJET_INSCRIPTION_ETUDIANT;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.c2e.back.entity.AppUser;
import com.c2e.back.entity.Entreprise;
import com.c2e.back.entity.EtapeRecrutement;
import com.c2e.back.entity.Etudiant;
import com.c2e.back.entity.Offre;
import com.c2e.back.entity.RegistrationForm;
import com.c2e.back.entity.ResetPasswordForm;
import com.c2e.back.entity.TypeOffre;
import com.c2e.back.exception.AppUserException.PasswordDontEqualsToRepassword;
import com.c2e.back.exception.AppUserException.PasswordSyntaxeException;
import com.c2e.back.exception.AppUserException.TokenExpiredException;
import com.c2e.back.exception.AppUserException.TokenNotExistsException;
import com.c2e.back.exception.AppUserException.UserAlreadyExistsException;
import com.c2e.back.exception.AppUserException.UserNotExist;
import com.c2e.back.exception.EtudiantException.EmailNotCorrect;
import com.c2e.back.service.AccountService;
import com.c2e.back.service.EmailSenderService;
import com.c2e.back.service.EntrepriseService;
import com.c2e.back.validator.AppUserValidator;

import io.swagger.annotations.ApiOperation;
@RestController
@RequestMapping("/account")
public class AppUserController {
	private AccountService accountService;
	private AppUserValidator appUserValidator;
	private EmailSenderService emailSenderService;
	private EntrepriseService entrepriseService;

	public AppUserController(AccountService accountService, AppUserValidator appUserValidator,
			EmailSenderService emailSenderService, EntrepriseService entrepriseService) {
		this.accountService = accountService;
		this.appUserValidator = appUserValidator;
		this.emailSenderService = emailSenderService;
		this.entrepriseService = entrepriseService;
	}

	@PostMapping("/etudiants")
	public ResponseEntity<Object> signUpStudent(@RequestBody RegistrationForm data) {
		Map<String, String> message = new HashedMap();

		String username = data.getUsername();
		try {
			appUserValidator.validateEmailSyntaxe(username);
			appUserValidator.validateUsername(username);
			// AppUser user=accountService.findUserByUsername(username);
			String password = data.getPassword();
			String repassword = data.getRepassword();
			appUserValidator.validatePassword(password);
			appUserValidator.validatePasswordAndRepassword(password, repassword);
			Etudiant newETudiant = new Etudiant.Builder().withEmail(username).build();
			newETudiant.setPassword(password);
			newETudiant.setUsername(username);

			accountService.saveEtudiant(newETudiant);
			accountService.addRoleToUser(username, "USER");
			accountService.addRoleToUser(username, "ETUDIANT");

			// Email message

			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setTo(newETudiant.getUsername());
			mailMessage.setSubject(SUJET_INSCRIPTION_ETUDIANT);
			mailMessage.setFrom(CITY_SKILL_MAIL);
			mailMessage.setText(MESSAGE_REGISTRATION_ON_CITY_SKILL_ETUDIANT + BASE_URL_OF_FRONT + LOGIN_PATH);

			emailSenderService.sendEmail(mailMessage);

			return new ResponseEntity<Object>(newETudiant, HttpStatus.ACCEPTED);
		} catch (UserAlreadyExistsException e) {
			message.put("message", e.getMessage());
		} catch (PasswordDontEqualsToRepassword e) {
			message.put("message", e.getMessage());
		} catch (EmailNotCorrect e) {
			message.put("message", e.getMessage());
		} catch (PasswordSyntaxeException e) {
			message.put("message", e.getMessage());
		}
		return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@GetMapping("/id/{username}")
	public Long getUserId(@PathVariable("username") String username) {
		AppUser user = this.accountService.findUserByUsername(username);
		if (user != null) {
			return user.getId();
		} else {
			return null;
		}
	}

	@PostMapping("/entreprises")
	public ResponseEntity<Object> signUpEntreprise(@RequestBody RegistrationForm data) {
		Map<String, String> message = new HashedMap();

		String username = data.getUsername();
		try {
			appUserValidator.validateEmailSyntaxe(username);
			appUserValidator.validateUsername(username);
			// AppUser user=accountService.findUserByUsername(username);
			String password = data.getPassword();
			String repassword = data.getRepassword();
			appUserValidator.validatePassword(password);
			appUserValidator.validatePasswordAndRepassword(password, repassword);

			// toutes les entreprises ont par dfaut une offre spontanée
			Offre SPONTANNEE = new Offre.Builder().withIntitule(OFFRE_SPONTANEE).withTypeOffre(TypeOffre.SPONTANEE)
					.build();
			accountService.saveOffreSpontanee(SPONTANNEE);
			Entreprise newEntreprise = new Entreprise.Builder().withEmail(username).build();
			newEntreprise.setPassword(password);
			newEntreprise.setUsername(username);

			accountService.saveEntreprise(newEntreprise, SPONTANNEE);
			accountService.addRoleToUser(username, "USER");
			accountService.addRoleToUser(username, "MANAGER");
			accountService.addRoleToUser(username, "ENTREPRISE");

			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setTo(newEntreprise.getUsername());
			mailMessage.setSubject(SUJET_INSCRIPTION_ENTREPRISE);
			mailMessage.setFrom(CITY_SKILL_MAIL);
			mailMessage.setText(MESSAGE_REGISTRATION_ON_CITY_SKILL_ENTERPRISE + BASE_URL_OF_FRONT + LOGIN_PATH);

			emailSenderService.sendEmail(mailMessage);
			return new ResponseEntity<Object>(newEntreprise, HttpStatus.ACCEPTED);
		} catch (UserAlreadyExistsException e) {
			message.put("message", e.getMessage());
		} catch (PasswordDontEqualsToRepassword e) {
			message.put("message", e.getMessage());
		} catch (EmailNotCorrect e) {
			message.put("message", e.getMessage());
		} catch (PasswordSyntaxeException e) {
			message.put("message", e.getMessage());
		}
		return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ApiOperation(value = "voir si le rôle est user")
	@GetMapping("/role")
	public boolean isUser() {
		System.out.println("hh");
		return true;
	}

	// Reset password:
	@PostMapping(value = "/forgotPassword/{userEmail}")
	public ResponseEntity<Object> processForgotPasswordForm(@PathVariable("userEmail") String userEmail,
			HttpServletRequest request) {
		Map<String, String> message = new HashedMap();
		try {
			appUserValidator.validateEmailSyntaxe(userEmail);
			appUserValidator.checkUsername(userEmail);
			// Lookup user in database by e-mail
			AppUser user = accountService.findUserByUsername(userEmail);

			// Generate random 36-character string token for reset password
			user.setResetToken(UUID.randomUUID().toString());
			user.setDateExpiredToken(LocalDateTime.now().plusHours(24l));
			// Save token to database
			accountService.saveUser(user);

			// Email message

			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setTo(user.getUsername());
			mailMessage.setSubject("Password Reset Request");
			mailMessage.setFrom("cityskills.not.reply@gmail.com");
			mailMessage.setText(RESET_PASSWORD1 + BASE_URL_OF_FRONT + RESET_PASSWORD2 + user.getResetToken());

			emailSenderService.sendEmail(mailMessage);

			return new ResponseEntity<Object>(userEmail, HttpStatus.OK);

		} catch (UserNotExist e) {
			message.put("message", e.getMessage());
		} catch (EmailNotCorrect e) {
			message.put("message", e.getMessage());
		}
		return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@GetMapping(value = "/reset/{token}")
	public ResponseEntity<Object> displayResetPasswordPage(@PathVariable("token") String token) {

		Map<String, String> message = new HashedMap();

		try {
			appUserValidator.checkTokenExists(token);
			appUserValidator.checkTokenExpired(token);
			AppUser user = accountService.findUserByResetToken(token).get();
			return new ResponseEntity<Object>(token, HttpStatus.OK);

		} catch (TokenNotExistsException e) {
			message.put("message", e.getMessage());
		} catch (TokenExpiredException e) {
			message.put("message", e.getMessage());
		}
		return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(value = "/resetPassword")
	public ResponseEntity<Object> setNewPassword(@RequestBody ResetPasswordForm data) {
		Map<String, String> message = new HashedMap();
		System.out.println(data);
		try {
			appUserValidator.checkTokenExists(data.getToken());
			appUserValidator.checkTokenExpired(data.getToken());
			appUserValidator.validatePassword(data.getPassword());
			appUserValidator.validatePasswordAndRepassword(data.getPassword(), data.getRepassword());

			AppUser user = accountService.findUserByResetToken(data.getToken()).get();

			user.setPassword(data.getPassword());

			user.setResetToken(null);
			user.setDateExpiredToken(null);
			accountService.saveUser(user);

			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setTo(user.getUsername());
			mailMessage.setSubject("Password Reset Request");
			mailMessage.setFrom("cityskills.not.reply@gmail.com");
			mailMessage.setText(PASSWORD_RESETED_SUCCESSFULY);

			emailSenderService.sendEmail(mailMessage);

			return new ResponseEntity<Object>(user, HttpStatus.OK);

		} catch (TokenNotExistsException e) {
			message.put("message", e.getMessage());
		} catch (TokenExpiredException e) {
			message.put("message", e.getMessage());
		} catch (PasswordSyntaxeException e) {
			message.put("message", e.getMessage());
		} catch (PasswordDontEqualsToRepassword e) {
			message.put("message", e.getMessage());
		}
		return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping("/changePassword/{idUser}")
	public ResponseEntity<Object> changePasswordWithJWTToken(@PathVariable("idUser") Long idUser,
			@RequestBody ResetPasswordForm data) {
		Map<String, String> message = new HashedMap();
		System.out.println(data);
		try {
			appUserValidator.validatePassword(data.getPassword());
			appUserValidator.validatePasswordAndRepassword(data.getPassword(), data.getRepassword());

			AppUser user = accountService.findUserById(idUser);

			user.setPassword(data.getPassword());
			accountService.saveUser(user);

			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setTo(user.getUsername());
			mailMessage.setSubject("Password Reset Request");
			mailMessage.setFrom("cityskills.not.reply@gmail.com");
			mailMessage.setText(PASSWORD_RESETED_SUCCESSFULY);

			emailSenderService.sendEmail(mailMessage);

			return new ResponseEntity<Object>(user, HttpStatus.OK);

		} catch (PasswordSyntaxeException e) {
			message.put("message", e.getMessage());
		} catch (PasswordDontEqualsToRepassword e) {
			message.put("message", e.getMessage());
		}
		return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@ApiOperation(value = "Recuperer la liste des étapes de recrutement")
	@GetMapping("/etapeRecrutement/{idEntrepriseOuManager}")
	public List<EtapeRecrutement> getEtapesDeRecrutement(
			@PathVariable("idEntrepriseOuManager") Long idEntrepriseOuManager) {
		List<EtapeRecrutement> etapesRecrutement = this.entrepriseService.getEtapesDeRecrutement(idEntrepriseOuManager);
		Collections.sort(etapesRecrutement);
		return etapesRecrutement;
	}


	@ApiOperation(value = "Modifier la photo de profil")
	@PostMapping("/profil/{isEntreprise}/{idUser}")
	public String changeProfil(@RequestPart(value = "file") MultipartFile multipartFile,
			@PathVariable("isEntreprise")Boolean isEntreprise, @PathVariable("idUser")Long idUser) throws Exception {
		
		InputStream newProfil = multipartFile.getInputStream();		
		String newImage= null;
		if(this.accountService.existsById(idUser)) {
			AppUser user = accountService.findUserById(idUser);
			String lastFileId = user.getImage();
			String newFileId= accountService.createGoogleFile(isEntreprise, "image/**", "newfile"+idUser.toString(), newProfil );
			accountService.deleteGoogleFileById(lastFileId);	
			
			user.setImage(GOOGLE_PUBLIC_LINK+newFileId);
			accountService.save(user);
			newImage= GOOGLE_PUBLIC_LINK+newFileId;
		}
	 
		return newImage;
	}
	

	@ApiOperation(value = "get la photo de profil")
	@GetMapping("/profil/{idUser}")
	public ResponseEntity<String> getImage(@PathVariable("idUser")Long idUser) {
		AppUser user = accountService.findUserById(idUser);
		String imageLink=user.getImage();
		return new ResponseEntity<String>(imageLink,HttpStatus.OK);
	}
	
}