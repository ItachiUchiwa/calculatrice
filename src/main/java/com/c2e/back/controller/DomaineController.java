package com.c2e.back.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.entity.Domaine;
import com.c2e.back.service.DomaineService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/domaines")
public class DomaineController {

	private DomaineService domaineService;
	
	public DomaineController(DomaineService domaineService) {
		this.domaineService = domaineService;
	}
	
	@ApiOperation(value = "recuperer tous les domaines")
	@GetMapping("")
	public List<Domaine> findAll() {
		return this.domaineService.findAll();
	}
}
