package com.c2e.back.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.dto.EtudiantDtoLabel;
import com.c2e.back.entity.Candidature;
import com.c2e.back.entity.Commentaire;
import com.c2e.back.entity.EtapeRecrutement;
import com.c2e.back.mapper.EtudiantMapper;
import com.c2e.back.service.RHService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/gestion")
public class RHController {

	private RHService rhService;
	
	public RHController(RHService rhService) {
		this.rhService = rhService;
	}
	
	@ApiOperation(value = "Changer etat de candidature")
	@PatchMapping("/etatCandidature/{idEntreprise}/{idCandidature}/{idEtatCandidatureActuel}")
	public EtapeRecrutement changerEtatCandidature(@PathVariable("idEntreprise")Long idEntreprise,
			@PathVariable("idCandidature")Long idCandidature,
			@PathVariable("idEtatCandidatureActuel")Long idEtatCandidatureActuel) {
		
		return this.rhService.changerEtatCandidature(idEntreprise,idCandidature,idEtatCandidatureActuel);
	}
	
	@ApiOperation(value = "Ajouter un commentaire à une candidature")
	@PatchMapping("/commentaires/{idCandidature}")
	public Commentaire ajouterCommentaire(@PathVariable("idCandidature")Long idCandidature,
			@RequestBody Commentaire commentaire) {
		
		return this.rhService.ajouterCommentaire(idCandidature,commentaire);
	}
	
	@ApiOperation(value = "Ajouter un point à une candidature")
	@PatchMapping("/point/{idCandidature}/{point}")
	public Integer ajouterPoint(@PathVariable("idCandidature")Long idCandidature,
			@PathVariable("point")Integer point) {
		
		return this.rhService.ajouterPoint(idCandidature,point);
	}
	
	@ApiOperation(value = "Chercher les candidature d'un manager par intitule offre")
	@GetMapping("/candituresDuManager/{idEntreprise}/{idManager}/{intituleOffre}")
	public List<Candidature> candidatureDuManager(@PathVariable("idEntreprise")Long idEntreprise,
			@PathVariable("idManager")Long idManager,
			@PathVariable("intituleOffre")String intituleOffre) {
		return this.rhService.candidatureDuManager(idEntreprise,idManager,intituleOffre);
	}
	
	@ApiOperation(value = "Chercher les candidature d'un manager par intitule offre")
	@GetMapping("/nombreCandidature/{idEntreprise}/{idManager}/{intituleOffre}")
	public Integer getNombreCandidature(@PathVariable("idEntreprise")Long idEntreprise,
			@PathVariable("idManager")Long idManager,
			@PathVariable("intituleOffre")String intituleOffre) {
		return this.rhService.candidatureDuManager(idEntreprise,idManager,intituleOffre).size();
	}
	
	@ApiOperation(value = "Ajouter aux favoris")
	@GetMapping("/favoris/{idManager}/{idEtudiant}") 
	public void ajouterAuxFavoris(@PathVariable("idManager")Long idManager,
			@PathVariable("idEtudiant")Long idEtudiant) {
		this.rhService.ajouterAuxFavoris(idManager,idEtudiant);
	}
	
	@ApiOperation(value = "get my favoris")
	@GetMapping("/favoris/{idManager}") 
	public List<EtudiantDtoLabel> getMyFavoris(@PathVariable("idManager")Long idManager) {
		return this.rhService.getMyFavoris(idManager).stream().map(EtudiantMapper::etudiantToEtudiantDtoLabel)
				.collect(Collectors.toList());
	}
	
	@ApiOperation(value = "tester si c'est un favoris")
	@GetMapping("/estFavoris/{idManager}/{idEtudiant}") 
	public boolean estFavoris(@PathVariable("idManager")Long idManager,
			@PathVariable("idEtudiant")Long idEtudiant) {
		return this.rhService.estFavoris(idManager,idEtudiant);
	}
	
	@ApiOperation(value = "enlever aux favoris")
	@DeleteMapping("/favoris/{idManager}/{idEtudiant}") 
	public void enleverDesFavoris(@PathVariable("idManager")Long idManager,
			@PathVariable("idEtudiant")Long idEtudiant) {
		this.rhService.enleverDesFavoris(idManager,idEtudiant);
	}
	
	@ApiOperation(value = "voir si le rôle est manager")
	@GetMapping("/role")
	public boolean isManager() {
		return true;
	}	
	
}
