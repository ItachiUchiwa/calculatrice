package com.c2e.back.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.entity.Langue;
import com.c2e.back.service.LangueService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/langues")
public class LangueController {

	private LangueService langueService;

	public LangueController(LangueService langueService) {
		this.langueService = langueService;
	}

	@ApiOperation(value = "Recuperer la liste des langue qui contiennent 'mot' dans leur nom")
	@GetMapping("/findByMot/{mot}")
	public List<Langue> findWichContainsMot(@PathVariable("mot") String mot) {

		List<Langue> langues= langueService.findAll().stream()
				.filter(langue -> langue.getNom().toLowerCase().contains(mot.toLowerCase()))
				.collect(Collectors.toList());
		if(langues.size()>2) {
			langues=langues.subList(0, 2);
		}
		return langues;
	}

}
