package com.c2e.back.controller;

import java.util.Collection;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.dao.FiliereRepository;
import com.c2e.back.entity.Filiere;
import com.c2e.back.service.FiliereService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/filieres")
public class FiliereController {

	private FiliereService filiereService;
	public FiliereController(FiliereService filiereService) {
		this.filiereService = filiereService;
	}
	
	@ApiOperation("Recuperer toutes les filières")
	@GetMapping
	public Collection<Filiere> findAll(){
		return this.filiereService.findAll();
	}
	
}
