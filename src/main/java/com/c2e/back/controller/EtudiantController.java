package com.c2e.back.controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.dto.CandidatureAvancementDto;
import com.c2e.back.entity.Certificat;
import com.c2e.back.entity.Competence;
import com.c2e.back.entity.Etudiant;
import com.c2e.back.entity.ExperienceProfessionnelle;
import com.c2e.back.entity.Langue;
import com.c2e.back.entity.ProjetPersonnel;
import com.c2e.back.exception.CompetenceException.IntituleException;
import com.c2e.back.exception.EcoleException.FiliereNullException;
import com.c2e.back.exception.EntrepriseException.NomNotCorrect;
import com.c2e.back.exception.EntrepriseException.TelephoneNotCorrect;
import com.c2e.back.exception.EtudiantException.EmailExists;
import com.c2e.back.exception.EtudiantException.EmailNotCorrect;
import com.c2e.back.exception.EtudiantException.NomException;
import com.c2e.back.exception.EtudiantException.PrenomException;
import com.c2e.back.exception.EtudiantException.ToMuchSevenSkills;
import com.c2e.back.exception.LangueException.NomLangueException;
import com.c2e.back.exception.ProjetException.DescriptionNotCorrect;
import com.c2e.back.exception.ProjetException.EntrepriseHoteNotCorrect;
import com.c2e.back.exception.ProjetException.IntituleNotCorrect;
import com.c2e.back.exception.ProjetException.NombreJourException;
import com.c2e.back.mapper.CandidatureMapper;
import com.c2e.back.service.EtudiantService;
import com.c2e.back.validator.CertificatValidator;
import com.c2e.back.validator.CompetenceValidator;
import com.c2e.back.validator.EcoleValidator;
import com.c2e.back.validator.EtudiantValidator;
import com.c2e.back.validator.LangueValidator;
import com.c2e.back.validator.ProjetValidator;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/etudiants")
public class EtudiantController {

	private EtudiantService etudiantService;
	private EtudiantValidator etudiantValidator;
	private CompetenceValidator competenceValidator;
	private ProjetValidator projetValidator;
	private LangueValidator langueValidator;
	private CertificatValidator certificatValidator;

	public EtudiantController(EtudiantService etudiantService, EtudiantValidator etudiantValidator,
			EcoleValidator ecoleValidator, CompetenceValidator competenceValidator, ProjetValidator projetValidator,
			LangueValidator langueValidator, CertificatValidator certificatValidator) {
		this.etudiantService = etudiantService;
		this.etudiantValidator = etudiantValidator;
		this.competenceValidator = competenceValidator;
		this.projetValidator = projetValidator;
		this.langueValidator = langueValidator;
		this.certificatValidator = certificatValidator;
	}

	@ApiOperation("Recuperer un étudiant par son id")
	@GetMapping("/{idEtudiant}")
	public Etudiant getEtudiantById(@PathVariable("idEtudiant") Long idEtudiant) {
		return this.etudiantService.getEtudiantById(idEtudiant);
	}

	@ApiOperation("Mettre à jour la partie generalité de l'étudiant")
	@PatchMapping("/generalities/{idEtudiant}")
	public ResponseEntity<Object> updateGeneralities(@RequestBody Etudiant etudiant,
			@PathVariable("idEtudiant") Long idEtudiant) {
		Map<String, String> message = new HashMap<>();

		try {
			etudiantValidator.validate(etudiant);
			etudiantService.updateGeneralities(idEtudiant, etudiant);
			message.put("message", "GENERALITIES_UPDATED_MESSAGE");
			return new ResponseEntity<Object>(message, HttpStatus.ACCEPTED);
		} catch (NomException e) {
			message.put("message", e.getMessage());
		} catch (PrenomException e) {
			message.put("message", e.getMessage());
		} catch (FiliereNullException e) {
			message.put("message", e.getMessage());
		} catch (EmailNotCorrect e) {
			message.put("message", e.getMessage());
		} catch (NomNotCorrect e) {
			message.put("message", e.getMessage());
		} catch (EmailExists e) {
			message.put("message", e.getMessage());
		} catch (TelephoneNotCorrect e) {
			message.put("message", e.getMessage());
		}
		return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ApiOperation("Créer une compétence pour l'étudiant")
	@PostMapping("/competences/{idEtudiant}")
	public ResponseEntity<Object> createCompetence(@RequestBody Competence competence,
			@PathVariable("idEtudiant") Long idEtudiant) {
		Map<String, String> message = new HashMap<>();

		try {
			competenceValidator.validateIntitule(competence.getIntituleCompetence());
			Competence competenceNouvelle = etudiantService.createCompetence(competence, idEtudiant);
			return new ResponseEntity<Object>(competenceNouvelle, HttpStatus.ACCEPTED);
		} catch (IntituleException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ApiOperation(value = "Supprimer une competence parmi la liste des compétences de l'étudiant")
	@DeleteMapping("/competences/{idCompetence}/{idEtudiant}")
	public void deleteCompetence(@PathVariable("idCompetence") Long idCompetence,
			@PathVariable("idEtudiant") Long idEtudiant) {
		this.etudiantService.deleteCompetence(idCompetence, idEtudiant);
	}

	@ApiOperation("rendre une compétence sevenSkill ou modifier son pourcentage")
	@PatchMapping("/updateSevenSkills/{idEtudiant}/{idCompetenceSevenSkill}/{pourcentageDeMaitrise}")
	public ResponseEntity<Object> updateSevenSkills(@PathVariable("idCompetenceSevenSkill") Long idCompetenceSevenSkill,
			@PathVariable("pourcentageDeMaitrise") int pourcentageDeMaitrise,
			@PathVariable("idEtudiant") Long idEtudiant) {
		Map<String, String> message = new HashMap<>();
		try {
			Competence competence = this.etudiantService.updateSevenSkills(idEtudiant, idCompetenceSevenSkill,
					pourcentageDeMaitrise);
			return new ResponseEntity<Object>(competence, HttpStatus.ACCEPTED);
		} catch (ToMuchSevenSkills e) {
			message.put("message", e.getMessage());
		}
		return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ApiOperation(value = "Créer un projet pour etudiant avec idEtudiant")
	@PostMapping("/projetsPersonnels/{idEtudiant}")
	public ResponseEntity<Object> creerProjet(@RequestBody ProjetPersonnel projetPersonnel,
			@PathVariable("idEtudiant") Long idEtudiant) {

		Map<String, String> message = new HashMap<>();

		try {
			projetValidator.validate(projetPersonnel);
			projetValidator.validateNombreJour(projetPersonnel.getNombreJour());
			projetPersonnel = etudiantService.creerProjet(projetPersonnel, idEtudiant);
			return new ResponseEntity<Object>(projetPersonnel, HttpStatus.ACCEPTED);
		} catch (IntituleNotCorrect e) {
			message.put("message", e.getMessage());
			return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (DescriptionNotCorrect e) {
			message.put("message", e.getMessage());
			return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (NombreJourException e) {
			message.put("message", e.getMessage());
			return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation(value = "Créer une experience Professionnelle")
	@PostMapping("/experiencesProfessionnelles/{idEtudiant}")
	public ResponseEntity<Object> creerProjet(@RequestBody ExperienceProfessionnelle experienceProfessionnelle,
			@PathVariable("idEtudiant") Long idEtudiant) {

		Map<String, String> message = new HashMap<>();

		try {
			projetValidator.validate(experienceProfessionnelle);
			projetValidator.validateNombreJour(experienceProfessionnelle.getNombreJour());
			projetValidator.validateEntrepriseHote(experienceProfessionnelle.getEntrepriseHote());
			experienceProfessionnelle = etudiantService.creerExperienceProfessionnelle(experienceProfessionnelle,
					idEtudiant);
			return new ResponseEntity<Object>(experienceProfessionnelle, HttpStatus.ACCEPTED);
		} catch (IntituleNotCorrect e) {
			message.put("message", e.getMessage());
		} catch (DescriptionNotCorrect e) {
			message.put("message", e.getMessage());
		} catch (EntrepriseHoteNotCorrect e) {
			message.put("message", e.getMessage());
		} catch (NombreJourException e) {
			message.put("message", e.getMessage());
		}
		return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ApiOperation(value = "Créer une langue pour l'étudiant")
	@PostMapping("/langues/{idEtudiant}")
	public ResponseEntity<Object> creerLangue(@RequestBody Langue langue, @PathVariable("idEtudiant") Long idEtudiant) {

		Map<String, String> message = new HashMap<>();

		try {
			langueValidator.validate(langue);
			Langue newLangue=etudiantService.creerUneLangue(langue, idEtudiant);
			return new ResponseEntity<Object>(newLangue, HttpStatus.ACCEPTED);
		} catch (NomLangueException e) {
			message.put("message", e.getMessage());
		}
		return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ApiOperation(value = "Supprimer une langue parmi la liste des langues de l'étudiant")
	@DeleteMapping("/langues/{idLangue}/{idEtudiant}")
	public void deleteLangue(@PathVariable("idLangue") Long idLangue, @PathVariable("idEtudiant") Long idEtudiant) {
		this.etudiantService.deleteFromMyLanguages(idLangue, idEtudiant);
	}

	@ApiOperation(value = "Créer un certificat pour l'étudiant")
	@PostMapping("/certificats/{idEtudiant}")
	public ResponseEntity<Object> creerCertificat(@RequestBody Certificat certificat,
			@PathVariable("idEtudiant") Long idEtudiant) {

		Map<String, String> message = new HashMap<>();

		try {
			certificatValidator.validate(certificat);
			Certificat newCertificat = etudiantService.creerUnCertificat(certificat, idEtudiant);
			return new ResponseEntity<Object>(newCertificat,	HttpStatus.ACCEPTED);
		} catch (com.c2e.back.exception.CertificatException.IntituleException e) {
			message.put("message", e.getMessage());
		}
		return new ResponseEntity<Object>(message, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ApiOperation(value = "Supprimer un certificat parmi la liste des certificats de l'étudiant")
	@DeleteMapping("/certificats/{idCertificat}/{idEtudiant}")
	public void deleteCertificat(@PathVariable("idCertificat") Long idCertificat,
			@PathVariable("idEtudiant") Long idEtudiant) {
		this.etudiantService.deleteFromMyCertificates(idCertificat, idEtudiant);
	}
	
	
	@ApiOperation(value = "Chercher les candidatures d'un etudiant")
	@GetMapping("/candidatures/{idEtudiant}")
	public Collection<CandidatureAvancementDto> getCandidatures(@PathVariable("idEtudiant") Long idEtudiant) {
		return this.etudiantService.getCandidatures(idEtudiant)
				.stream()
				.map(CandidatureMapper::candidatureToCadidatureAvancementDto)
				.collect(Collectors.toList());
	}
	

	@ApiOperation(value = "voir si le rôle est etudiant")
	@GetMapping("/role")
	public boolean iSEtudiant() {
		return true;
	}
}
