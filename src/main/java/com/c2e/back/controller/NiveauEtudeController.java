package com.c2e.back.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.entity.NiveauEtude;
import com.c2e.back.service.NiveauEtudeService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/niveauEtudes")
public class NiveauEtudeController {

	private NiveauEtudeService niveauEtudeService;
	
	public NiveauEtudeController(NiveauEtudeService niveauEtudeService) {
		this.niveauEtudeService = niveauEtudeService;
	}
	
	@ApiOperation(value = "Recuperer la liste complète des niveaux")
	@GetMapping()
	public List<NiveauEtude> findAll() {
		return this.niveauEtudeService.findAll();
	}
}
