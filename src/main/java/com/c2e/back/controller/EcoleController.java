package com.c2e.back.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.c2e.back.dto.EcoleDtoNomId;
import com.c2e.back.dto.FiliereDtoIntituleId;
import com.c2e.back.entity.Ecole;
import com.c2e.back.mapper.EcoleMapper;
import com.c2e.back.mapper.FiliereMapper;
import com.c2e.back.service.EcoleService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/ecoles")
public class EcoleController {

	private EcoleService ecoleService;
	
	public EcoleController(EcoleService ecoleService) {
		this.ecoleService = ecoleService;
	}
	
	@ApiOperation(value = "Recuperer les école mais : EcoleDtoNomId")
	@GetMapping("/ecoleDtoNomIds")
	public List<EcoleDtoNomId> findAll(){
		return this.ecoleService.findAll()
				                .stream()
				                .map(EcoleMapper :: ecoleToEcoleDtoNomId)
				                .collect(Collectors.toList());
	}
	
	@ApiOperation(value = "Recuperer les filières d'une école avec le id de l'école mais : FiliereDtoNomId")
	@GetMapping("/{idEcole}/filieres")
	public List<FiliereDtoIntituleId> findFiliereOfEcole(@PathVariable("idEcole")Long idEcole){
		return this.ecoleService.getById(idEcole)
								.getFilieres()
				                .stream()
				                .map(FiliereMapper :: filiereToFiliereDtoIntituleId)
				                .collect(Collectors.toList());
	}
	
	@ApiOperation(value = "Recuperer les filières d'une école avec le nom de l'école mais : FiliereDtoNomId")
	@GetMapping("/byNom/{nomEcole}/filieres")
	public List<FiliereDtoIntituleId> findFiliereOfEcole(@PathVariable("nomEcole")String nomEcole){
		Ecole ecole=null;
		if(nomEcole != null && !nomEcole.isEmpty()) {
			ecole = this.ecoleService.findByNom(nomEcole);
			if(ecole!=null) {
				return this.ecoleService.findByNom(nomEcole)
						.getFilieres()
		                .stream()
		                .map(FiliereMapper :: filiereToFiliereDtoIntituleId)
		                .collect(Collectors.toList());
			}
				
		}
		return new ArrayList<FiliereDtoIntituleId>();
	}
}
