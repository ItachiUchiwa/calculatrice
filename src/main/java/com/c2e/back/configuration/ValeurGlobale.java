package com.c2e.back.configuration;

public class ValeurGlobale {

	/*
	 * Quelques conditions importantes:
	 * 1-LORS de la création d'une entreprise, il faut créer par défaut une offre spontanée dont l'intitulé sera désigné par la variable globale OFFRE_SPONTANEE
	 *
	 */
	public final static String OFFRE_SPONTANEE = "OFFRE_SPONTANNEE";
	
	 /*
	 * 2-La taille du texte dans le champ motivation ne doit pas depasser 500 cette condition est designé par la variable MAX_MOTIVATION_TEXT
	 */
	public final static int MAX_MOTIVATION_TEXT = 1000;
	
	/*
	 *3-Les valeurs de retour des Exceptions
	 */  
	//A-EntrepriseValidator:
	public final static String NUMBER_COLLABORATOR_EXCEPTION = "NUMBER_COLLABORATOR_EXCEPTION";
	public final static String EMAIL_EXIST_EXCEPTION = "EMAIL_EXIST_EXCEPTION";
	public final static String EMAIL_NOT_CORRECT_EXCEPTION = "EMAIL_NOT_CORRECT_EXCEPTION";
	public final static String LIEN_SITE_NOT_CORRECT_EXCEPTION = "LIEN_SITE_NOT_CORRECT_EXCEPTION";
	public final static String NOM_NOT_CORRECT_EXCEPTION = "NOM_NOT_CORRECT_EXCEPTION";
	public final static String TELEPHONE_NOT_CORRECT_EXCEPTION = "TELEPHONE_NOT_CORRECT_EXCEPTION";
	public final static String TO_MUCH_SEVEN_SKILL_EXCEPTION = "TO_MUCH_SEVEN_SKILL_EXCEPTION";
	//B-ProjetValidator:
	public final static String PROJECT_INTITULE_EXCEPTION = "PROJECT_INTITULE_EXCEPTION";
	public final static String PROJECT_DESCRIPTION_EXCEPTION = "PROJECT_DESCRIPTION_EXCEPTION";
	public final static String ENTREPRISE_HOTE_EXCEPTION = "ENTREPRISE_HOTE_EXCEPTION";
	public final static String NOMBRE_JOUR_EXCEPTION = "NOMBRE_JOUR_EXCEPTION";

	//C-OffreValidator:
	public final static String TYPE_OFFRE_EXCEPTION = "TYPE_OFFRE_EXCEPTION";
	public final static String OFFRE_INTITULE_EXCEPTION = "OFFRE_INTITULE_EXCEPTION";
	public final static String OFFRE_DESCRIPTION_POSTE_EXCEPTION = "OFFRE_DESCRIPTION_POSTE_EXCEPTION";
	public final static String OFFRE_DESCRIPTION_PROFIL_EXCEPTION = "OFFRE_DESCRIPTION_PROFIL_EXCEPTION";
	
	//D-EcoleValidator
	public final static String ECOLE_EXIST_EXCEPTION = "ECOLE_EXIST_EXCEPTION";
	public final static String FILIERE_NULL_EXCEPTION = "FILIERE_NULL_EXCEPTION";
	
	//E-EtudiantValidator
	public final static String NOM_ETUDIANT_EXCEPTION = "NOM_ETUDIANT_EXCEPTION";
	public final static String PRENOM_ETUDIANT_EXCEPTION = "PRENOM_ETUDIANT_EXCEPTION";
	public final static String YOU_ARE_ALREADY_APPLIED = "YOU_ARE_ALREADY_APPLIED";
	//F-Competence Exception:
	public final static String COMPETENCE_INTITULE_EXCEPTION = "COMPETENCE_INTITULE_EXCEPTION";
	
	//G-Certificat
	public final static String INTITULE_NULL_CERTIFICAT = "INTITULE_NULL_CERTIFICAT";
	//H-Langues:
	public final static String NOM_NULL_FOR_LANGUAGE = "NOM_NULL_FOR_LANGUAGE";
	
	//I- AppUser Services	
	public final static String USER_ALREADY_EXIST_EXCEPTION = "USER_ALREADY_EXIST_EXCEPTION";
	public final static String PASSWORD_DONT_EQUALS_TO_REPASSWORD = "PASSWORD_DONT_EQUALS_TO_REPASSWORD";
	public final static String PASSWORD_SYNTAXE_EXCEPTION = "PASSWORD_SYNTAXE_EXCEPTION";
	public final static String USER_NOT_EXIST = "USER_NOT_EXIST";
	public final static String TOKEN_NOT_EXISTS = "TOKEN_NOT_EXISTS";
	public final static String TOKEN_EXPIRED = "TOKEN_EXPIRED";
	//GENERALITIES EXCEPTION
	public final static String GENERAL_ERROR = "GENERAL_ERROR";
	
	
	//URLS
	//public final static String BASE_URL_OF_FRONT ="http://city-skill-frontend-1.s3-website.eu-west-3.amazonaws.com";
	//public final static String BASE_URL_OF_FRONT ="https://city-job.herokuapp.com";
	public final static String BASE_URL_OF_FRONT ="http://localhost:4200";
	public final static String CITY_SKILL_MAIL = "cityskills.not.reply@gmail.com";
	public final static String SPECIFIC_OFFRE="/offre-specific/";
	//Mail messages:
	public final static String SUJET_INSCRIPTION_ETUDIANT = "Registration on city-skills";
	public final static String SUJET_INSCRIPTION_ENTREPRISE = "Registration on city-skills";
	public final static String MESSAGE_REGISTRATION_ON_CITY_SKILL_ETUDIANT ="You have been registered succesfully on city skill. \n Now you can go and update your skills ";
	public final static String MESSAGE_REGISTRATION_ON_CITY_SKILL_ENTERPRISE ="You have been registered succesfully on city skill. \n. Now you can publish your enterprise offers and manage its applies ";
	public final static String LOGIN_PATH = "/login/sign_in";
	public final static String RESET_PASSWORD1 = "To reset your password, click the link below:\n" ;
	public final static String RESET_PASSWORD2 = "/login/new_password/";
	public final static String PASSWORD_RESETED_SUCCESSFULY="Your password is modified successfuly ";
	public final static String SUJET_AJOUT_MANAGER = "New manager";
	public final static String MESSAGE_REGISTRATION_ON_CITY_SKILL_MANAGER = "You have been save as MANAGER on city skill. Please login on :\n";
	public final static String WITH_IDENTIFIANTS = "\n Your identifiants are: \n";
	public final static String USERNAME_IS = "\n Username: ";
	public final static String PASSWORD_IS = "\n Password: ";
	public final static String MESSAGE_EVOLUTION_CANDIDATURE1 = "Your application about  \"";
	public final static String MESSAGE_EVOLUTION_CANDIDATURE2 = "\" has progressed to step \"";
	public final static String SUJET_AFFECTATION_CANDIDATURE = "You have new Application";
	public final static String MESSAGE_AFFECTATION_CANDIDATURE = "You have been assigned the application of \"";
	public final static String ABOUT_OFFER = " regarding the offer of ";
	public final static String MESSAGE_SUPPRESSION_CANDIDATURE1 = "Your application concerning the \"";
	public final static String MESSAGE_SUPPRESSION_CANDIDATURE2 = "\" has just been rejected.";
	public final static String SUJET_NOUVEL_OFFRE = ": Une nouvelle offre - ";
	public final static String MESSAGE_NOUVEL_OFFRE = "Vous avez une nouvelle offre disponible sur \n";
	
	//Google param
	public final static String GOOGLE_CREDENTIAL_PATH="src/main/resources/credentials";
	public final static String GOOGLE_CLIENT_SECRET_FILE_NAME="client_secret.json";
	public final static String GOOGLE_PUBLIC_LINK="https://drive.google.com/uc?export=view&id=";
	public final static String ID_GOOGLE_FOLDER_PROFIL_ENTREPRISE = "13guwdV_Owp0R54_qAoVPPWSeg5jAnuy7";
	public final static String ID_GOOGLE_FOLDER_PROFIL_CANDIDAT= "1D2mvftyQvNdeCS_cu14tdMBX9ZyUU-1B";
}
