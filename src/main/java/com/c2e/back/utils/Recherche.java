package com.c2e.back.utils;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class Recherche {

	private Long nombreElementTrouve;
	private int iterationActuelle;
	private int nombreElementParPage;
	private int nombreTotalDePage;

	private Object listeObjetCherche;

	public Recherche() {
	}

	public Recherche(Builder builder) {
		this.nombreElementTrouve = builder.nombreElementTrouve;
		this.nombreElementParPage = builder.nombreElementParPage;
		this.iterationActuelle = builder.iterationActuelle;
		this.listeObjetCherche = builder.listeObjetCherche;
		this.nombreTotalDePage = builder.nombreTotalDePage;
	}

	public Long getNombreElementTrouve() {
		return nombreElementTrouve;
	}

	public void setNombreElementTrouve(Long nombreElementTrouve) {
		this.nombreElementTrouve = nombreElementTrouve;
	}

	public int getIterationActuelle() {
		return iterationActuelle;
	}

	public void setIterationActuelle(int iterationActuelle) {
		this.iterationActuelle = iterationActuelle;
	}

	public int getNombreElementParPage() {
		return nombreElementParPage;
	}

	public void setNombreElementParPage(int nombreElementParPage) {
		this.nombreElementParPage = nombreElementParPage;
	}

	public Object getListeObjetCherche() {
		return listeObjetCherche;
	}

	public void setListeObjetCherche(Object listeObjetCherche) {
		this.listeObjetCherche = listeObjetCherche;
	}

	public int getNombreTotalDePage() {
		return nombreTotalDePage;
	}

	public void setNombreTotalDePage(int nombreTotalDePage) {
		this.nombreTotalDePage = nombreTotalDePage;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public static class Builder {

		private Long nombreElementTrouve;
		private int iterationActuelle;
		private int nombreElementParPage;
		private int nombreTotalDePage;
		private Object listeObjetCherche;

		public Builder() {
		}

		public Builder withNombreElementTrouve(Long nombreElementTrouve) {
			this.nombreElementTrouve = nombreElementTrouve;
			return this;
		}

		public Builder withIterationActuelle(int iterationActuelle) {
			this.iterationActuelle = iterationActuelle;
			return this;
		}

		public Builder withNombreElementParPage(int nombreElementParPage) {
			this.nombreElementParPage = nombreElementParPage;
			return this;
		}

		public Builder withListeObjetCherche(Object listeObjetCherche) {
			this.listeObjetCherche = listeObjetCherche;
			return this;
		}
		
		public Builder withNombreTotalPage(int nombreTotalDePage) {
			this.nombreTotalDePage = nombreTotalDePage;
			return this;
		}

		public Recherche build() {
			return new Recherche(this);
		}
	}
}
