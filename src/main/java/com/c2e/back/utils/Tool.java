package com.c2e.back.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tool {
	
    private static  Pattern pattern;
    private static Matcher matcher;

	private static final String PASSWORD_PATTERN =  "^(?=.*[0-9])"
										            + "(?=.*[a-z])(?=.*[A-Z])"
										            + "(?=.*[@#$%^&+=])"
										            + "(?=\\S+$).{8,20}$"; 

	public static boolean IsEmailSyntaxCorrect(String email) {
		String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		if (email == null) {
			return false;
		}
		if (!email.matches(regex)) {
			return false;
		}
		return true;
	}

	public static boolean isRightPhoneNumber(String telephone) {
		telephone = telephone.replace('-', ' ');
		Pattern p = Pattern.compile("^\\+(?:[0-9] ?){6,14}[0-9]$");

		Matcher m = p.matcher(telephone);
		if (!(m.find() && m.group().equals(telephone))) {
			m.reset();
			return false;
		}
		return true;

	}

	public static boolean validatePassword(String password) {
		pattern = Pattern.compile(PASSWORD_PATTERN);
		matcher = pattern.matcher(password);
		return matcher.matches();
	}
}
