package com.c2e.back.dto;

import java.time.LocalDate;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.c2e.back.entity.Adresse;
import com.c2e.back.entity.Domaine;

public class EntrepriseDtoLabel {
	private Long id;
	private String nom;
	private Domaine domaine;
	private Adresse adresse;
	private LocalDate dateCreation;
	private Integer nombreCollaborateur;
	private Float chiffreAffaire;
	private String lienSiteWeb;
	private String email;
	private String telephone;
	private String image;
	
	public EntrepriseDtoLabel() {
		// TODO Auto-generated constructor stub
	}

	public EntrepriseDtoLabel(Builder builder) {
		this.id = builder.id;
		this.nom = builder.nom;
		this.domaine = builder.domaine;
		this.adresse = builder.adresse;
		this.dateCreation = builder.dateCreation;
		this.nombreCollaborateur = builder.nombreCollaborateur;
		this.chiffreAffaire = builder.chiffreAffaire;
		this.lienSiteWeb = builder.lienSiteWeb;
		this.email = builder.email;
		this.telephone = builder.telephone;
		this.image = builder.image;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Domaine getDomaine() {
		return domaine;
	}

	public void setDomaine(Domaine domaine) {
		this.domaine = domaine;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public LocalDate getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDate dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Integer getNombreCollaborateur() {
		return nombreCollaborateur;
	}

	public void setNombreCollaborateur(Integer nombreCollaborateur) {
		this.nombreCollaborateur = nombreCollaborateur;
	}

	public Float getChiffreAffaire() {
		return chiffreAffaire;
	}

	public void setChiffreAffaire(Float chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	public String getLienSiteWeb() {
		return lienSiteWeb;
	}

	public void setLienSiteWeb(String lienSiteWeb) {
		this.lienSiteWeb = lienSiteWeb;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public static class Builder {

		private Long id;
		private String nom;
		private Domaine domaine;
		private Adresse adresse;
		private LocalDate dateCreation;
		private Integer nombreCollaborateur;
		private Float chiffreAffaire;
		public String lienSiteWeb;
		private String email;
		public String telephone;
		private String image;

		public Builder() {

		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withNom(String nom) {
			this.nom = nom;
			return this;
		}

		public Builder withDomaine(Domaine domaine) {
			this.domaine = domaine;
			return this;
		}

		public Builder withAdresse(Adresse adresse) {
			this.adresse = adresse;
			return this;
		}

		public Builder withDateCreation(LocalDate dateCreation) {
			this.dateCreation = dateCreation;
			return this;
		}

		public Builder withNombreCollaborateur(Integer nombreCollaborateur) {
			this.nombreCollaborateur = nombreCollaborateur;
			return this;
		}

		public Builder withChiffreAffaire(Float chiffreAffaire) {
			this.chiffreAffaire = chiffreAffaire;
			return this;
		}
		
		public Builder withLienSiteWeb(String lienSiteWeb) {
			this.lienSiteWeb = lienSiteWeb;
			return this;
		}
		
		public Builder withEmail(String email) {
			this.email = email;
			return this;
		}
		
		public Builder withTelephone(String telephone) {
			this.telephone = telephone;
			return this;
		}
		
		public Builder withImage(String image) {
			this.image = image;
			return this;
		}	
		
		
		public EntrepriseDtoLabel build() {
			return new EntrepriseDtoLabel(this);
		}
	}

}
