package com.c2e.back.dto;

import java.time.LocalDateTime;
import java.util.Collection;

import com.c2e.back.entity.Commentaire;
import com.c2e.back.entity.EtapeRecrutement;
import com.c2e.back.entity.TypeOffre;

public class CandidatureAvancementDto {
	private Long id;
	private LocalDateTime dateCandidature;
	private EtapeRecrutement etapeRecrutement;
	private String intituleOffre;
	private String nomEntreprise;
	private String imageEntreprise;
	private Collection<Commentaire> commentaires;
	private Integer point;
	private Long idEntrepriseHote;
	private TypeOffre typeOffre;
	private Long idManager;

	public CandidatureAvancementDto() {
		// TODO Auto-generated constructor stub
	}

	public CandidatureAvancementDto(Builder builder) {
		this.id = builder.id;
		this.dateCandidature = builder.dateCandidature;
		this.etapeRecrutement = builder.etapeRecrutement;
		this.intituleOffre = builder.intituleOffre;
		this.nomEntreprise = builder.nomEntreprise;
		this.imageEntreprise = builder.imageEntreprise;
		this.commentaires = builder.commentaires;
		this.point = builder.point;
		this.idEntrepriseHote = builder.idEntrepriseHote;
		this.typeOffre = builder.typeOffre;
		this.idManager = builder.idManager;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDateCandidature() {
		return dateCandidature;
	}

	public void setDateCandidature(LocalDateTime dateCandidature) {
		this.dateCandidature = dateCandidature;
	}

	public EtapeRecrutement getEtapeRecrutement() {
		return etapeRecrutement;
	}

	public void setEtapeRecrutement(EtapeRecrutement etapeRecrutement) {
		this.etapeRecrutement = etapeRecrutement;
	}

	public String getIntituleOffre() {
		return intituleOffre;
	}

	public void setIntituleOffre(String intituleOffre) {
		this.intituleOffre = intituleOffre;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	public String getImageEntreprise() {
		return imageEntreprise;
	}

	public void setImageEntreprise(String imageEntreprise) {
		this.imageEntreprise = imageEntreprise;
	}

	public Collection<Commentaire> getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(Collection<Commentaire> commentaires) {
		this.commentaires = commentaires;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

	public Long getIdEntrepriseHote() {
		return idEntrepriseHote;
	}

	public void setIdEntrepriseHote(Long idEntrepriseHote) {
		this.idEntrepriseHote = idEntrepriseHote;
	}

	public TypeOffre getTypeOffre() {
		return typeOffre;
	}

	public void setTypeOffre(TypeOffre typeOffre) {
		this.typeOffre = typeOffre;
	}

	public Long getIdManager() {
		return idManager;
	}

	public void setIdManager(Long idManager) {
		this.idManager = idManager;
	}

	public static class Builder {
		private Long id;
		private LocalDateTime dateCandidature;
		private EtapeRecrutement etapeRecrutement;
		private String intituleOffre;
		private String nomEntreprise;
		private String imageEntreprise;
		private Collection<Commentaire> commentaires;
		private Integer point;
		private Long idEntrepriseHote;
		private TypeOffre typeOffre;
		private Long idManager;

		public Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withDateCandidature(LocalDateTime dateCandidature) {
			this.dateCandidature = dateCandidature;
			return this;
		}

		public Builder withEtat(EtapeRecrutement etapeRecrutement) {
			this.etapeRecrutement = etapeRecrutement;
			return this;
		}

		public Builder withIntituleOffre(String intituleOffre) {
			this.intituleOffre = intituleOffre;
			return this;
		}

		public Builder withNomEntreprise(String nomEntreprise) {
			this.nomEntreprise = nomEntreprise;
			return this;
		}

		public Builder withImageEntreprise(String imageEntreprise) {
			this.imageEntreprise = imageEntreprise;
			return this;
		}

		public Builder withCommentaires(Collection<Commentaire> commentaires) {
			this.commentaires = commentaires;
			return this;
		}

		public Builder withIdEntrepriseHote(Long idEntrepriseHote) {
			this.idEntrepriseHote = idEntrepriseHote;
			return this;
		}

		public Builder withTypeOffre(TypeOffre typeOffre) {
			this.typeOffre = typeOffre;
			return this;
		}

		public Builder withPoint(Integer point) {
			this.point = point;
			return this;
		}

		public Builder withIdManager(Long idManager) {
			this.idManager = idManager;
			return this;
		}

		public CandidatureAvancementDto build() {
			return new CandidatureAvancementDto(this);
		}
	}
}