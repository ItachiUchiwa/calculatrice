package com.c2e.back.dto;

import java.time.LocalDateTime;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.c2e.back.entity.EtapeRecrutement;

public class CandidatureDtoLabel {

	private Long id;
	private LocalDateTime dateCandidature;
	private EtapeRecrutement etapeRecrutement;
	private EtudiantDtoLabel etudiant;
	private Long idEntreprise;
	private Long idEntrepriseHote;
	private Long idManager;

	public CandidatureDtoLabel() {
		// TODO Auto-generated constructor stub
	}

	public CandidatureDtoLabel(Builder builder) {
		this.id = builder.id;
		this.dateCandidature = builder.dateCandidature;
		this.etapeRecrutement = builder.etapeRecrutement;
		this.etudiant = builder.etudiant;
		this.idEntreprise = builder.idEntreprise;
		this.idManager = builder.idManager;
		this.idEntrepriseHote = builder.idEntrepriseHote;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDateCandidature() {
		return dateCandidature;
	}

	public void setDateCandidature(LocalDateTime dateCandidature) {
		this.dateCandidature = dateCandidature;
	}

	public EtapeRecrutement getEtatCandidature() {
		return etapeRecrutement;
	}

	public void setEtatCandidature(EtapeRecrutement etapeRecrutement) {
		this.etapeRecrutement = etapeRecrutement;
	}

	public EtudiantDtoLabel getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(EtudiantDtoLabel etudiant) {
		this.etudiant = etudiant;
	}

	public Long getIdEntreprise() {
		return idEntreprise;
	}

	public void setIdEntreprise(Long idEntreprise) {
		this.idEntreprise = idEntreprise;
	}

	public EtapeRecrutement getEtapeRecrutement() {
		return etapeRecrutement;
	}

	public void setEtapeRecrutement(EtapeRecrutement etapeRecrutement) {
		this.etapeRecrutement = etapeRecrutement;
	}

	public Long getIdManager() {
		return idManager;
	}

	public void setIdManager(Long idManager) {
		this.idManager = idManager;
	}

	public Long getIdEntrepriseHote() {
		return idEntrepriseHote;
	}

	public void setIdEntrepriseHote(Long idEntrepriseHote) {
		this.idEntrepriseHote = idEntrepriseHote;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public static class Builder {
		private Long id;
		private LocalDateTime dateCandidature;
		private EtapeRecrutement etapeRecrutement;
		private EtudiantDtoLabel etudiant;
		private Long idEntreprise;
		private Long idManager;
		private Long idEntrepriseHote;

		public Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withDateCandidature(LocalDateTime dateCandidature) {
			this.dateCandidature = dateCandidature;
			return this;
		}

		public Builder withEtat(EtapeRecrutement etapeRecrutement) {
			this.etapeRecrutement = etapeRecrutement;
			return this;
		}

		public Builder withEtudiant(EtudiantDtoLabel etudiant) {
			this.etudiant = etudiant;
			return this;
		}

		public Builder withIdEntreprise(Long idEntreprise) {
			this.idEntreprise = idEntreprise;
			return this;
		}

		public Builder withIdManager(Long idManager) {
			this.idManager = idManager;
			return this;
		}

		public Builder withIdEntrepriseHote(Long idEntrepriseHote) {
			this.idEntrepriseHote = idEntrepriseHote;
			return this;
		}

		public CandidatureDtoLabel build() {
			return new CandidatureDtoLabel(this);
		}
	}
}
