package com.c2e.back.dto;

import java.util.Collection;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.c2e.back.entity.Competence;
import com.c2e.back.entity.Langue;
import com.c2e.back.entity.NiveauEtude;

public class EtudiantDtoLabel {
	private Long id;
	private String nom;
	private String prenom;
	private String nomEcole;
	private String intituleFiliere;
	private NiveauEtude niveauEtudes;
	private Collection<Competence> competences;
	private Collection<Langue> langues;
	private String image;

	public EtudiantDtoLabel() {
		// TODO Auto-generated constructor stub
	}

	public EtudiantDtoLabel(Builder builder) {
		this.competences = builder.competences;
		this.id = builder.id;
		this.langues = builder.langues;
		this.nom = builder.nom;
		this.prenom = builder.prenom;
		this.niveauEtudes = builder.niveauEtudes;
		this.nomEcole = builder.nomEcole;
		this.intituleFiliere = builder.intituleFiliere;
		this.image = builder.image;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNomEcole() {
		return nomEcole;
	}

	public void setNomEcole(String nomEcole) {
		this.nomEcole = nomEcole;
	}

	public String getIntituleFiliere() {
		return intituleFiliere;
	}

	public void setIntituleFiliere(String intituleFiliere) {
		this.intituleFiliere = intituleFiliere;
	}

	public NiveauEtude getNiveauEtudes() {
		return niveauEtudes;
	}

	public void setNiveauEtudes(NiveauEtude niveauEtudes) {
		this.niveauEtudes = niveauEtudes;
	}

	public Collection<Competence> getCompetences() {
		return competences;
	}

	public void setCompetences(Collection<Competence> competences) {
		this.competences = competences;
	}

	public Collection<Langue> getLangues() {
		return langues;
	}

	public void setLangues(Collection<Langue> langues) {
		this.langues = langues;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public static class Builder {
		private Long id;
		private String nom;
		private String prenom;
		private String nomEcole;
		private String intituleFiliere;
		private NiveauEtude niveauEtudes;
		private Collection<Competence> competences;
		private Collection<Langue> langues;
		private String image;

		public Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withNom(String nom) {
			this.nom = nom;
			return this;
		}

		public Builder withPrenom(String prenom) {
			this.prenom = prenom;
			return this;
		}

		public Builder withCompetences(Collection<Competence> competences) {
			this.competences = competences;
			return this;
		}

		public Builder withLangues(Collection<Langue> langues) {
			this.langues = langues;
			return this;
		}

		public Builder withNomEcole(String nomEcole) {
			this.nomEcole = nomEcole;
			return this;
		}

		public Builder withIntituleFiliere(String intituleFiliere) {
			this.intituleFiliere = intituleFiliere;
			return this;
		}

		public Builder withNiveauEtudes(NiveauEtude niveauEtudes) {
			this.niveauEtudes = niveauEtudes;
			return this;
		}
		
		public Builder withImage(String image) {
			this.image = image;
			return this;
		}

		public EtudiantDtoLabel build() {
			return new EtudiantDtoLabel(this);
		}
	}
}