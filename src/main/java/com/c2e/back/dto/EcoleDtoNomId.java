package com.c2e.back.dto;

import java.time.LocalDate;
import java.util.Collection;

import javax.persistence.OneToMany;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.c2e.back.entity.Ecole;
import com.c2e.back.entity.Filiere;
import com.c2e.back.entity.Ecole.Builder;

public class EcoleDtoNomId {
	private Long id;
	private String nom;

	public EcoleDtoNomId() {
	}

	public EcoleDtoNomId(Builder builder) {
		this.id = builder.id;
		this.nom = builder.nom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public static class Builder {
		private Long id;
		private String nom;
		
		public Builder() {
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withNom(String nom) {
			this.nom = nom;
			return this;
		}
		
		public EcoleDtoNomId build() {
			return new EcoleDtoNomId(this);
		}
	}
}
