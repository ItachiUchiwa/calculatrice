package com.c2e.back.dto;

public class FiliereDtoIntituleId {

	private Long id;
	private String intitule;
	
	public FiliereDtoIntituleId() {
		// TODO Auto-generated constructor stub
	}

	public FiliereDtoIntituleId(Builder builder) {
		this.id = builder.id;
		this.intitule = builder.intitule;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	
	public static class Builder {
		private Long id;
		private String intitule;
		
		public Builder() {
			// TODO Auto-generated constructor stub
		}
		
		public Builder withId(Long id) {
			this.id = id;
			return this;
		}
		
		public Builder withIntitule(String intitule) {
			this.intitule = intitule;
			return this;
		}
		
		public FiliereDtoIntituleId build() {
			return new FiliereDtoIntituleId(this);
		}
	}
}