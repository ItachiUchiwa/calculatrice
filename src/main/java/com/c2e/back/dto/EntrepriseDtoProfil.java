package com.c2e.back.dto;

import java.time.LocalDate;
import java.util.Collection;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.c2e.back.entity.Adresse;
import com.c2e.back.entity.Domaine;
import com.c2e.back.entity.EtapeRecrutement;
import com.c2e.back.entity.Partenariat;
import com.c2e.back.entity.Projet;

public class EntrepriseDtoProfil {

	private Long id;
	private String nom;
	private Domaine domaine;
	private Adresse adresse;
	private LocalDate dateCreation;
	private Integer nombreCollaborateur;
	private Float chiffreAffaire;
	private String lienSiteWeb;
	private String email;
	private String telephone;
	private Collection<Projet> projets;
	private Collection<OffreDtoProfil> offres;
	private Collection<EtapeRecrutement> etapeRecrutements;
	private Collection<Partenariat> partenariats;
	private String image;
	
	public EntrepriseDtoProfil() {
		// TODO Auto-generated constructor stub
	}

	public EntrepriseDtoProfil(Builder builder) {
		this.id = builder.id;
		this.nom = builder.nom;
		this.domaine = builder.domaine;
		this.adresse = builder.adresse;
		this.dateCreation = builder.dateCreation;
		this.nombreCollaborateur = builder.nombreCollaborateur;
		this.chiffreAffaire = builder.chiffreAffaire;
		this.lienSiteWeb = builder.lienSiteWeb;
		this.projets = builder.projets;
		this.offres = builder.offres;
		this.etapeRecrutements = builder.etapeRecrutements;
		this.partenariats = builder.partenariats;
		this.email = builder.email;
		this.telephone = builder.telephone;
		this.image = builder.image;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Domaine getDomaine() {
		return domaine;
	}

	public void setDomaine(Domaine domaine) {
		this.domaine = domaine;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public LocalDate getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(LocalDate dateCreation) {
		this.dateCreation = dateCreation;
	}

	public Integer getNombreCollaborateur() {
		return nombreCollaborateur;
	}

	public void setNombreCollaborateur(Integer nombreCollaborateur) {
		this.nombreCollaborateur = nombreCollaborateur;
	}

	public Float getChiffreAffaire() {
		return chiffreAffaire;
	}

	public void setChiffreAffaire(Float chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	public String getLienSiteWeb() {
		return lienSiteWeb;
	}

	public void setLienSiteWeb(String lienSiteWeb) {
		this.lienSiteWeb = lienSiteWeb;
	}
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Collection<Projet> getProjets() {
		return projets;
	}

	public void setProjets(Collection<Projet> projets) {
		this.projets = projets;
	}

	public Collection<OffreDtoProfil> getOffres() {
		return offres;
	}

	public void setOffres(Collection<OffreDtoProfil> offres) {
		this.offres = offres;
	}

	public Collection<EtapeRecrutement> getEtatCandidatures() {
		return etapeRecrutements;
	}

	public void setEtatCandidatures(Collection<EtapeRecrutement> etapeRecrutements) {
		this.etapeRecrutements = etapeRecrutements;
	}

	public Collection<Partenariat> getPartenariats() {
		return partenariats;
	}

	public void setPartenariats(Collection<Partenariat> partenariats) {
		this.partenariats = partenariats;
	}

	public Collection<EtapeRecrutement> getEtapeRecrutements() {
		return etapeRecrutements;
	}

	public void setEtapeRecrutements(Collection<EtapeRecrutement> etapeRecrutements) {
		this.etapeRecrutements = etapeRecrutements;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	public static class Builder {

		public String telephone;
		private Long id;
		private String nom;
		private Domaine domaine;
		private Adresse adresse;
		private LocalDate dateCreation;
		private Integer nombreCollaborateur;
		private Float chiffreAffaire;
		private String lienSiteWeb;
		private Collection<Projet> projets;
		private Collection<OffreDtoProfil> offres;
		private Collection<EtapeRecrutement> etapeRecrutements;
		private Collection<Partenariat> partenariats;
		private String email;
		private String image;
		
		public Builder() {

		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withNom(String nom) {
			this.nom = nom;
			return this;
		}

		public Builder withDomaine(Domaine domaine) {
			this.domaine = domaine;
			return this;
		}

		public Builder withAdresse(Adresse adresse) {
			this.adresse = adresse;
			return this;
		}

		public Builder withDateCreation(LocalDate dateCreation) {
			this.dateCreation = dateCreation;
			return this;
		}

		public Builder withNombreCollaborateur(Integer nombreCollaborateur) {
			this.nombreCollaborateur = nombreCollaborateur;
			return this;
		}

		public Builder withChiffreAffaire(Float chiffreAffaire) {
			this.chiffreAffaire = chiffreAffaire;
			return this;
		}

		public Builder withLienSiteWeb(String lienSiteWeb) {
			this.lienSiteWeb = lienSiteWeb;
			return this;
		}
		
		public Builder withEmail(String email) {
			this.email = email;
			return this;
		}
		
		public Builder withTelephone(String telephone) {
			this.telephone = telephone;
			return this;
		}

		public Builder withProjets(Collection<Projet> projets) {
			this.projets = projets;
			return this;
		}

		public Builder withOffres(Collection<OffreDtoProfil> offres) {
			this.offres = offres;
			return this;
		}

		public Builder withEtatCandidature(EtapeRecrutement etapeRecrutement) {
			this.etapeRecrutements = etapeRecrutements;
			return this;
		}

		public Builder withPartenariats(Collection<Partenariat> partenariats) {
			this.partenariats = partenariats;
			return this;
		}

		public Builder withImage(String image) {
			this.image = image;
			return this;
		}
		
		public EntrepriseDtoProfil build() {
			return new EntrepriseDtoProfil(this);
		}

	}

}
