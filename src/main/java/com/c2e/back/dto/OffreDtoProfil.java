package com.c2e.back.dto;

import java.time.LocalDateTime;
import java.util.Collection;

import com.c2e.back.entity.Adresse;
import com.c2e.back.entity.Competence;
import com.c2e.back.entity.NiveauEtude;
import com.c2e.back.entity.TypeOffre;

public class OffreDtoProfil {

	private Long id;
	private String intitule;
	private LocalDateTime datePublication;
	private LocalDateTime dateFin;
	private Adresse adresseOffre;
	private String descriptionPoste;
	private String descriptionProfil;
	private TypeOffre typeOffre;
	private Collection<NiveauEtude> niveauxEtudes;
	private Collection<Competence> competences;
	private String nomEntreprise;
	private Long idEntreprise;
	private boolean ouvert;
	private String link;
	private String imageEntreprise;
	private boolean shared;

	
	public OffreDtoProfil() {
	}

	public OffreDtoProfil(Builder builder) {
		this.id = builder.id;
		this.intitule = builder.intitule;
		this.datePublication = builder.datePublication;
		this.dateFin = builder.dateFin;
		this.adresseOffre = builder.adresseOffre;
		this.descriptionPoste = builder.descriptionPoste;
		this.descriptionProfil = builder.descriptionProfil;
		this.typeOffre = builder.typeOffre;
		this.niveauxEtudes = builder.niveauxEtudes;
		this.competences = builder.competences;
		this.nomEntreprise = builder.nomEntreprise;
		this.idEntreprise = builder.idEntreprise;
		this.ouvert =builder.ouvert;
		this.link = builder.link;
		this.imageEntreprise=builder.imageEntreprise;
		this.shared = builder.shared;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public LocalDateTime getDatePublication() {
		return datePublication;
	}

	public void setDatePublication(LocalDateTime datePublication) {
		this.datePublication = datePublication;
	}

	public LocalDateTime getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDateTime dateFin) {
		this.dateFin = dateFin;
	}

	public Adresse getAdresseOffre() {
		return adresseOffre;
	}

	public void setAdresseOffre(Adresse adresseOffre) {
		this.adresseOffre = adresseOffre;
	}

	public String getDescriptionPoste() {
		return descriptionPoste;
	}

	public void setDescriptionPoste(String descriptionPoste) {
		this.descriptionPoste = descriptionPoste;
	}

	public String getDescriptionProfil() {
		return descriptionProfil;
	}

	public void setDescriptionProfil(String descriptionProfil) {
		this.descriptionProfil = descriptionProfil;
	}

	public TypeOffre getTypeOffre() {
		return typeOffre;
	}

	public void setTypeOffre(TypeOffre typeOffre) {
		this.typeOffre = typeOffre;
	}

	public Collection<NiveauEtude> getNiveau() {
		return niveauxEtudes;
	}

	public void setNiveau(Collection<NiveauEtude> niveauxEtudes) {
		this.niveauxEtudes = niveauxEtudes;
	}

	public Collection<Competence> getCompetences() {
		return competences;
	}

	public void setCompetences(Collection<Competence> competences) {
		this.competences = competences;
	}

	public Collection<NiveauEtude> getNiveauxEtudes() {
		return niveauxEtudes;
	}

	public void setNiveauxEtudes(Collection<NiveauEtude> niveauxEtudes) {
		this.niveauxEtudes = niveauxEtudes;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	public Long getIdEntreprise() {
		return idEntreprise;
	}

	public void setIdEntreprise(Long idEntreprise) {
		this.idEntreprise = idEntreprise;
	}

	public boolean isOuvert() {
		return ouvert;
	}

	public void setOuvert(boolean ouvert) {
		this.ouvert = ouvert;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getImageEntreprise() {
		return imageEntreprise;
	}

	public void setImageEntreprise(String imageEntreprise) {
		this.imageEntreprise = imageEntreprise;
	}
	
	public boolean isShared() {
		return shared;
	}

	public void setShared(boolean isShared) {
		this.shared = isShared;
	}

	public static class Builder {
		private Long id;
		private String intitule;
		private LocalDateTime datePublication;
		private LocalDateTime dateFin;
		private Adresse adresseOffre;
		private String descriptionPoste;
		private String descriptionProfil;
		private TypeOffre typeOffre;
		private Collection<NiveauEtude> niveauxEtudes;
		private Collection<Competence> competences;
		private String nomEntreprise;
		private Long idEntreprise;
		private boolean ouvert;
		private String link;
		private String imageEntreprise;
		private boolean shared;


		
		public Builder() {
			// TODO Auto-generated constructor stub
		}

		public Builder withId(Long id) {
			this.id = id;
			return this;
		}

		public Builder withIntitule(String intitule) {
			this.intitule = intitule;
			return this;
		}

		public Builder withDatePublication(LocalDateTime datePublication) {
			this.datePublication = datePublication;
			return this;
		}

		public Builder withDateFin(LocalDateTime dateFin) {
			this.dateFin = dateFin;
			return this;
		}

		public Builder withAdresseOffre(Adresse adresseOffre) {
			this.adresseOffre = adresseOffre;
			return this;
		}

		public Builder withDescriptionPoste(String descriptionPoste) {
			this.descriptionPoste = descriptionPoste;
			return this;
		}

		public Builder withDescriptionProfil(String descriptionProfil) {
			this.descriptionProfil = descriptionProfil;
			return this;
		}

		public Builder withTypeOffre(TypeOffre typeOffre) {
			this.typeOffre = typeOffre;
			return this;
		}

		public Builder withNiveauxEtudes(Collection<NiveauEtude> niveauxEtudes) {
			this.niveauxEtudes = niveauxEtudes;
			return this;
		}

		public Builder withCompetences(Collection<Competence> competences) {
			this.competences = competences;
			return this;
		}

		public Builder withNomEntreprise(String nomEntreprise) {
			this.nomEntreprise = nomEntreprise;
			return this;
		}

		public Builder withIdEntreprise(Long idEntreprise) {
			this.idEntreprise = idEntreprise;
			return this;
		}


		public Builder withOuvert(boolean ouvert) {
			this.ouvert = ouvert;
			return this;
		}
		
		public Builder withLink(String link) {
			this.link = link;
			return this;
		}
		
		public Builder withImage(String imageEntreprise) {
			this.imageEntreprise = imageEntreprise;
			return this;
		}
		
		public Builder withIsShared(boolean isShared) {
			this.shared = isShared;
			return this;
		}
		
		public OffreDtoProfil build() {
			return new OffreDtoProfil(this);
		}
	}

}
