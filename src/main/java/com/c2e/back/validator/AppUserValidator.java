package com.c2e.back.validator;

import static com.c2e.back.configuration.ValeurGlobale.EMAIL_NOT_CORRECT_EXCEPTION;
import static com.c2e.back.configuration.ValeurGlobale.PASSWORD_DONT_EQUALS_TO_REPASSWORD;
import static com.c2e.back.configuration.ValeurGlobale.PASSWORD_SYNTAXE_EXCEPTION;
import static com.c2e.back.configuration.ValeurGlobale.TOKEN_EXPIRED;
import static com.c2e.back.configuration.ValeurGlobale.TOKEN_NOT_EXISTS;
import static com.c2e.back.configuration.ValeurGlobale.USER_ALREADY_EXIST_EXCEPTION;
import static com.c2e.back.configuration.ValeurGlobale.USER_NOT_EXIST;

import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

import com.c2e.back.dao.AppUserRepository;
import com.c2e.back.exception.AppUserException.PasswordDontEqualsToRepassword;
import com.c2e.back.exception.AppUserException.PasswordSyntaxeException;
import com.c2e.back.exception.AppUserException.TokenExpiredException;
import com.c2e.back.exception.AppUserException.TokenNotExistsException;
import com.c2e.back.exception.AppUserException.UserAlreadyExistsException;
import com.c2e.back.exception.AppUserException.UserNotExist;
import com.c2e.back.exception.EtudiantException.EmailNotCorrect;
import com.c2e.back.utils.Tool;

@Component
public class AppUserValidator {

	private AppUserRepository appUserRepository;
	
	public AppUserValidator(AppUserRepository appUserRepository) {
		this.appUserRepository = appUserRepository;
	}
	
	public void validateAppUser() {
		
	}
	
	public void validateUsername(String username) throws UserAlreadyExistsException {
		if(this.appUserRepository.existsByUsername(username)) {
			throw new UserAlreadyExistsException(USER_ALREADY_EXIST_EXCEPTION);
		}
	}
	
	public void validateEmailSyntaxe(String email) throws EmailNotCorrect {
		if(!Tool.IsEmailSyntaxCorrect(email)) {
			throw new EmailNotCorrect(EMAIL_NOT_CORRECT_EXCEPTION);
		}
	}
	
	public void validatePassword(String password) throws PasswordSyntaxeException{
		if(!Tool.validatePassword(password)) {
			throw new PasswordSyntaxeException(PASSWORD_SYNTAXE_EXCEPTION);
		}
	}
	public void validatePasswordAndRepassword(String password, String repassword) throws PasswordDontEqualsToRepassword {
		if(!password.equals(repassword)) 
			throw new PasswordDontEqualsToRepassword(PASSWORD_DONT_EQUALS_TO_REPASSWORD);
	}
	
	public void checkUsername(String username) throws UserNotExist {
		if(!this.appUserRepository.existsByUsername(username)) {
			throw new UserNotExist(USER_NOT_EXIST);
		}
	}
	
	public void checkTokenExists(String token) throws TokenNotExistsException {
		if(!this.appUserRepository.existsByResetToken(token)) {
			throw new TokenNotExistsException(TOKEN_NOT_EXISTS);
		}
	}
	
	public void checkTokenExpired(String token) throws TokenExpiredException {
		LocalDateTime date=appUserRepository.findByResetToken(token).get().getDateExpiredToken();
		
		if(LocalDateTime.now().isAfter(date)) {
			throw new TokenExpiredException(TOKEN_EXPIRED);
		}
	}
}
