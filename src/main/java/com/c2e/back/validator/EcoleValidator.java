package com.c2e.back.validator;

import org.springframework.stereotype.Component;

import com.c2e.back.dao.EcoleRepository;
import com.c2e.back.entity.Ecole;
import com.c2e.back.exception.EcoleException.EcoleNotExist;

import static com.c2e.back.configuration.ValeurGlobale.*;
import static com.c2e.back.exception.EcoleException.*;

@Component
public class EcoleValidator {

	private EcoleRepository ecoleRepository;

	public EcoleValidator(EcoleRepository ecoleRepository) {
		this.ecoleRepository = ecoleRepository;
	}

	public void validate(Ecole ecole) throws EcoleNotExist {
		validateEcoleNom(ecole.getNom());
	}

	public void validateEcoleNom(String nom) throws EcoleNotExist {
		if (!ecoleRepository.existsByNom(nom)) {
			throw new EcoleNotExist(ECOLE_EXIST_EXCEPTION);
		}
	}
}
