package com.c2e.back.validator;

import static com.c2e.back.configuration.ValeurGlobale.ENTREPRISE_HOTE_EXCEPTION;
import static com.c2e.back.configuration.ValeurGlobale.NOMBRE_JOUR_EXCEPTION;
import static com.c2e.back.configuration.ValeurGlobale.PROJECT_DESCRIPTION_EXCEPTION;
import static com.c2e.back.configuration.ValeurGlobale.PROJECT_INTITULE_EXCEPTION;

import org.springframework.stereotype.Component;

import com.c2e.back.entity.Projet;
import com.c2e.back.exception.ProjetException.DescriptionNotCorrect;
import com.c2e.back.exception.ProjetException.EntrepriseHoteNotCorrect;
import com.c2e.back.exception.ProjetException.IntituleNotCorrect;
import com.c2e.back.exception.ProjetException.NombreJourException;

@Component
public class ProjetValidator {
	
	public ProjetValidator() {
	}
	
	public void validate(Projet projet) throws IntituleNotCorrect,
	DescriptionNotCorrect{
		
		validateIntitule(projet.getIntitule());
		validateDescription(projet.getDescription());
	}
	

	public void validateIntitule(String intitule) throws IntituleNotCorrect{
		if(intitule == null || intitule.isEmpty()) {
			throw new IntituleNotCorrect(PROJECT_INTITULE_EXCEPTION);
		}
	}
	
	public void validateDescription(String description) throws DescriptionNotCorrect{
		if(description == null || description.isEmpty()) {
			throw new DescriptionNotCorrect(PROJECT_DESCRIPTION_EXCEPTION);
		}
	}
	
	public void validateNombreJour(Integer nombreJour) throws NombreJourException{
		if(nombreJour < 1) {
			throw new NombreJourException(NOMBRE_JOUR_EXCEPTION);
		}
	}
	
	public void validateEntrepriseHote(String entrepriseHote) throws EntrepriseHoteNotCorrect{
		if(entrepriseHote == null || entrepriseHote.isEmpty()) {
			throw new EntrepriseHoteNotCorrect(ENTREPRISE_HOTE_EXCEPTION);
		}
	}
}
