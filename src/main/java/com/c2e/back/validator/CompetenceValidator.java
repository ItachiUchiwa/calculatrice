package com.c2e.back.validator;

import org.springframework.stereotype.Component;

import com.c2e.back.entity.Competence;
import com.c2e.back.entity.IntituleCompetence;
import com.c2e.back.exception.CompetenceException.IntituleException;

import static com.c2e.back.exception.CompetenceException.*;
import static com.c2e.back.configuration.ValeurGlobale.*;

@Component
public class CompetenceValidator {

	public CompetenceValidator() {
	}

	public void validate(Competence competence) throws IntituleException {
		validateIntitule(competence.getIntituleCompetence());
	}

	public void validateIntitule(IntituleCompetence intituleCompetence) throws IntituleException {
		if (intituleCompetence == null || intituleCompetence.getIntitule()==null) {
			throw new IntituleException(COMPETENCE_INTITULE_EXCEPTION);
		}
	}
}
