package com.c2e.back.validator;

import static com.c2e.back.configuration.ValeurGlobale.INTITULE_NULL_CERTIFICAT;

import org.springframework.stereotype.Component;

import com.c2e.back.entity.Certificat;
import com.c2e.back.exception.CertificatException.IntituleException;

@Component
public class CertificatValidator {

	public CertificatValidator() {
		// TODO Auto-generated constructor stub
	}
	
	public void validate(Certificat certificat) throws IntituleException {
		validateIntitule(certificat.getIntitule());
	}
	
	public void validateIntitule(String intitule) throws IntituleException {
		if(intitule == null || intitule.isEmpty()) {
			throw new IntituleException(INTITULE_NULL_CERTIFICAT);
		}
	}
}
