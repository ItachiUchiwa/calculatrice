package com.c2e.back.validator;

import org.springframework.stereotype.Component;

import com.c2e.back.entity.Langue;
import com.c2e.back.exception.LangueException.NomLangueException;

import static com.c2e.back.exception.LangueException.*;
import static com.c2e.back.configuration.ValeurGlobale.*;

@Component
public class LangueValidator {

	public LangueValidator() {
		// TODO Auto-generated constructor stub
	}
	
	public void validate(Langue langue) throws NomLangueException {
		validateNom(langue.getNom());
	}
	
	public void validateNom(String nom) throws NomLangueException {
		if(nom == null || nom.isEmpty()) {
			throw new NomLangueException(NOM_NULL_FOR_LANGUAGE);
		}
	}
}
