package com.c2e.back.validator;

import static com.c2e.back.configuration.ValeurGlobale.ECOLE_EXIST_EXCEPTION;
import static com.c2e.back.configuration.ValeurGlobale.EMAIL_EXIST_EXCEPTION;
import static com.c2e.back.configuration.ValeurGlobale.EMAIL_NOT_CORRECT_EXCEPTION;
import static com.c2e.back.configuration.ValeurGlobale.FILIERE_NULL_EXCEPTION;
import static com.c2e.back.configuration.ValeurGlobale.NOM_ETUDIANT_EXCEPTION;
import static com.c2e.back.configuration.ValeurGlobale.PRENOM_ETUDIANT_EXCEPTION;
import static com.c2e.back.configuration.ValeurGlobale.TELEPHONE_NOT_CORRECT_EXCEPTION;
import static com.c2e.back.configuration.ValeurGlobale.TO_MUCH_SEVEN_SKILL_EXCEPTION;
import static com.c2e.back.configuration.ValeurGlobale.YOU_ARE_ALREADY_APPLIED;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.c2e.back.dao.EcoleRepository;
import com.c2e.back.dao.EtudiantRepository;
import com.c2e.back.dao.OffreRepository;
import com.c2e.back.entity.Candidature;
import com.c2e.back.entity.Etudiant;
import com.c2e.back.entity.Offre;
import com.c2e.back.exception.EcoleException.EcoleNotExist;
import com.c2e.back.exception.EcoleException.FiliereNullException;
import com.c2e.back.exception.EntrepriseException.NomNotCorrect;
import com.c2e.back.exception.EntrepriseException.TelephoneNotCorrect;
import com.c2e.back.exception.EtudiantException.DejaCandidateException;
import com.c2e.back.exception.EtudiantException.EmailExists;
import com.c2e.back.exception.EtudiantException.EmailNotCorrect;
import com.c2e.back.exception.EtudiantException.NomException;
import com.c2e.back.exception.EtudiantException.PrenomException;
import com.c2e.back.exception.EtudiantException.ToMuchSevenSkills;
import com.c2e.back.utils.Tool;

@Component
public class EtudiantValidator {

	private EcoleRepository ecoleRepository;
	private EtudiantRepository etudiantRepository;
	private OffreRepository offreRepository;

	public EtudiantValidator(EcoleRepository ecoleRepository,
			EtudiantRepository etudiantRepository,OffreRepository offreRepository) {
		this.ecoleRepository = ecoleRepository;
		this.etudiantRepository = etudiantRepository;
		this.offreRepository = offreRepository;
	}

	public void validate(Etudiant etudiant ) throws NomException, PrenomException,
	 FiliereNullException, EmailNotCorrect, NomNotCorrect, TelephoneNotCorrect {
		
		validateNom(etudiant.getNom());
		validatePrenom(etudiant.getPrenom());
		validateFiliere(etudiant.getIntituleFiliere());
		validateEmailSyntaxe(etudiant.getEmail());
		validateTelephone(etudiant.getTelephone());
	}

	public void validateNom(String nom) throws NomException {
		if (nom == null || nom.isEmpty()) {
			throw new NomException(NOM_ETUDIANT_EXCEPTION);
		}
	}

	public void validatePrenom(String prenom) throws PrenomException {
		if (prenom == null || prenom.isEmpty()) {
			throw new PrenomException(PRENOM_ETUDIANT_EXCEPTION);
		}
	}
	
	public void validateEcoleNom(String nomEcole) throws EcoleNotExist {
		if(nomEcole == null || nomEcole.isEmpty()) {
			throw new EcoleNotExist(ECOLE_EXIST_EXCEPTION);
		}
		if (!ecoleRepository.existsByNom(nomEcole)) {
			throw new EcoleNotExist(ECOLE_EXIST_EXCEPTION);
		}
	}
	
	public void validateFiliere(String nomFiliere) throws FiliereNullException {
		if(nomFiliere == null || nomFiliere.isEmpty()) {
			throw new FiliereNullException(FILIERE_NULL_EXCEPTION);
		}
	}

	public void validateEmailExistence(String email) throws EmailExists {
		if(email != null && this.etudiantRepository.existsByEmail(email)) {
			throw new EmailExists(EMAIL_EXIST_EXCEPTION);
		}
	}
	
	public void validateEmailSyntaxe(String email) throws EmailNotCorrect {
		if(!Tool.IsEmailSyntaxCorrect(email)) {
			throw new EmailNotCorrect(EMAIL_NOT_CORRECT_EXCEPTION);
		}
	}
	
	public void validateTelephone(String telephone) throws NomNotCorrect, TelephoneNotCorrect {
		if(telephone != null && !Tool.isRightPhoneNumber(telephone)) {
			throw new TelephoneNotCorrect(TELEPHONE_NOT_CORRECT_EXCEPTION);
		}
	}

	public void validateSevenSkills(Etudiant etudiant) throws ToMuchSevenSkills {
		if(etudiant.getCompetences()
				.stream()
				.filter(competence -> competence.isEstSevenSkills()==true)
				.count() >6) {
			throw new ToMuchSevenSkills(TO_MUCH_SEVEN_SKILL_EXCEPTION);
		}
	}
	
	public void validateCandidature(Long idOffre, Long idEtudiant) throws DejaCandidateException {
		Offre offre = offreRepository.getOne(idOffre);
		Optional<Candidature> candidatureOpt=offre.getCandidatures().stream()
			.filter(candidatureF->candidatureF.getEtudiant().getId()==idEtudiant)
			.findAny();
		
		if(candidatureOpt.isPresent()) {
			throw new DejaCandidateException(YOU_ARE_ALREADY_APPLIED);
		}
	}
}
