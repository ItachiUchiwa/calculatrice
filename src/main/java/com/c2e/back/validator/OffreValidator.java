package com.c2e.back.validator;

import static com.c2e.back.configuration.ValeurGlobale.*;

import org.springframework.stereotype.Component;

import com.c2e.back.entity.Offre;
import com.c2e.back.entity.TypeOffre;
import com.c2e.back.exception.OffreException.DescriptionPosteOffreNotCorrect;
import com.c2e.back.exception.OffreException.DescriptionProfilOffreNotCorrect;
import com.c2e.back.exception.OffreException.IntituleOffreNotCorrect;
import com.c2e.back.exception.OffreException.TypeOffreNotCorrect;

import static com.c2e.back.exception.OffreException.*;

@Component
public class OffreValidator {

	public void validate(Offre offre) throws IntituleOffreNotCorrect,
	DescriptionPosteOffreNotCorrect, DescriptionProfilOffreNotCorrect,
	TypeOffreNotCorrect {
		validateTypeOffre(offre.getTypeOffre());
		validateIntitule(offre.getIntitule());
		validateDescriptionPoste(offre.getDescriptionPoste());
		validateDescriptionProfil(offre.getDescriptionProfil());
	}

	public void validateIntitule(String intitule) throws IntituleOffreNotCorrect{
		if(intitule == null || intitule.isEmpty()) {
			throw new IntituleOffreNotCorrect(OFFRE_INTITULE_EXCEPTION);
		}
	}
	
	public void validateDescriptionPoste(String descriptionPoste) throws DescriptionPosteOffreNotCorrect{
		if(descriptionPoste == null || descriptionPoste.isEmpty()) {
			throw new DescriptionPosteOffreNotCorrect(OFFRE_DESCRIPTION_POSTE_EXCEPTION);
		}
	}
	
	public void validateDescriptionProfil(String descriptionProfil) throws DescriptionProfilOffreNotCorrect{
		if(descriptionProfil == null || descriptionProfil.isEmpty()) {
			throw new DescriptionProfilOffreNotCorrect(OFFRE_DESCRIPTION_PROFIL_EXCEPTION);
		}
	}
	
	public void validateTypeOffre(TypeOffre typeOffre) throws TypeOffreNotCorrect {
		if(typeOffre == null) {
			throw new TypeOffreNotCorrect(TYPE_OFFRE_EXCEPTION);
		}
	}
	
}
