package com.c2e.back.validator;

import org.springframework.stereotype.Component;
import org.apache.commons.validator.routines.UrlValidator;

import static com.c2e.back.configuration.ValeurGlobale.*;
import com.c2e.back.dao.EntrepriseRepository;
import com.c2e.back.entity.Entreprise;
import com.c2e.back.exception.EntrepriseException.EmailExists;
import com.c2e.back.exception.EntrepriseException.EmailNotCorrect;
import com.c2e.back.exception.EntrepriseException.LienSiteNotCorrect;
import com.c2e.back.exception.EntrepriseException.NomNotCorrect;
import com.c2e.back.exception.EntrepriseException.NombreCollaborateurNotCorrect;
import com.c2e.back.exception.EntrepriseException.TelephoneNotCorrect;
import com.c2e.back.utils.Tool;

import static com.c2e.back.exception.EntrepriseException.*;

@Component
public class EntrepriseValidator {
	
	private EntrepriseRepository entrepriseRepository;
	
	public EntrepriseValidator(EntrepriseRepository entrepriseRepository) {
		this.entrepriseRepository = entrepriseRepository;
	}
	
	public void validate(Entreprise entreprise) throws EmailNotCorrect,
		NombreCollaborateurNotCorrect, LienSiteNotCorrect, NomNotCorrect, TelephoneNotCorrect {
	
		validateNom(entreprise.getNom());
		validateNombreCollaborateur(entreprise.getNombreCollaborateur());
		validateEmailSyntaxe(entreprise.getEmail());
		validateTelephone(entreprise.getTelephone());
		validateLienSiteWeb(entreprise.getLienSiteWeb());
	}
	
	public void validateNombreCollaborateur(Integer nombreCollaborateur) throws NombreCollaborateurNotCorrect {
	
		if(nombreCollaborateur != null && nombreCollaborateur <= 0) {
			throw new NombreCollaborateurNotCorrect(NUMBER_COLLABORATOR_EXCEPTION);
		}
	}
	
	public void validateEmailExistence(String email) throws EmailExists {
		if(email != null && this.entrepriseRepository.existsByEmail(email)) {
			throw new EmailExists(EMAIL_EXIST_EXCEPTION);
		}
	}
	
	public void validateEmailSyntaxe(String email) throws EmailNotCorrect {
		if(!Tool.IsEmailSyntaxCorrect(email)) {
			throw new EmailNotCorrect(EMAIL_NOT_CORRECT_EXCEPTION);
		}
	}
	
	public void validateLienSiteWeb(String lienSiteWeb) throws LienSiteNotCorrect {
		UrlValidator urlValidator = new UrlValidator();
		if(lienSiteWeb != null && !urlValidator.isValid(lienSiteWeb)) {
			throw new LienSiteNotCorrect(LIEN_SITE_NOT_CORRECT_EXCEPTION);
		}
	}
	
	public void validateNom(String nom) throws NomNotCorrect {
		if(nom == null || nom.isEmpty()) {
			throw new NomNotCorrect(NOM_NOT_CORRECT_EXCEPTION);
		}
	}
	
	public void validateTelephone(String telephone) throws NomNotCorrect, TelephoneNotCorrect {
		if(telephone != null && !Tool.isRightPhoneNumber(telephone)) {
			throw new TelephoneNotCorrect(TELEPHONE_NOT_CORRECT_EXCEPTION);
		}
	}
}
