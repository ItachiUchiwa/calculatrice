package com.c2e.back.mapper;

import com.c2e.back.dto.EcoleDtoNomId;
import com.c2e.back.entity.Ecole;

public class EcoleMapper {

	public static EcoleDtoNomId ecoleToEcoleDtoNomId(Ecole ecole) {
		return new EcoleDtoNomId.Builder()
				.withId(ecole.getId())
				.withNom(ecole.getNom())
				.build();
	}
}
