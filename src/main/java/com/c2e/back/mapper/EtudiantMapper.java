package com.c2e.back.mapper;

import com.c2e.back.dto.EtudiantDtoLabel;
import com.c2e.back.entity.Etudiant;

public class EtudiantMapper {

	public static EtudiantDtoLabel etudiantToEtudiantDtoLabel(Etudiant etudiant) {
		return new EtudiantDtoLabel.Builder()
				.withCompetences(etudiant.getCompetences())
				.withId(etudiant.getId())
				.withIntituleFiliere(etudiant.getIntituleFiliere())
				.withLangues(etudiant.getLangues())
				.withNiveauEtudes(etudiant.getNiveauEtudes())
				.withNom(etudiant.getNom())
				.withNomEcole(etudiant.getNomEcole())
				.withPrenom(etudiant.getPrenom())
				.withImage(etudiant.getImage())
				.build();
	}
}
