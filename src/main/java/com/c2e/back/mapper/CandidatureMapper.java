package com.c2e.back.mapper;

import com.c2e.back.dto.CandidatureAvancementDto;
import com.c2e.back.dto.CandidatureDtoLabel;
import com.c2e.back.entity.Candidature;

public class CandidatureMapper {

	public static CandidatureDtoLabel candidatureToCadidatureDtoLabel(Candidature candidature) {
		return new CandidatureDtoLabel.Builder()
				.withDateCandidature(candidature.getDateCandidature())
				.withEtat(candidature.getEtatCandidature())
				.withEtudiant(EtudiantMapper.etudiantToEtudiantDtoLabel(candidature.getEtudiant()))
				.withId(candidature.getId())
				.withIdEntreprise(candidature.getIdEntreprise())
				.withIdManager(candidature.getIdManager())
				.withIdEntrepriseHote(candidature.getIdEntrepriseHote())
				.build();
	}
	
	public static CandidatureAvancementDto candidatureToCadidatureAvancementDto(Candidature candidature) {
		return new CandidatureAvancementDto.Builder()
				.withDateCandidature(candidature.getDateCandidature())
				.withEtat(candidature.getEtatCandidature())
				.withId(candidature.getId())
				.withCommentaires(candidature.getCommentaires())
				.withImageEntreprise(candidature.getImageEntreprise())
				.withIntituleOffre(candidature.getIntituleOffre())
				.withNomEntreprise(candidature.getNomEntreprise())
				.withPoint(candidature.getPoint())
				.withIdEntrepriseHote(candidature.getIdEntrepriseHote())
				.withTypeOffre(candidature.getTypeOffre())
				.withIdManager(candidature.getIdManager())
				.build();
				
	}
}