package com.c2e.back.mapper;

import com.c2e.back.dto.FiliereDtoIntituleId;
import com.c2e.back.entity.Filiere;

public class FiliereMapper {

	public static FiliereDtoIntituleId filiereToFiliereDtoIntituleId(Filiere filiere) {
		return new FiliereDtoIntituleId.Builder().withId(filiere.getId())
				.withIntitule(filiere.getIntitule())
				.build();
	}
}
