package com.c2e.back.mapper;

import java.util.stream.Collectors;

import com.c2e.back.dto.OffreDtoLabel;
import com.c2e.back.dto.OffreDtoProfil;
import com.c2e.back.entity.Offre;

public class OffreMapper {

	public static OffreDtoProfil offreToOffreDtoProfil(Offre offre) {
		return new OffreDtoProfil.Builder().withAdresseOffre(offre.getAdresseOffre())
				.withCompetences(offre.getCompetences()).withDateFin(offre.getDateFin())
				.withDatePublication(offre.getDatePublication()).withDescriptionPoste(offre.getDescriptionPoste())
				.withDescriptionProfil(offre.getDescriptionProfil()).withId(offre.getId())
				.withIntitule(offre.getIntitule()).withTypeOffre(offre.getTypeOffre())
				.withNiveauxEtudes(offre.getNiveauxEtudes()).withNomEntreprise(offre.getNomEntreprise())
				.withIdEntreprise(offre.getIdEntreprise())
				.withOuvert(offre.isOuvert())
				.withLink(offre.getLink())
				.withImage(offre.getImageEntreprise())
				.withIsShared(offre.isShared())
				.build();
	}

	public static OffreDtoLabel offreToOffreDtoLabel(Offre offre) {
		return new OffreDtoLabel.Builder().withAdresseOffre(offre.getAdresseOffre())
				.withCandidatures(offre.getCandidatures().stream()
						.map(CandidatureMapper::candidatureToCadidatureDtoLabel).collect(Collectors.toList()))
				.withCompetences(offre.getCompetences()).withDateFin(offre.getDateFin())
				.withDatePublication(offre.getDatePublication()).withDescriptionPoste(offre.getDescriptionPoste())
				.withDescriptionProfil(offre.getDescriptionProfil()).withEcolesConcernees(offre.getEcolesConcernees())
				.withId(offre.getId()).withIntitule(offre.getIntitule()).withTypeOffre(offre.getTypeOffre())
				.withNiveauxEtudes(offre.getNiveauxEtudes())
				.withNomEntreprise(offre.getNomEntreprise())
				.withOuvert(offre.isOuvert())
				.withLink(offre.getLink())
				.withIsShared(offre.isShared())
				.withImage(offre.getImageEntreprise())
				.build();
	}
}
