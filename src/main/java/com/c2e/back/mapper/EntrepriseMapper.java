package com.c2e.back.mapper;

import java.util.Collections;
import java.util.stream.Collectors;

import com.c2e.back.dto.EntrepriseDtoLabel;
import com.c2e.back.dto.EntrepriseDtoProfil;
import com.c2e.back.entity.Entreprise;

public class EntrepriseMapper {
	
	public static EntrepriseDtoProfil entrepriseToEntrepriseDToProfil(Entreprise entreprise) {
		return new EntrepriseDtoProfil.Builder()
				.withAdresse(entreprise.getAdresse())
				.withChiffreAffaire(entreprise.getChiffreAffaire())
				.withDateCreation(entreprise.getDateCreation())
				.withDomaine(entreprise.getDomaine())
				.withId(entreprise.getId())
				.withLienSiteWeb(entreprise.getLienSiteWeb())
				.withEmail(entreprise.getEmail())
				.withNom(entreprise.getNom())
				.withNombreCollaborateur(entreprise.getNombreCollaborateur())
				.withOffres(entreprise.getOffres() == null 
							? Collections.emptyList()
							: entreprise.getOffres().stream()
							.map(OffreMapper::offreToOffreDtoProfil)
							.collect(Collectors.toList()))
				.withProjets(entreprise.getProjets())
				.withTelephone(entreprise.getTelephone())
				.withImage(entreprise.getImage())
				.build();
	}
	
	public static EntrepriseDtoLabel entrepriseToEntrepriseDtoLabel(Entreprise entreprise) {
		return new EntrepriseDtoLabel.Builder()
				.withAdresse(entreprise.getAdresse())
				.withChiffreAffaire(entreprise.getChiffreAffaire())
				.withDateCreation(entreprise.getDateCreation())
				.withDomaine(entreprise.getDomaine())
				.withEmail(entreprise.getEmail())
				.withId(entreprise.getId())
				.withLienSiteWeb(entreprise.getLienSiteWeb())
				.withNom(entreprise.getNom())
				.withNombreCollaborateur(entreprise.getNombreCollaborateur())
				.withTelephone(entreprise.getTelephone())
				.withImage(entreprise.getImage())
				.build();
	}
	
	
	
	
}
