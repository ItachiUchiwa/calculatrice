package com.c2e.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.Competence;

@Repository
public interface CompetenceRepository extends JpaRepository<Competence, Long> {

	@Modifying
	@Query(value="DELETE FROM competence WHERE id =:idCompetence", nativeQuery=true )
	void deleteWithId(@Param("idCompetence")Long idCompetence);

	@Modifying
	@Query(value="DELETE FROM app_user_competences WHERE etudiant_id=:idEtudiant AND "
			+ "competences_id=:idCompetence", nativeQuery=true)
	void deleteCompetenceFromCompetences(@Param("idCompetence")Long idCompetence, 
			@Param("idEtudiant")Long idEtudiant);
}
