package com.c2e.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.c2e.back.entity.AppRole;

public interface AppRoleRepository extends JpaRepository<AppRole, Long>{

	AppRole findByRole(String role);
	boolean existsByRole(String role);
	
}
