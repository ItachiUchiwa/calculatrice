package com.c2e.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.Filiere;

@Repository
public interface FiliereRepository extends JpaRepository<Filiere, Long>{

	boolean existsByIntitule(String intituleFiliere);

	Filiere findByIntitule(String intituleFiliere);

}
