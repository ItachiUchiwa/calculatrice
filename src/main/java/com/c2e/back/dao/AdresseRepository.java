package com.c2e.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.Adresse;

@Repository
public interface AdresseRepository extends JpaRepository<Adresse, Long> {

	@Modifying
	@Query(value="DELETE FROM adresse WHERE id =:idAdresse", nativeQuery=true )
	void deleteWithId(@Param("idAdresse")Long idAdresse);
}
