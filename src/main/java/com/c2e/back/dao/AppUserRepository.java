package com.c2e.back.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.AppUser;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser, Long>{

	AppUser findByUsername(String username);
	boolean existsByUsername(String username);
	Optional<AppUser> findByResetToken(String resetToken);
	boolean existsByResetToken(String token);

}
