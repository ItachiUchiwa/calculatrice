package com.c2e.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.Partenariat;

@Repository
public interface PartenariatRepository extends JpaRepository<Partenariat, Long>{

	@Modifying
	@Query(value="DELETE FROM partenariat WHERE id =:idPartenariat", nativeQuery=true )
	void deleteWithId(@Param("idPartenariat")Long idPartenariat);

}
