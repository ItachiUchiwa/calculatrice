package com.c2e.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.NiveauEtude;

@Repository
public interface NiveauEtudeRepository extends JpaRepository<NiveauEtude, Long> {

	boolean existsByIntitule(String intitule);

	NiveauEtude findByIntitule(String intitule);

}
