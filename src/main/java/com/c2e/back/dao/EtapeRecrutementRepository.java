package com.c2e.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.EtapeRecrutement;

@Repository
public interface EtapeRecrutementRepository extends JpaRepository<EtapeRecrutement, Long>{

	boolean existsByIntitule(String etatCandidature);

	EtapeRecrutement findByIntitule(String etatCandidature);

	@Modifying
	@Query(value="DELETE FROM app_user_etape_recrutements WHERE etape_recrutements_id =:idEtapeRecrutement", nativeQuery=true )
	void deleteWithId(@Param("idEtapeRecrutement")Long idEtapeRecrutement);

}