package com.c2e.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.Domaine;

@Repository
public interface DomaineRepository extends JpaRepository<Domaine, Long>{

	public boolean existsByIntitule(String intitule);
	public Domaine findByIntitule(String intitule);
}
