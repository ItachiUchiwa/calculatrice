package com.c2e.back.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.Offre;
import com.c2e.back.entity.TypeOffre;

@Repository
public interface OffreRepository extends JpaRepository<Offre, Long> {

	Page<Offre> findByTypeOffreAndOuvertAndIntituleContainingIgnoreCaseOrTypeOffreAndOuvertAndDescriptionPosteContainingIgnoreCaseOrTypeOffreAndOuvertAndDescriptionProfilContainingIgnoreCaseOrTypeOffreAndOuvertAndNomEntrepriseContainingIgnoreCaseOrderByDatePublicationDesc(
			TypeOffre typeOffre1,boolean isOuvert1,String elementAchercher1, TypeOffre typeOffre2,boolean isOuvert2,
			String elementAchercher2,TypeOffre typeOffre3,boolean isOuvert3, String elementAchercher3,TypeOffre typeOffre4,boolean isOuvert4, String elementAchercher4, Pageable pageable);

	Page<Offre> findByTypeOffreAndOuvertOrderByDatePublicationDesc(TypeOffre typeOffre,boolean isOuvert, Pageable pageable);

	@Modifying
	@Query(value="DELETE FROM app_user_offres WHERE offres_id =:idOffre", nativeQuery=true )
	void deleteWithId(@Param("idOffre")Long idOffre);

	public Offre findByLink(String link);

}
