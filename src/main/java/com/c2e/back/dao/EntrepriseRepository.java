package com.c2e.back.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.Entreprise;

@Repository
public interface EntrepriseRepository extends JpaRepository<Entreprise,Long> {

	boolean existsByEmail(String email);

	Page<Entreprise> findByNomContainingIgnoreCaseOrDomaineIntituleContainingIgnoreCaseOrTypeRechercheNaturelContainingIgnoreCaseOrderByNomDesc(
			String elementAchercher, String elementAchercher2,String elementAchercher3, Pageable pageable);

	@Modifying
	@Query(value="SELECT * FROM app_user_offres WHERE offres_id = :idOffre", nativeQuery=true)
	Entreprise getEntrepriseCorrespondantAOffre(Long idOffre);

	@Modifying
	@Query(value="INSERT INTO app_user_followers(entreprise_id, followers_id) VALUES (:idEntreprise, :idFollower)", nativeQuery=true)
	void addToFollower(@Param("idEntreprise")Long idEntreprise,
			@Param("idFollower")Long idFollower);
}

