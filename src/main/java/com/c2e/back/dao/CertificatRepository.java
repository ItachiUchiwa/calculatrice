package com.c2e.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.Certificat;

@Repository
public interface CertificatRepository extends JpaRepository<Certificat, Long> {

	boolean existsByIntitule(String intitule);

	Certificat findByIntitule(String intitule);

}
