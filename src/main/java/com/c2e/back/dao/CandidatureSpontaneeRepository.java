package com.c2e.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.CandidatureSpontanee;

@Repository
public interface CandidatureSpontaneeRepository extends JpaRepository<CandidatureSpontanee, Long>{

}
