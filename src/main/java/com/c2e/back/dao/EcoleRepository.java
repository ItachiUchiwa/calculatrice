package com.c2e.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.Ecole;

@Repository
public interface EcoleRepository extends JpaRepository<Ecole,Long> {

	boolean existsByNom(String nom);

	public Ecole findByNom(String nom);
}
