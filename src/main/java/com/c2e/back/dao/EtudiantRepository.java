package com.c2e.back.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.Etudiant;

@Repository
public interface EtudiantRepository extends JpaRepository<Etudiant, Long> {

	@Modifying
	@Query(value="DELETE FROM filiere_etudiants WHERE etudiants_id =:idEtudiant", nativeQuery=true )
	void deleteLastFiliere(@Param("idEtudiant") Long idEtudiant);

	boolean existsByEmail(String email);

	@Modifying
	@Query(value="DELETE FROM app_user_competences WHERE etudiant_id=:idEtudiant AND "
			+ "competences_id=:idCompetence", nativeQuery=true)
	void deleteCompetenceFromCompetences(@Param("idCompetence")Long idCompetence, 
			@Param("idEtudiant")Long idEtudiant);

	@Modifying
	@Query(value="DELETE FROM app_user_langues WHERE etudiant_id=:idEtudiant AND "
			+ "langues_id=:idLangue", nativeQuery=true)
	void deleteFromMyLanguages(@Param("idLangue")Long idLangue, @Param("idEtudiant")Long idEtudiant);

	@Modifying
	@Query(value="DELETE FROM app_user_certificats WHERE etudiant_id=:idEtudiant AND "
			+ "certificats_id=:idCertificat", nativeQuery=true)
	void deleteFromMyCertificates(@Param("idCertificat")Long idCertificat, @Param("idEtudiant")Long idEtudiant);

	Page<Etudiant> findByNomContainingIgnoreCaseOrPrenomContainingIgnoreCaseOrNomEcoleContainingIgnoreCaseOrIntituleFiliereContainingIgnoreCaseOrTypeRechercheNaturelContainingIgnoreCaseOrderByNomDesc(
			String elementAchercher1, String elementAchercher2, String elementAchercher3, String elementAchercher4, String elementAchercher5, Pageable pageable);

	@Override
	Page<Etudiant> findAll(Pageable pageable);
}
