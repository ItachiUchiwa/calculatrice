package com.c2e.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.Langue;

@Repository
public interface LangueRepository extends JpaRepository<Langue, Long> {

	boolean existsByNom(String nom);

	Langue findByNom(String nom);


}
