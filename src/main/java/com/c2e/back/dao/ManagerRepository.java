package com.c2e.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.Manager;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Long>{

	@Modifying 
	@Query(value="DELETE FROM app_user_candidatures WHERE candidatures_id=:idCandidature", nativeQuery=true)
	void supprimerCandidatureSiDejaAssignee(@Param("idCandidature")Long idCandidature);

	@Modifying
	@Query(value="INSERT INTO app_user_candidatures(candidatures_id, manager_id) VALUES (:idCandidature, :idManager)", nativeQuery=true)
	void assignerCandidatureAuManager(@Param("idCandidature")Long idCandidature,
			@Param("idManager")Long idManager);

	@Query(value="SELECT manager, candidature FROM Manager manager, Candidature candidature WHERE candidature.id = :idCandidature")
	Manager getManagerOfCandidature(@Param("idCandidature")Long idCandidature);


	@Modifying
	@Query(value="DELETE FROM app_user_managers WHERE managers_id =:idManager", nativeQuery=true )
	void deleteWithId(@Param("idManager")Long idManager);


}