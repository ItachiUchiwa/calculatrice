package com.c2e.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.Projet;

@Repository
public interface ProjetRepository extends JpaRepository<Projet, Long> {
	@Modifying
	@Query(value="DELETE FROM app_user_projets WHERE projets_id =:idProjet", nativeQuery=true )
	void deleteWithId(@Param("idProjet")Long idProjet);

}
