package com.c2e.back.dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.ProjetPersonnel;

@Repository
public interface ProjetPersonnelRepository extends JpaRepository<ProjetPersonnel, Long> {

	@Modifying
	@Query(value="DELETE FROM app_user_projets_personnels WHERE projets_personnels_id =:idProjetPersonnel", nativeQuery=true )
	void deleteWithId(@Param("idProjetPersonnel")Long idProjetPersonnel);
}
