package com.c2e.back.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.IntituleCompetence;

@Repository
public interface IntituleCompetenceRepository extends JpaRepository<IntituleCompetence, Long>{

	boolean existsByIntitule(String intitule);

	IntituleCompetence findByIntitule(String intitule);

}
