package com.c2e.back.dao;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.c2e.back.entity.Candidature;

@Repository
public interface CandidatureRepository extends JpaRepository<Candidature, Long> {

	Collection<Candidature> findByEtudiantId(Long idEtudiant);

	
	@Modifying
	@Query(value="DELETE FROM app_user_candidatures WHERE candidatures_id =:idCandidature", nativeQuery=true )
	void deleteWithId(@Param("idCandidature")Long idCandidature);


	@Modifying
	@Query(value="DELETE FROM offre_candidatures WHERE candidatures_id =:idCandidature", nativeQuery=true )
	void deleteWithCandidatureIdOnOffre(@Param("idCandidature")Long idCandidature);

}
