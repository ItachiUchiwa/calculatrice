package com.c2e.back.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.c2e.back.dao.CertificatRepository;
import com.c2e.back.entity.Certificat;

@Service
@Transactional
public class CertificatService {

	private CertificatRepository certificatRepository;

	public CertificatService(CertificatRepository certificatRepository) {
		this.certificatRepository = certificatRepository;
	}

	public List<Certificat> findAll() {
		return this.certificatRepository.findAll();
	}

	public Certificat save(Certificat certificat) {
		return this.certificatRepository.save(certificat);
	}

	public Certificat findByIntituleIfExistElseCreate(String intitule) {
		Certificat certificat = null;

		certificat = new Certificat.Builder().withIntitule(intitule).build();
		certificat = this.certificatRepository.save(certificat);
		return certificat;
	}
}
