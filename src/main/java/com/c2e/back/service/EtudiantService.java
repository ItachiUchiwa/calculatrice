package com.c2e.back.service;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.c2e.back.dao.CandidatureRepository;
import com.c2e.back.dao.EtudiantRepository;
import com.c2e.back.entity.Adresse;
import com.c2e.back.entity.Candidature;
import com.c2e.back.entity.Certificat;
import com.c2e.back.entity.Competence;
import com.c2e.back.entity.Ecole;
import com.c2e.back.entity.Etudiant;
import com.c2e.back.entity.ExperienceProfessionnelle;
import com.c2e.back.entity.Filiere;
import com.c2e.back.entity.Langue;
import com.c2e.back.entity.ProjetPersonnel;
import com.c2e.back.exception.EtudiantException.EmailExists;
import com.c2e.back.exception.EtudiantException.ToMuchSevenSkills;
import com.c2e.back.validator.EtudiantValidator;

@Service
@Transactional
public class EtudiantService {

	private EtudiantRepository etudiantRepository;
	private EcoleService ecoleService;
	private FiliereService filiereService;
	private AdresseService adresseService;
	private NiveauEtudeService niveauEtudeService;
	private EtudiantValidator etudiantValidator;
	private CompetenceService competenceService;
	private ProjetService projetService;
	private LangueService langueService;
	private CertificatService certificatService;
	private CandidatureService candidatureService;
	private CandidatureRepository candidatureRepository;


	public EtudiantService(EtudiantRepository etudiantRepository, EcoleService ecoleService,
			FiliereService filiereService, AdresseService adresseService,
			NiveauEtudeService niveauEtudeService, EtudiantValidator etudiantValidator,
			CompetenceService competenceService, ProjetService projetService,
			LangueService langueService, CertificatService certificatService,
			CandidatureRepository candidatureRepository) {
		
		this.etudiantRepository = etudiantRepository;
		this.ecoleService = ecoleService;
		this.filiereService = filiereService;
		this.adresseService = adresseService;
		this.niveauEtudeService = niveauEtudeService;
		this.etudiantValidator = etudiantValidator;
		this.competenceService = competenceService;
		this.projetService = projetService;
		this.langueService = langueService;
		this.certificatService = certificatService;
		this.candidatureRepository = candidatureRepository;
	}

	public Etudiant getEtudiantById(Long idEtudiant) {
		return etudiantRepository.getOne(idEtudiant);
	}

	public Etudiant save(Etudiant etudiant) {
		return etudiantRepository.save(etudiant);
	}

	public void updateGeneralities(Long idEtudiant, Etudiant etudiant) throws EmailExists {
		Etudiant etudiantEnBase = getEtudiantById(idEtudiant);
		Filiere filiere;
		Ecole ecole;
		etudiantEnBase.setNomEcole(etudiant.getNomEcole());
		etudiantEnBase.setIntituleFiliere(etudiant.getIntituleFiliere());
		mettreAjourLesAutresInfosGeneralesEtudiant(etudiant, etudiantEnBase);
	}

	// methodes utilisées par d'autres:
	public Filiere filiereOfEcoleCreateIfNotExist(Ecole ecole, String IntituleFiliere) {
		Filiere filiereNouv = ecole.getFilieres().stream()
				.filter(element -> element.getIntitule().equals(IntituleFiliere))
				.findFirst()
				.orElse(null);
		
		if (filiereNouv != null) {
			return filiereNouv;
		} else {
		    filiereNouv = new Filiere.Builder().withIntitule(IntituleFiliere).build();
			filiereNouv = this.filiereService.save(filiereNouv);
			if (ecole.getFilieres() == null) {
				ecole.setFilieres(new ArrayList<>());
			}
			ecole.getFilieres().add(filiereNouv);
			ecoleService.save(ecole);
			return filiereNouv;
		}
	}

	public void supprimerEtudiantDeSonAncienneFiliere(String intituleFiliere, String nomEcole, Etudiant etudiant) {

		this.etudiantRepository.deleteLastFiliere(etudiant.getId());
	}

	public void mettreAjourLesAutresInfosGeneralesEtudiant(Etudiant etudiant, Etudiant etudiantEnBase) throws EmailExists {
		if(etudiantEnBase.getAdresse() == null) {
			Adresse newAdresse = adresseService.save(etudiant.getAdresse());
			etudiantEnBase.setAdresse(newAdresse);
		}else if(!etudiantEnBase.getAdresse().equals(etudiant.getAdresse())) {
			adresseService.delete(etudiantEnBase.getAdresse());
			Adresse newAdresse = adresseService.save(etudiant.getAdresse());
			etudiantEnBase.setAdresse(newAdresse);
		}
		

		if (etudiantEnBase.getEmail() == null) {
			etudiantValidator.validateEmailExistence(etudiant.getEmail());
			etudiantEnBase.setEmail(etudiant.getEmail());
		}else if (!etudiantEnBase.getEmail().equals(etudiant.getEmail())) {
			etudiantValidator.validateEmailExistence(etudiant.getEmail());
			etudiantEnBase.setEmail(etudiant.getEmail());
		}
		if(etudiant.getNiveauEtudes()!=null && etudiant.getNiveauEtudes().getIntitule()!=null) {
			etudiantEnBase.setNiveauEtudes(niveauEtudeService.getNiveauIfExistElseCreate(etudiant.getNiveauEtudes().getIntitule()));
		}
		else {
			etudiantEnBase.setNiveauEtudes(null);
		}
		etudiantEnBase.setNom(etudiant.getNom());
		etudiantEnBase.setPrenom(etudiant.getPrenom());
		etudiantEnBase.setTelephone(etudiant.getTelephone());
		System.out.println(etudiantEnBase);
		etudiantRepository.save(etudiantEnBase);
	}

	public Competence createCompetence(Competence competence, Long idEtudiant) {
		
		competence = competenceService.getCompetenceSiNonExistCreer(competence.getIntituleCompetence().getIntitule());
		Etudiant etudiant = etudiantRepository.getOne(idEtudiant);
		if(etudiant.getCompetences()==null) {
			etudiant.setCompetences(new ArrayList<>());
		}
		etudiant.getCompetences().add(competence);
		etudiantRepository.save(etudiant);
		return competence;
	}

	public void deleteCompetence(Long idCompetence, Long idEtudiant) {
		// TODO Auto-generated method stub
		this.etudiantRepository.deleteCompetenceFromCompetences(idCompetence,idEtudiant);
	}

	public Competence updateSevenSkills(Long idEtudiant, Long idCompetenceSevenSkill, int pourcentageDeMaitrise) throws ToMuchSevenSkills {
		Etudiant etudiant = getEtudiantById(idEtudiant);
		etudiantValidator.validateSevenSkills(etudiant);
		return competenceService.updateSevenSkills(idCompetenceSevenSkill, pourcentageDeMaitrise);
	}

	public ProjetPersonnel creerProjet(ProjetPersonnel projetPersonnel, Long idEtudiant) {
		Etudiant etudiant = getEtudiantById(idEtudiant);
		projetPersonnel = projetService.saveProjetPersonnel(projetPersonnel);
		if(etudiant.getProjetsPersonnels()==null){
			etudiant.setProjetsPersonnels(new ArrayList<>());
		}
		etudiant.getProjetsPersonnels().add(projetPersonnel);
		etudiantRepository.save(etudiant);
		return projetPersonnel;
	}

	public ExperienceProfessionnelle creerExperienceProfessionnelle(ExperienceProfessionnelle experienceProfessionnelle,
			Long idEtudiant) {
		Etudiant etudiant = getEtudiantById(idEtudiant);
		experienceProfessionnelle = projetService.saveExperienceProfessionnelle(experienceProfessionnelle);
		if(etudiant.getExperiencesProfessionnelles()==null){
			etudiant.setExperiencesProfessionnelles(new ArrayList<>());
		}
		etudiant.getExperiencesProfessionnelles().add(experienceProfessionnelle);
		etudiantRepository.save(etudiant);
		return experienceProfessionnelle;
	}

	public Langue creerUneLangue(Langue langue, Long idEtudiant) {
		Etudiant etudiant = getEtudiantById(idEtudiant);
		if(etudiant.getLangues() == null) {
			etudiant.setLangues(new ArrayList<>());
		}
		Langue newLangue = this.langueService.findByNomIfExistElseCreate(langue.getNom());
		etudiant.getLangues().add(newLangue);
		etudiantRepository.save(etudiant);
		return newLangue;
	}

	public void deleteFromMyLanguages(Long idLangue, Long idEtudiant) {
		this.etudiantRepository.deleteFromMyLanguages(idLangue,idEtudiant);
	}
	
	public Certificat creerUnCertificat(Certificat certificat, Long idEtudiant) {
		Etudiant etudiant = getEtudiantById(idEtudiant);
		if(etudiant.getCertificats() == null) {
			etudiant.setCertificats(new ArrayList<>());
		}
		Certificat newCertificat = this.certificatService.findByIntituleIfExistElseCreate(certificat.getIntitule());
		etudiant.getCertificats().add(newCertificat);
		etudiantRepository.save(etudiant);
		return newCertificat;	
	}
	

	public void deleteFromMyCertificates(Long idCertificat, Long idEtudiant) {
		this.etudiantRepository.deleteFromMyCertificates(idCertificat,idEtudiant);
	}

	public Page<Etudiant> searchEtudiants(String elementAchercher, Pageable pageable) {
		String type = "etudiants students ";
		if(type.contains(elementAchercher.toLowerCase())) {
			return this.etudiantRepository.findAll(pageable);
		}
		return this.etudiantRepository.findByNomContainingIgnoreCaseOrPrenomContainingIgnoreCaseOrNomEcoleContainingIgnoreCaseOrIntituleFiliereContainingIgnoreCaseOrTypeRechercheNaturelContainingIgnoreCaseOrderByNomDesc(
				elementAchercher, elementAchercher, elementAchercher, elementAchercher, elementAchercher, pageable);
	}

	public Collection<Candidature> getCandidatures(Long idEtudiant) {
		return this.candidatureRepository.findByEtudiantId(idEtudiant);		
	}
	
}