package com.c2e.back.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.c2e.back.entity.Entreprise;
import com.c2e.back.entity.Etudiant;
import com.c2e.back.entity.Offre;

@Service
public class RechercheService {
	private OffreService offreService;
	private EcoleService ecoleService;
	private EntrepriseService entrepriseService;
	private EtudiantService etudiantService;

	public RechercheService(OffreService offreService, EcoleService ecoleService, EntrepriseService entrepriseService,
			EtudiantService etudiantService) {

		this.offreService = offreService;
		this.ecoleService = ecoleService;
		this.entrepriseService = entrepriseService;
		this.etudiantService = etudiantService;
	}

	public Page<Offre> searchStages(String elementAchercher, Pageable pageable) {
		return offreService.searchStage(elementAchercher,pageable);
	}

	public Page<Offre> searchAlternances(String elementAchercher, Pageable pageable) {
		return offreService.searchAlternances(elementAchercher,pageable);
	}
	
	public Page<Offre> searchEmplois(String elementAchercher, Pageable pageable) {
		return offreService.searchEmplois(elementAchercher,pageable);
	}

	public Page<Offre> searchAppelsOffres(String elementAchercher, Pageable pageable) {
		return offreService.searchAppelsOffres(elementAchercher,pageable);
	}

	public Page<Etudiant> searchEtudiants(String elementAchercher, Pageable pageable) {
		return etudiantService.searchEtudiants(elementAchercher,pageable);
	}

	public Page<Entreprise> searchEntreprises(String elementAchercher, Pageable pageable) {
		return entrepriseService.searchEntreprises(elementAchercher,pageable);
	}

	public Offre getOffreByLink(String link) {
		return this.offreService.getOffreByLink(link);
	}

}
