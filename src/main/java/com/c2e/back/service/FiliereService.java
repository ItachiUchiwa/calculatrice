package com.c2e.back.service;

import java.util.Collection;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.c2e.back.dao.FiliereRepository;
import com.c2e.back.entity.Ecole;
import com.c2e.back.entity.Filiere;

@Service
@Transactional
public class FiliereService {

	private FiliereRepository filiereRepository;
	private EcoleService ecoleService;

	public FiliereService(FiliereRepository filiereRepository, EcoleService ecoleService) {
		this.filiereRepository = filiereRepository;
		this.ecoleService = ecoleService;
	}

	public Filiere save(Filiere filiere) {
		return this.filiereRepository.save(filiere);
	}

	public Collection<Filiere> findAll() {
		return this.filiereRepository.findAll();
	}
}
