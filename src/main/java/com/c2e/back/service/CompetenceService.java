package com.c2e.back.service;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.c2e.back.dao.CompetenceRepository;
import com.c2e.back.dao.IntituleCompetenceRepository;
import com.c2e.back.entity.Competence;
import com.c2e.back.entity.IntituleCompetence;

@Service
@Transactional
public class CompetenceService {

	private CompetenceRepository competenceRepository;
	private IntituleCompetenceRepository intituleCompetenceRepository;

	public CompetenceService(CompetenceRepository competenceRepository,
			IntituleCompetenceRepository intituleCompetenceRepository) {
		this.competenceRepository = competenceRepository;
		this.intituleCompetenceRepository = intituleCompetenceRepository;
	}

	public List<Competence> findAll() {
		return this.competenceRepository.findAll();
	}

	public Competence getCompleteCompetence(Competence competence) {
		return this.competenceRepository.getOne(competence.getId());
	}

	public Competence getCompetenceSiNonExistCreer(String intitule) {
		IntituleCompetence intituleCompetence = null;
		if (intituleCompetenceRepository.existsByIntitule(intitule)) {
			intituleCompetence = intituleCompetenceRepository.findByIntitule(intitule);
		} else {
			intituleCompetence = new IntituleCompetence.Builder().withIntitule(intitule).build();
			intituleCompetenceRepository.save(intituleCompetence);
		}
		return competenceRepository.save(new Competence.Builder().withIntituleCompetence(intituleCompetence).build());
	}

	public Collection<IntituleCompetence> findAllIntituleCompetence() {
		return intituleCompetenceRepository.findAll();
	}

	public void deleteById(Long idCompetence) {
		this.competenceRepository.deleteWithId(idCompetence);
	}

	public Competence getCompetenceById(Long idCompetence) {
		return this.competenceRepository.getOne(idCompetence);
	}
	
	public Competence updateSevenSkills(Long idCompetenceSevenSkill, int pourcentageDeMaitrise) {
		Competence competence = getCompetenceById(idCompetenceSevenSkill);
		competence.setEstSevenSkills(true);
		competence.setPourcentageDeMaitrise(pourcentageDeMaitrise);
		return competenceRepository.save(competence);
	}

	public Competence deleteCompetenceFromSevenSkills(Long idCompetence) {
		Competence competence = this.competenceRepository.getOne(idCompetence);
		competence.setEstSevenSkills(false);
		competence.setPourcentageDeMaitrise(0);
		return competenceRepository.save(competence);
	}

	public void deleteCompetence(Long idCompetence, Long idEtudiant) {
		this.competenceRepository.deleteCompetenceFromCompetences(idCompetence,idEtudiant);
		
	}
}
