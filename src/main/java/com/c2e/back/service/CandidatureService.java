package com.c2e.back.service;

import static com.c2e.back.configuration.ValeurGlobale.CITY_SKILL_MAIL;
import static com.c2e.back.configuration.ValeurGlobale.MESSAGE_EVOLUTION_CANDIDATURE1;
import static com.c2e.back.configuration.ValeurGlobale.MESSAGE_EVOLUTION_CANDIDATURE2;
import static com.c2e.back.configuration.ValeurGlobale.MESSAGE_SUPPRESSION_CANDIDATURE1;
import static com.c2e.back.configuration.ValeurGlobale.MESSAGE_SUPPRESSION_CANDIDATURE2;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.c2e.back.dao.CandidatureRepository;
import com.c2e.back.dao.CommentaireRepository;
import com.c2e.back.entity.Candidature;
import com.c2e.back.entity.Commentaire;
import com.c2e.back.entity.EtapeRecrutement;
import com.c2e.back.entity.Etudiant;
import com.c2e.back.entity.Offre;
import com.c2e.back.entity.TypeOffre;

@Service
@Transactional
public class CandidatureService {

	private CandidatureRepository candidatureRepository;
	private EtudiantService etudiantService;
	private OffreService offreService;
	private EntrepriseService entrepriseService;
	private CommentaireRepository commentaireRepository;
	private EmailSenderService emailSenderService;

	public CandidatureService(CandidatureRepository candidatureRepository, EtudiantService etudiantService,
			OffreService offreService, EntrepriseService entrepriseService, CommentaireRepository commentaireRepository,
			EmailSenderService emailSenderService) {
		this.candidatureRepository = candidatureRepository;
		this.etudiantService = etudiantService;
		this.offreService = offreService;
		this.entrepriseService = entrepriseService;
		this.commentaireRepository = commentaireRepository;
		this.emailSenderService = emailSenderService;
	}

	// Fonctions métiers:
	public void deleteCandidatureByIdEtudiant(Candidature candidature, Long idEtudiant) {
		
		if (candidature.getEtudiant().getId() == idEtudiant) {
			// Email message

			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setTo(candidature.getEtudiant().getUsername());
			mailMessage.setSubject(candidature.getIntituleOffre());
			mailMessage.setFrom(CITY_SKILL_MAIL);
			mailMessage.setText(MESSAGE_SUPPRESSION_CANDIDATURE1 + candidature.getIntituleOffre()
					+ MESSAGE_SUPPRESSION_CANDIDATURE2 );
			emailSenderService.sendEmail(mailMessage);

			candidatureRepository.deleteWithId(candidature.getId());
			candidatureRepository.deleteWithCandidatureIdOnOffre(candidature.getId());
			candidatureRepository.deleteById(candidature.getId());

		}
	}

	// ----------------------------------------
	public Candidature creerCandidature(Long idOffre, Long idEtudiantOuEntreprise) {

		Offre offre = offreService.getByIdOffre(idOffre);
		List<EtapeRecrutement> etapesRecrutement = this.entrepriseService
				.getEtapesDeRecrutement(offre.getIdEntreprise());
		Collections.sort(etapesRecrutement);
		if (offre.getTypeOffre() == TypeOffre.APPEL_OFFRE) {
			Candidature candidature = new Candidature.Builder().withDateCandidature(LocalDateTime.now())
					.withIdEntreprise(idEtudiantOuEntreprise).withIntituleOffre(offre.getIntitule())
					.withImageEntreprise(offre.getImageEntreprise()).withNomEntreprise(offre.getNomEntreprise())
					.withIdEntrepriseHote(offre.getIdEntreprise()).withTypeOffre(offre.getTypeOffre())
					.withEtat(etapesRecrutement.get(0)).withPoint(0).build();

			candidatureRepository.save(candidature);
			offre.getCandidatures().add(candidature);
			offreService.save(offre);
			
			return candidature;

		} else {
			Etudiant etudiant = etudiantService.getEtudiantById(idEtudiantOuEntreprise);
			Candidature candidature = new Candidature.Builder().withDateCandidature(LocalDateTime.now())
					.withEtudiant(etudiant).withIntituleOffre(offre.getIntitule())
					.withImageEntreprise(offre.getImageEntreprise()).withNomEntreprise(offre.getNomEntreprise())
					.withIdEntrepriseHote(offre.getIdEntreprise()).withTypeOffre(offre.getTypeOffre())
					.withEtat(etapesRecrutement.get(0)).withPoint(0).build();
			candidatureRepository.save(candidature);
			offre.getCandidatures().add(candidature);
			offreService.save(offre);
			etudiant.setIdOffresPostulees(
					etudiant.getIdOffresPostulees()==null?offre.getId().toString()
					:
					etudiant.getIdOffresPostulees()+","+offre.getId()
			);
			etudiant.setIdOffresPostulees(
					manageListIdOffresPostulees(etudiant.getIdOffresPostulees())
					);
			etudiantService.save(etudiant);
			return candidature;
		}

	}

	public String manageListIdOffresPostulees(String idOffresPostulees) {
		String result = idOffresPostulees;
		if(idOffresPostulees.length()>1000) {
			String right = idOffresPostulees.substring(500);
			int idx = right.indexOf(",");
			result = right.substring(idx+1);
		}
		return result;
	}
	public Candidature getCandidatureById(Long idCandidature) {
		return candidatureRepository.getOne(idCandidature);
	}

	public void deleteCandidatureByIdOffreAndIdEtudiant(Long idOffre, Long idEtudiant) {
		Offre offre = offreService.getByIdOffre(idOffre);
		if (offre != null) {
			offre.getCandidatures().stream().forEach((candidature) -> {
				deleteCandidatureByIdEtudiant(candidature, idEtudiant);
			});

		}

	}

	public List<Candidature> findAll() {
		return candidatureRepository.findAll();
	}

	public void deleteCandidatureByIdCandidature(Long idCandidature) {
		Candidature candidature = candidatureRepository.getOne(idCandidature);
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(candidature.getEtudiant().getUsername());
		mailMessage.setSubject(candidature.getIntituleOffre());
		mailMessage.setFrom(CITY_SKILL_MAIL);
		mailMessage.setText(MESSAGE_SUPPRESSION_CANDIDATURE1 + candidature.getIntituleOffre()
				+ MESSAGE_SUPPRESSION_CANDIDATURE2 );

		candidatureRepository.deleteWithId(idCandidature);
		candidatureRepository.deleteWithCandidatureIdOnOffre(idCandidature);		
		candidatureRepository.deleteById(idCandidature);
		emailSenderService.sendEmail(mailMessage);
	}

	public Candidature changerEtatCandidature(Long idCandidature, EtapeRecrutement etapeRecrutement) {
		Candidature candidature = getCandidatureById(idCandidature);
		candidature.setEtatCandidature(etapeRecrutement);
		candidatureRepository.save(candidature);

		// Email message

		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(candidature.getEtudiant().getUsername());
		mailMessage.setSubject(candidature.getIntituleOffre());
		mailMessage.setFrom(CITY_SKILL_MAIL);
		mailMessage.setText(MESSAGE_EVOLUTION_CANDIDATURE1 + candidature.getIntituleOffre()
				+ MESSAGE_EVOLUTION_CANDIDATURE2 + etapeRecrutement.getIntitule());

		emailSenderService.sendEmail(mailMessage);
		return candidature;
	}

	public Commentaire ajouterCommentaire(Long idCandidature, Commentaire commentaire) {
		this.commentaireRepository.save(commentaire);
		Candidature candidature = getCandidatureById(idCandidature);
		if (candidature.getCommentaires() == null) {
			candidature.setCommentaires(new ArrayList<>());
		}
		candidature.getCommentaires().add(commentaire);
		candidatureRepository.save(candidature);
		return commentaire;

	}

	public Integer ajouterPoint(Long idCandidature, Integer point) {
		Candidature candidature = getCandidatureById(idCandidature);
		if (candidature.getPoint() == null) {
			candidature.setPoint(point);
		} else {
			candidature.setPoint(candidature.getPoint() + point);
		}
		candidatureRepository.save(candidature);
		return point;
	}

	public Collection<Candidature> getCandidatureByEtudiantId(Long idEtudiant) {
		return this.candidatureRepository.findByEtudiantId(idEtudiant);

	}
}
