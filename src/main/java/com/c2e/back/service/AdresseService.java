package com.c2e.back.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.c2e.back.dao.AdresseRepository;
import com.c2e.back.entity.Adresse;

@Service
@Transactional
public class AdresseService {

	private AdresseRepository adresseRepository;
	
	public AdresseService(AdresseRepository adresseRepository) {
		this.adresseRepository = adresseRepository;
	}

	public Adresse save(Adresse adresse) {
		return adresseRepository.save(adresse);
	}

	public void delete(Adresse adresse) {
		adresseRepository.deleteWithId(adresse.getId());
	}
	
}
