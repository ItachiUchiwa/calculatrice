package com.c2e.back.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.c2e.back.dao.OffreRepository;
import com.c2e.back.entity.Offre;
import com.c2e.back.entity.TypeOffre;

@Service
@Transactional
public class OffreService {

	private OffreRepository offreRepository;
	public OffreService(OffreRepository offreRepository) {
		this.offreRepository = offreRepository;
	}
	
	public Offre getByIdOffre(Long idOffre) {
		return offreRepository.getOne(idOffre);
	}

	public Offre save(Offre offre) {
		return offreRepository.save(offre);
		
	}

	public List<Offre> findAll() {
		return offreRepository.findAll();
	}

	public void deleteById(Long id) {
		this.offreRepository.deleteWithId(id);
		this.offreRepository.deleteById(id);
	}

	public Offre getOffreById(Long idOffre) {
		return this.offreRepository.getOne(idOffre);
	}

	public Page<Offre> searchStage(String elementAchercher,Pageable pageable) {
		String type = "stages internships";
		if(type.contains(elementAchercher.toLowerCase())) {
			return this.offreRepository.findByTypeOffreAndOuvertOrderByDatePublicationDesc(TypeOffre.STAGE,true, pageable);
		}
		System.out.println(elementAchercher);
		return this.offreRepository.findByTypeOffreAndOuvertAndIntituleContainingIgnoreCaseOrTypeOffreAndOuvertAndDescriptionPosteContainingIgnoreCaseOrTypeOffreAndOuvertAndDescriptionProfilContainingIgnoreCaseOrTypeOffreAndOuvertAndNomEntrepriseContainingIgnoreCaseOrderByDatePublicationDesc(
				TypeOffre.STAGE,true,elementAchercher,TypeOffre.STAGE,true,elementAchercher,TypeOffre.STAGE,true, elementAchercher, TypeOffre.STAGE,true, elementAchercher, pageable);
	}

	public Page<Offre> searchAlternances(String elementAchercher, Pageable pageable) {
		String type = "alternances work-study";
		if(type.contains(elementAchercher.toLowerCase())) {
			return this.offreRepository.findByTypeOffreAndOuvertOrderByDatePublicationDesc(TypeOffre.ALTERNANCE, true, pageable);
		}
		return this.offreRepository.findByTypeOffreAndOuvertAndIntituleContainingIgnoreCaseOrTypeOffreAndOuvertAndDescriptionPosteContainingIgnoreCaseOrTypeOffreAndOuvertAndDescriptionProfilContainingIgnoreCaseOrTypeOffreAndOuvertAndNomEntrepriseContainingIgnoreCaseOrderByDatePublicationDesc(
				TypeOffre.ALTERNANCE, true,elementAchercher,TypeOffre.ALTERNANCE,true, elementAchercher, TypeOffre.ALTERNANCE, true, elementAchercher,TypeOffre.ALTERNANCE, true, elementAchercher, pageable);
	}
	
	public Page<Offre> searchEmplois(String elementAchercher, Pageable pageable) {
		String type = "emplois jobs";
		if(type.contains(elementAchercher.toLowerCase())) {
			return this.offreRepository.findByTypeOffreAndOuvertOrderByDatePublicationDesc(TypeOffre.EMPLOI,true, pageable);
		}
		return this.offreRepository.findByTypeOffreAndOuvertAndIntituleContainingIgnoreCaseOrTypeOffreAndOuvertAndDescriptionPosteContainingIgnoreCaseOrTypeOffreAndOuvertAndDescriptionProfilContainingIgnoreCaseOrTypeOffreAndOuvertAndNomEntrepriseContainingIgnoreCaseOrderByDatePublicationDesc(
				TypeOffre.EMPLOI, true, elementAchercher,TypeOffre.STAGE, true,elementAchercher, TypeOffre.EMPLOI,true, elementAchercher, TypeOffre.EMPLOI, true, elementAchercher, pageable);
	}

	public Page<Offre> searchAppelsOffres(String elementAchercher, Pageable pageable) {
		String type = "appel d'offres call for tender appel_offres";
		if(type.contains(elementAchercher.toLowerCase())) {
			return this.offreRepository.findByTypeOffreAndOuvertOrderByDatePublicationDesc(TypeOffre.APPEL_OFFRE,true, pageable);
		}
		return this.offreRepository.findByTypeOffreAndOuvertAndIntituleContainingIgnoreCaseOrTypeOffreAndOuvertAndDescriptionPosteContainingIgnoreCaseOrTypeOffreAndOuvertAndDescriptionProfilContainingIgnoreCaseOrTypeOffreAndOuvertAndNomEntrepriseContainingIgnoreCaseOrderByDatePublicationDesc(
				TypeOffre.APPEL_OFFRE,true,elementAchercher,TypeOffre.STAGE,true,elementAchercher, TypeOffre.APPEL_OFFRE, true, elementAchercher, TypeOffre.APPEL_OFFRE, true, elementAchercher, pageable);
	}

	public Offre getOffreByLink(String link) {
		return this.offreRepository.findByLink(link);
	}
}
