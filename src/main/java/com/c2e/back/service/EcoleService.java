package com.c2e.back.service;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.c2e.back.dao.EcoleRepository;
import com.c2e.back.entity.Ecole;

@Repository
@Transactional
public class EcoleService {

	private EcoleRepository ecoleRepository;
	
	public EcoleService(EcoleRepository ecoleRepository) {
		this.ecoleRepository = ecoleRepository;
	}
	
	public List<Ecole> findAll() {
		return this.ecoleRepository.findAll();
	}
	
	public Ecole getCompleteEcole(Ecole ecole) {
		return this.ecoleRepository.getOne(ecole.getId());
	}

	public Ecole getById(Long idEcole) {
		return this.ecoleRepository.getOne(idEcole);
	}

	public Ecole findByNom(String nomEcole) {
		return this.ecoleRepository.findByNom(nomEcole);
	}

	public Ecole save(Ecole ecole) {
		return ecoleRepository.save(ecole);
	}

}
