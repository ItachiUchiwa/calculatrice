package com.c2e.back.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.c2e.back.dao.PartenariatRepository;
import com.c2e.back.entity.Entreprise;
import com.c2e.back.entity.Partenariat;

@Service
@Transactional
public class PartenariatService {

	private PartenariatRepository partenariatRepository;
	private EntrepriseService entrepriseService;

	public PartenariatService(PartenariatRepository partenariatRepository,
			EntrepriseService entrepriseService) {
		this.partenariatRepository = partenariatRepository;
		this.entrepriseService = entrepriseService;
	}

	public void demanderPartenariat(Partenariat partenariat, Long idEntrepriseDestinataire,
			Long idEntrepriseExpediteur) {
		
		Entreprise entrepriseDestinataire = entrepriseService.getEntrepriseById(idEntrepriseDestinataire);
		Entreprise entrepriseExpediteur = entrepriseService.getEntrepriseById(idEntrepriseExpediteur);
		partenariat.setEntrepriseExpediteur(entrepriseExpediteur);
		partenariatRepository.save(partenariat);
		entrepriseDestinataire.getPartenariats().add(partenariat);
		
		entrepriseService.save(entrepriseDestinataire);
	}

	public void deletePartenariatById(Long idPartenariat) {
		this.partenariatRepository.deleteWithId(idPartenariat);
	}

}