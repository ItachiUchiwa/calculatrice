package com.c2e.back.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.c2e.back.dao.ExperienceProfessionnelleRepository;
import com.c2e.back.dao.ProjetPersonnelRepository;
import com.c2e.back.dao.ProjetRepository;
import com.c2e.back.entity.ExperienceProfessionnelle;
import com.c2e.back.entity.Projet;
import com.c2e.back.entity.ProjetPersonnel;

@Service
@Transactional
public class ProjetService {

	private ProjetRepository projetRepository;
	private ProjetPersonnelRepository projetPersonnelRepository;
	private ExperienceProfessionnelleRepository experienceProfessionnelleRepository;
	
	public ProjetService(ProjetRepository projetRepository,
			ProjetPersonnelRepository projetPersonnelRepository,
			ExperienceProfessionnelleRepository experienceProfessionnelleRepository) {
		this.projetRepository = projetRepository;
		this.projetPersonnelRepository = projetPersonnelRepository;
		this.experienceProfessionnelleRepository = experienceProfessionnelleRepository;
	}
	
	public Projet save(Projet projet) {
		return this.projetRepository.save(projet);
	}
	
	public ProjetPersonnel saveProjetPersonnel(ProjetPersonnel projetPersonnel) {
		return this.projetPersonnelRepository.save(projetPersonnel);
	}
	
	public ExperienceProfessionnelle saveExperienceProfessionnelle(ExperienceProfessionnelle experienceProfessionnelle) {
		return this.experienceProfessionnelleRepository.save(experienceProfessionnelle);
	}

	public void deleteById(Long id) {
		this.projetRepository.deleteWithId(id);
	}

	public void deleteProjetPersonnelById(Long idProjetPersonnel) {
		this.projetPersonnelRepository.deleteWithId(idProjetPersonnel);
		this.projetPersonnelRepository.deleteById(idProjetPersonnel);
	}

	public void deleteExperienceProfessionnelleById(Long idExperienceProfessionnelle) {
		this.experienceProfessionnelleRepository.deleteWithId(idExperienceProfessionnelle);		
		this.experienceProfessionnelleRepository.deleteById(idExperienceProfessionnelle);		
	}
}
