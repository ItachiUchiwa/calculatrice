package com.c2e.back.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.c2e.back.dao.DomaineRepository;
import com.c2e.back.entity.Domaine;

@Service
@Transactional
public class DomaineService {

	private DomaineRepository domaineRepository;
	public DomaineService(DomaineRepository domaineRepository) {
		this.domaineRepository = domaineRepository;
	}
	
	public Domaine recupererDomaineParIntituleSinonCreer(String intitule) {
		if(domaineRepository.existsByIntitule(intitule)){
			return domaineRepository.findByIntitule(intitule);
		}
		return domaineRepository.save(new Domaine.Builder().withIntitule(intitule).build());
	}

	public List<Domaine> findAll() {
		return this.domaineRepository.findAll();
	}
}
