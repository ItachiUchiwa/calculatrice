package com.c2e.back.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.c2e.back.dao.EtapeRecrutementRepository;
import com.c2e.back.dao.ManagerRepository;
import com.c2e.back.entity.Candidature;
import com.c2e.back.entity.Commentaire;
import com.c2e.back.entity.Entreprise;
import com.c2e.back.entity.EtapeRecrutement;
import com.c2e.back.entity.Etudiant;
import com.c2e.back.entity.Manager;
import com.c2e.back.entity.Offre;

@Service
@Transactional
public class RHService {

	private CandidatureService candidatureService;
	private EtapeRecrutementRepository etapeRecrutementRepository;
	private EntrepriseService entrepriseService;
	private ManagerRepository managerRepository;
	private EtudiantService etudiantService;

	public RHService(CandidatureService candidatureService, EntrepriseService entrepriseService,
			EtapeRecrutementRepository etapeRecrutementRepository,
			ManagerRepository managerRepository,EtudiantService etudiantService) {
		this.candidatureService = candidatureService;
		this.etapeRecrutementRepository = etapeRecrutementRepository;
		this.entrepriseService = entrepriseService;
		this.managerRepository = managerRepository;
		this.etudiantService = etudiantService;
	}

	public EtapeRecrutement changerEtatCandidature(Long idEntreprise, Long idCandidature,
			Long idEtatCandidatureActuel) {
		Entreprise entreprise = entrepriseService.getEntrepriseById(idEntreprise);
		Collections.sort(entreprise.getEtapeRecrutement());
		EtapeRecrutement etapeRecrutement = null;
		int indexEtapeSuivant = 0;
		int size = entreprise.getEtapeRecrutement().size();
		for (int i = 0; i < size; i++) {
			if (entreprise.getEtapeRecrutement().get(i).getId() == idEtatCandidatureActuel) {
				indexEtapeSuivant = i + 1;
			}
		}
		if (indexEtapeSuivant <= size) {
			etapeRecrutement = entreprise.getEtapeRecrutement().get(indexEtapeSuivant);
			this.candidatureService.changerEtatCandidature(idCandidature, etapeRecrutement);
		}
		return etapeRecrutement;
	}

	public Commentaire ajouterCommentaire(Long idCandidature, Commentaire commentaire) {
		return this.candidatureService.ajouterCommentaire(idCandidature, commentaire);
	}

	public Integer ajouterPoint(Long idCandidature, Integer point) {
		return this.candidatureService.ajouterPoint(idCandidature, point);
	}

	public List<Candidature> candidatureDuManager(Long idEntreprise, Long idManager, String intituleOffre) {
		Entreprise entreprise = this.entrepriseService.getEntrepriseById(idEntreprise);
		Manager manager = null;
		List<Candidature> candidatures = null;
		if (idEntreprise.equals(idManager)) {
			Optional<Offre> offreOpt = entreprise.getOffres().stream()
					.filter(offreF -> offreF.getIntitule().equals(intituleOffre)).findFirst();
			if (offreOpt.get() != null) {
				candidatures = offreOpt.get().getCandidatures().stream().collect(Collectors.toList());
			}
		} else {
			Optional<Manager> managerOpt = entreprise.getManagers().stream()
					.filter(managerF -> managerF.getId() == idManager).findFirst();
			System.out.println(managerOpt);
			if (managerOpt.get() != null) {
				manager = managerOpt.get();
				candidatures = manager.getCandidatures().stream()
						.filter(candidature -> candidature.getIntituleOffre().equals(intituleOffre))
						.collect(Collectors.toList());
				System.out.println(manager.getCandidatures());
			}
		}
		return candidatures;
	}

	public void ajouterAuxFavoris(Long idManager, Long idEtudiant) {
		Manager manager = this.managerRepository.getOne(idManager);
		Etudiant etudiant = etudiantService.getEtudiantById(idEtudiant);
		if (manager.getFavoris() == null) {
			manager.setFavoris(new ArrayList<>());
		}
		if(!manager.getFavoris().contains(etudiant)) {
			manager.getFavoris().add(etudiant);
			managerRepository.save(manager);
		}
	}
	
	public void enleverDesFavoris(Long idManager, Long idEtudiant) {
		Manager manager = this.managerRepository.getOne(idManager);
		Etudiant etudiant = etudiantService.getEtudiantById(idEtudiant);
		if (manager.getFavoris() == null) {
			manager.setFavoris(new ArrayList<>());
		}
		manager.getFavoris().removeIf(e-> e.getId().equals(etudiant.getId()));
	}

	public Collection<Etudiant> getMyFavoris(Long idManager) {
		return this.managerRepository.getOne(idManager).getFavoris();
	}

	public boolean estFavoris(Long idManager, Long idEtudiant) {
		Manager manager = this.managerRepository.getOne(idManager);
		for(Etudiant etudiant:manager.getFavoris()) {
			if(etudiant.getId().equals(idEtudiant)) {
				return true;
			}
		}
		return false;
	}

}
