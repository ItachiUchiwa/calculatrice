package com.c2e.back.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.c2e.back.dao.LangueRepository;
import com.c2e.back.entity.Langue;

@Service
@Transactional
public class LangueService {

	private LangueRepository langueRepository;
	
	public LangueService(LangueRepository langueRepository) {
		this.langueRepository = langueRepository;
	}
	
	public List<Langue>findAll() {
		return this.langueRepository.findAll();
	}
	 
	public Langue save(Langue langue) {
		return this.langueRepository.save(langue);
	}
	
	public Langue findByNomIfExistElseCreate(String nom) {
		Langue langue=null;
		
		langue = new Langue.Builder().withNom(nom).build();
		langue =this.langueRepository.save(langue);
		return langue;
	}
}
