package com.c2e.back.service;

import static com.c2e.back.configuration.ValeurGlobale.ID_GOOGLE_FOLDER_PROFIL_CANDIDAT;
import static com.c2e.back.configuration.ValeurGlobale.ID_GOOGLE_FOLDER_PROFIL_ENTREPRISE;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.c2e.back.dao.AppRoleRepository;
import com.c2e.back.dao.AppUserRepository;
import com.c2e.back.dao.EntrepriseRepository;
import com.c2e.back.dao.EtudiantRepository;
import com.c2e.back.dao.OffreRepository;
import com.c2e.back.entity.AppRole;
import com.c2e.back.entity.AppUser;
import com.c2e.back.entity.Entreprise;
import com.c2e.back.entity.Etudiant;
import com.c2e.back.entity.Offre;
import com.google.api.client.http.AbstractInputStreamContent;
import com.google.api.client.http.InputStreamContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;

@Service
@Transactional
public class AccountService {
	
	private AppUserRepository appUserRepository;
	private AppRoleRepository appRoleRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private EtudiantRepository etudiantRepository;
	private EntrepriseRepository entrepriseRepository;
	private OffreRepository offreRepository;
	
	public AccountService(AppUserRepository appUserRepository, AppRoleRepository appRoleRepository,
			BCryptPasswordEncoder bCryptPasswordEncoder,EtudiantRepository etudiantRepository,
	 EntrepriseRepository entrepriseRepository,OffreRepository offreRepository) {
		this.appUserRepository = appUserRepository;
		this.appRoleRepository = appRoleRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.etudiantRepository = etudiantRepository;
		this.entrepriseRepository = entrepriseRepository;
		this.offreRepository = offreRepository;
	}
	
	public AppUser saveEtudiant(Etudiant etudiant) {
		etudiant.setPassword(bCryptPasswordEncoder.encode(etudiant.getPassword()));
		return etudiantRepository.save(etudiant); 
	}
	
	public AppUser saveUser(AppUser appUser) {
		appUser.setPassword(bCryptPasswordEncoder.encode(appUser.getPassword()));
		return appUserRepository.save(appUser); 
	}
	
	public AppUser save(AppUser appUser) {
		return appUserRepository.save(appUser); 
	}
	
	public boolean existsById(Long idUser) {
		return appUserRepository.existsById(idUser); 
	}
	
	public AppUser saveEntreprise(Entreprise entreprise, Offre SPONTANNEE) {
		entreprise.setPassword(bCryptPasswordEncoder.encode(entreprise.getPassword()));
		entrepriseRepository.save(entreprise);
		SPONTANNEE.setIdEntreprise(entreprise.getId());
		SPONTANNEE.setDatePublication(LocalDateTime.now());
		ArrayList<Offre> offres = new ArrayList<>();
		offres.add(SPONTANNEE);
		entreprise.setOffres(offres);
		return entrepriseRepository.save(entreprise);
	}
	
	public void saveOffreSpontanee(Offre offre) {
		offreRepository.save(offre);
	}
	
	public AppRole saveRole(AppRole appRole) { 
		return appRoleRepository.save(appRole); 
	} 
	
	public AppUser findUserByUsername(String username) { 
		return appUserRepository.findByUsername(username);
	} 
	
	public void addRoleToUser(String username, String roleName) { 
		AppUser appUser = appUserRepository.findByUsername(username);
		AppRole appRole = appRoleRepository.findByRole(roleName);
		appUser.getRoles().add(appRole); 
		appUserRepository.save(appUser);
	}
	
	public Optional<AppUser> findUserByResetToken(String resetToken) {
		return appUserRepository.findByResetToken(resetToken);
	}
 
	public AppUser findUserById(Long idUser) {
		return appUserRepository.getOne(idUser); 
	} 

	public AppUser saveOwner(AppUser owner) {
		owner.setPassword(bCryptPasswordEncoder.encode(owner.getPassword()));
		return appUserRepository.save(owner); 		
	}
	
	//deleteFileId
	public void deleteGoogleFileById(String fileId) throws IOException {
		if(fileId!=null && fileId!="") {
			System.out.println("OK-1");
			Drive driveService = GoogleDriveUtils.getDriveService();
			driveService.files().delete(fileId);
		}
	}
	
	 //Search a file from Drive
	 public List<File> getGoogleFilesByName(String fileNameLike) throws IOException {

	        Drive driveService = GoogleDriveUtils.getDriveService();

	        String pageToken = null;
	        List<File> list = new ArrayList<File>();

	        String query = " name contains '" + fileNameLike + "' " //
	                + " and mimeType != 'application/vnd.google-apps.folder' ";

	        do {
	            FileList result = driveService.files().list().setQ(query).setSpaces("drive") //
	                    // Fields will be assigned values: id, name, createdTime, mimeType
	                    .setFields("nextPageToken, files(id, name, createdTime, mimeType)")//
	                    .setPageToken(pageToken).execute();
	            for (File file : result.getFiles()) {
	                list.add(file);
	            }
	            pageToken = result.getNextPageToken();
	        } while (pageToken != null);
	        //
	        return list;
	    }
	 
	 // Create Google File from java.io.File
	 public Permission createPublicPermission(String googleFileId) throws IOException {
	        // All values: user - group - domain - anyone
	        String permissionType = "anyone";
	        // All values: organizer - owner - writer - commenter - reader
	        String permissionRole = "reader";

	        Permission newPermission = new Permission();
	        newPermission.setType(permissionType);
	        newPermission.setRole(permissionRole);

	        Drive driveService = GoogleDriveUtils.getDriveService();
	        return driveService.permissions().create(googleFileId, newPermission).execute();
	 }
	 
	    private String _createGoogleFile(String googleFolderIdParent, String contentType, //
	            String customFileName, AbstractInputStreamContent uploadStreamContent) throws IOException {

	        File fileMetadata = new File();
	        fileMetadata.setName(customFileName);

	        List<String> parents = Arrays.asList(googleFolderIdParent);
	        fileMetadata.setParents(parents);
	        //
	        Drive driveService = GoogleDriveUtils.getDriveService();

	        File file = driveService.files().create(fileMetadata, uploadStreamContent)
	                .setFields("id, webContentLink, webViewLink, parents")
	                .execute();
	        
	        createPublicPermission(file.getId());
	        return file.getId();
	    }
	    
	    public String createGoogleFile(boolean isEntreprise,  String contentType, //
	            String customFileName,InputStream inputStream) throws IOException {
	        //
	        AbstractInputStreamContent uploadStreamContent = new InputStreamContent(contentType, inputStream);
	        //
	        if(isEntreprise) {
		        return _createGoogleFile(ID_GOOGLE_FOLDER_PROFIL_ENTREPRISE, contentType, customFileName, uploadStreamContent);
	        }
	        return _createGoogleFile(ID_GOOGLE_FOLDER_PROFIL_CANDIDAT, contentType, customFileName, uploadStreamContent);
	   }
	    
}