package com.c2e.back.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.c2e.back.dao.NiveauEtudeRepository;
import com.c2e.back.entity.NiveauEtude;

@Service
@Transactional
public class NiveauEtudeService {

	private NiveauEtudeRepository niveauEtudeRepository;
	
	public NiveauEtudeService(NiveauEtudeRepository niveauEtudeRepository) {
		this.niveauEtudeRepository = niveauEtudeRepository;
	}
	
	public List<NiveauEtude> findAll(){
		return this.niveauEtudeRepository.findAll();
	}
	
	public NiveauEtude getCompleteNiveauEtude(NiveauEtude niveauEtude) {
		return this.niveauEtudeRepository.getOne(niveauEtude.getId());
	}
	
	public NiveauEtude getNiveauIfExistElseCreate(String niveauEtudeIntitule) {
		if(niveauEtudeRepository.existsByIntitule(niveauEtudeIntitule)) {
			return niveauEtudeRepository.findByIntitule(niveauEtudeIntitule);
		}
		return this.niveauEtudeRepository.save(new NiveauEtude.Builder()
				.withIntitule(niveauEtudeIntitule)
				.build());
	}
}
