package com.c2e.back.service;

import static com.c2e.back.configuration.ValeurGlobale.BASE_URL_OF_FRONT;
import static com.c2e.back.configuration.ValeurGlobale.CITY_SKILL_MAIL;
import static com.c2e.back.configuration.ValeurGlobale.MESSAGE_NOUVEL_OFFRE;
import static com.c2e.back.configuration.ValeurGlobale.SPECIFIC_OFFRE;
import static com.c2e.back.configuration.ValeurGlobale.SUJET_NOUVEL_OFFRE;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.c2e.back.dao.AppUserRepository;
import com.c2e.back.dao.CandidatureRepository;
import com.c2e.back.dao.EntrepriseRepository;
import com.c2e.back.dao.EtapeRecrutementRepository;
import com.c2e.back.dao.ManagerRepository;
import com.c2e.back.entity.Adresse;
import com.c2e.back.entity.AppUser;
import com.c2e.back.entity.Candidature;
import com.c2e.back.entity.Competence;
import com.c2e.back.entity.Ecole;
import com.c2e.back.entity.Entreprise;
import com.c2e.back.entity.EtapeRecrutement;
import com.c2e.back.entity.Manager;
import com.c2e.back.entity.NiveauEtude;
import com.c2e.back.entity.Offre;
import com.c2e.back.entity.Projet;
import com.c2e.back.entity.TypeOffre;
import com.c2e.back.exception.EntrepriseException.EmailExists;
import com.c2e.back.validator.EntrepriseValidator;

@Service
@Transactional
public class EntrepriseService {

	private EntrepriseRepository entrepriseRepository;
	private EtapeRecrutementRepository etapeRecrutementRepository;
	private DomaineService domaineService;
	private EntrepriseValidator entrepriseValidator;
	private AdresseService adresseService;
	private ProjetService projetService;
	private OffreService offreService;
	private EcoleService ecoleService;
	private CompetenceService competenceService;
	private NiveauEtudeService niveauEtudeService;
	private ManagerRepository managerRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	private CandidatureRepository candidatureRepository;
	private AppUserRepository appUserRepository;
	private EmailSenderService emailSenderService;


	public EntrepriseService(EntrepriseRepository entrepriseRepository,
			EtapeRecrutementRepository etapeRecrutementRepository, DomaineService domaineService,
			EntrepriseValidator entrepriseValidator, AdresseService adresseService, ProjetService projetService,
			OffreService offreService, EcoleService ecoleService, CompetenceService competenceService,
			NiveauEtudeService niveauEtudeService, ManagerRepository managerRepository,
			BCryptPasswordEncoder bCryptPasswordEncoder,CandidatureRepository candidatureRepository,
			AppUserRepository appUserRepository,EmailSenderService emailSenderService) {

		this.entrepriseRepository = entrepriseRepository;
		this.etapeRecrutementRepository = etapeRecrutementRepository;
		this.domaineService = domaineService;
		this.entrepriseValidator = entrepriseValidator;
		this.adresseService = adresseService;
		this.projetService = projetService;
		this.offreService = offreService;
		this.ecoleService = ecoleService;
		this.competenceService = competenceService;
		this.niveauEtudeService = niveauEtudeService;
		this.managerRepository = managerRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.candidatureRepository = candidatureRepository;
		this.appUserRepository = appUserRepository;
		this.emailSenderService = emailSenderService;
	}

	public Entreprise getEntrepriseById(Long id) {
		return entrepriseRepository.getOne(id);
	}

	public Entreprise save(Entreprise entreprise) {
		return this.entrepriseRepository.save(entreprise);
	}

	public void updateGeneralities(Long idEntreprise, Entreprise entreprise) throws EmailExists {

		Entreprise entrepriseEnBase = getEntrepriseById(idEntreprise);

		if (entrepriseEnBase.getAdresse() == null) {
			Adresse newAdresse = adresseService.save(entreprise.getAdresse());
			entrepriseEnBase.setAdresse(newAdresse);
		} else if (!entrepriseEnBase.getAdresse().equals(entreprise.getAdresse())) {
			adresseService.delete(entrepriseEnBase.getAdresse());
			Adresse newAdresse = adresseService.save(entreprise.getAdresse());
			entrepriseEnBase.setAdresse(newAdresse);
		}
		entrepriseEnBase.setChiffreAffaire(entreprise.getChiffreAffaire());
		entrepriseEnBase.setDateCreation(entreprise.getDateCreation());
		if (entreprise.getDomaine() != null) {
			entrepriseEnBase.setDomaine(
					domaineService.recupererDomaineParIntituleSinonCreer(entreprise.getDomaine().getIntitule()));
		}

		if (entrepriseEnBase.getEmail() == null) {
			entrepriseValidator.validateEmailExistence(entreprise.getEmail());
			entrepriseEnBase.setEmail(entreprise.getEmail());
		} else if (!entrepriseEnBase.getEmail().equals(entreprise.getEmail())) {
			entrepriseValidator.validateEmailExistence(entreprise.getEmail());
			entrepriseEnBase.setEmail(entreprise.getEmail());
		}

		entrepriseEnBase.setLienSiteWeb(entreprise.getLienSiteWeb());
		entrepriseEnBase.setNom(entreprise.getNom());
		entrepriseEnBase.setNombreCollaborateur(entreprise.getNombreCollaborateur());
		entrepriseEnBase.setTelephone(entreprise.getTelephone());
		entrepriseRepository.save(entrepriseEnBase);
	}

	public Projet creerProjet(Projet projet, Long idEntreprise) {
		Entreprise entreprise = getEntrepriseById(idEntreprise);
		projet = projetService.save(projet);
		entreprise.getProjets().add(projet);
		entrepriseRepository.save(entreprise);
		return projet;

	}

	public Offre creerOffre(Offre offre, Long idEntreprise) {
		Entreprise entreprise = getEntrepriseById(idEntreprise);
		offre.setDatePublication(LocalDateTime.now());
		offre.setNomEntreprise(entreprise.getNom());
		offre.setImageEntreprise(entreprise.getImage());
		offre.setNomEntreprise(entreprise.getNom());
		offre.setIdEntreprise(entreprise.getId());
		String link = offre.getIntitule();
		link += new Date().toString();
		link =  bCryptPasswordEncoder.encode(link);
		link=link.replace("/", "-");
		offre.setLink(link);
		System.out.println(link);
		if (offre.getAdresseOffre() != null && offre.getAdresseOffre().getAdresse() != null) {
			Adresse adresse = adresseService.save(offre.getAdresseOffre());
			offre.setAdresseOffre(adresse);
		}

		if (offre.getEcolesConcernees() != null && offre.getEcolesConcernees().size() > 0) {
			List<Ecole> ecoles = offre.getEcolesConcernees().stream()
					.map(ecole -> this.ecoleService.getCompleteEcole(ecole)).collect(Collectors.toList());
			offre.setEcolesConcernees(ecoles);
		}

		if (offre.getCompetences() != null && offre.getCompetences().size() > 0) {
			
			List<Competence> competences = offre.getCompetences().stream()
					.map(competence -> this.competenceService.getCompetenceSiNonExistCreer(competence.getIntituleCompetence().getIntitule()))
					.collect(Collectors.toList());
			offre.setCompetences(competences);
		}
		if (offre.getNiveauxEtudes() != null && offre.getNiveauxEtudes().size() > 0) {
			List<NiveauEtude> niveauxEtudes = offre.getNiveauxEtudes().stream()
					.map(niveauEtude -> this.niveauEtudeService.getCompleteNiveauEtude(niveauEtude))
					.collect(Collectors.toList());
			System.out.println(niveauxEtudes);
			offre.setNiveauxEtudes(niveauxEtudes);
		}

		offre = offreService.save(offre);
		System.out.println(offre);
		entreprise.getOffres().add(offre);
		entrepriseRepository.save(entreprise);
		return offre;
	}

	public List<Offre> findAllOffre(Long idEntreprise) {
		if(this.managerRepository.existsById(idEntreprise)) {
			Manager manager = managerRepository.getOne(idEntreprise);
			idEntreprise = manager.getIdEntreprise();
			
		}
		return (List<Offre>) this.entrepriseRepository.getOne(idEntreprise).getOffres();
	}

	public List<Offre> findOffresByTypeAndIdEntreprise(Long idEntreprise, TypeOffre typeOffre) {
		Entreprise entreprise = this.entrepriseRepository.getOne(idEntreprise);
		return entreprise.getOffres().stream().filter(offre -> offre.getTypeOffre() == typeOffre)
				.collect(Collectors.toList());
	}

	public Page<Entreprise> searchEntreprises(String elementAchercher, Pageable pageable) {
		return this.entrepriseRepository
				.findByNomContainingIgnoreCaseOrDomaineIntituleContainingIgnoreCaseOrTypeRechercheNaturelContainingIgnoreCaseOrderByNomDesc(
						elementAchercher, elementAchercher, elementAchercher, pageable);
	}

	public EtapeRecrutement creerEtapeDeRecrutement(Long idEntreprise, String etapeRecrutement) {
		EtapeRecrutement newEtapeRecrutement = this.etapeRecrutementRepository
				.save(new EtapeRecrutement.Builder().withIntitule(etapeRecrutement).build());
		Entreprise entreprise = getEntrepriseById(idEntreprise);
		if (entreprise.getEtapeRecrutement() == null) {
			entreprise.setEtapeRecrutement(new ArrayList<>());
		}
		entreprise.getEtapeRecrutement().add(newEtapeRecrutement);
		entrepriseRepository.save(entreprise);
		return newEtapeRecrutement;
	}

	public EtapeRecrutement modifierEtapeDeRecrutement(Long idEtapeRecrutement, String intituleEtapeRecrutement) {
		EtapeRecrutement etapeRecrutement = this.etapeRecrutementRepository.getOne(idEtapeRecrutement);
		etapeRecrutement.setIntitule(intituleEtapeRecrutement);
		return etapeRecrutementRepository.save(etapeRecrutement);
	}

	public void supprimerEtapeDeRecrutement(Long idEtapeRecrutement) {
		this.etapeRecrutementRepository.deleteWithId(idEtapeRecrutement);
		this.etapeRecrutementRepository.deleteById(idEtapeRecrutement);
	}

	public List<EtapeRecrutement> getEtapesDeRecrutement(Long idEntrepriseOuManager) {
		if(managerRepository.existsById(idEntrepriseOuManager)) {
			Manager manager = managerRepository.getOne(idEntrepriseOuManager);
			Entreprise entreprise = getEntrepriseById(manager.getIdEntreprise());
			return entreprise.getEtapeRecrutement();
		}
		Entreprise entreprise = getEntrepriseById(idEntrepriseOuManager);
		return entreprise.getEtapeRecrutement();
	}

	public Manager ajouterManager(Long idEntreprise, Manager manager) {
		Entreprise entreprise = getEntrepriseById(idEntreprise);
		if(entreprise.getManagers() == null) {
			entreprise.setManagers(new ArrayList<>());
		}
		entreprise.getManagers().add(manager);
		entrepriseRepository.save(entreprise);
		return manager;
	}

	public Manager saveManager(Manager manager, Long idEntreprise) {
		manager.setIdEntreprise(idEntreprise);
		manager.setPassword(bCryptPasswordEncoder.encode(manager.getPassword()));
		return managerRepository.save(manager);
	}

	public void supprimerManager(Long idManager) {
		this.managerRepository.deleteWithId(idManager);
		this.managerRepository.deleteById(idManager);
	}

	public  Collection<Manager>  getManagers(Long idEntreprise) {
		return getEntrepriseById(idEntreprise).getManagers();
	}

	public void supprimerCandidatureSiDejaAssignee(Long idCandidature) {
		this.managerRepository.supprimerCandidatureSiDejaAssignee(idCandidature);
	}


	public void assignerCandidatureAuManager(Long idCandidature, Long idManager) {
		Candidature candidature = this.candidatureRepository.getOne(idCandidature);
//		Manager manager = managerRepository.getOne(idManager);
//		if(manager.getCandidatures()==null) {
//			manager.setCandidatures(new ArrayList());
//		}
//		if(candidature.getIdManager()==null) {
//			manager.getCandidatures().add(candidature);
//			managerRepository.assignerCandidatureAuManager(idCandidature, idManager);
//			candidature.setIdManager(idManager);
//			candidatureRepository.save(candidature);
//			System.out.println("ffffffffffffffffffffffff");
//		}else {
////			Manager managerActuel = managerRepository.getOne(candidature.getIdManager());
////			managerActuel.getCandidatures().removeIf(candidatureF->candidatureF.getId().equals(idCandidature));
////			managerRepository.save(managerActuel);
////			manager.getCandidatures().add(candidature);
//			supprimerCandidatureSiDejaAssignee(idCandidature);
//			managerRepository.assignerCandidatureAuManager(idCandidature, idManager);
//			candidature.setIdManager(manager.getId());
//			candidatureRepository.save(candidature);
//		}
		
		supprimerCandidatureSiDejaAssignee(idCandidature);
		candidature.setIdManager(idManager);
		candidatureRepository.save(candidature);
		managerRepository.assignerCandidatureAuManager(idCandidature, idManager);
		
	}

	public Entreprise getEntrepriseCorrespondantAOffre(Long idOffre) {
		return this.entrepriseRepository.getEntrepriseCorrespondantAOffre(idOffre);
	}

	public Manager getManager(Long idManager) {
		Manager manager = this.managerRepository.getOne(idManager);
		return manager;
	}

	public Offre fermerOffre(Long idEntreprise, Long idOffre) {
		Optional<Offre>  offreOpt = getEntrepriseById(idEntreprise).getOffres()
				.stream()
				.filter(offreF->offreF.getId()==idOffre)
				.findAny();
		if(offreOpt.isPresent()) {
			Offre offre=offreOpt.get();
			offre.setOuvert(false);
			return offre;
		}
		return null;
	}
	
	public Offre ouvrirOffre(Long idEntreprise, Long idOffre) {
		Optional<Offre>  offreOpt = getEntrepriseById(idEntreprise).getOffres()
				.stream()
				.filter(offreF->offreF.getId()==idOffre)
				.findAny();
		if(offreOpt.isPresent()) {
			Offre offre=offreOpt.get();
			offre.setOuvert(true);
			return offre;
		}
		return null;
	}

	public Candidature getCandidature(Long idCandidature) {
		return candidatureRepository.getOne(idCandidature);
	}

	public void follow(Long idEntreprise, Long idUser) {
		Entreprise entreprise =  getEntrepriseById(idEntreprise);
		AppUser appUser  = appUserRepository.getOne(idUser);
		if(entreprise.getFollowers()==null) {
			entreprise.setFollowers(new ArrayList<>());
		}
		entrepriseRepository.addToFollower(idEntreprise,idUser);
	}
	
	public void unFollow(Long idEntreprise, Long idUser) {
		Entreprise entreprise =  getEntrepriseById(idEntreprise);
		AppUser appUser  = appUserRepository.getOne(idUser);
		if(entreprise.getFollowers()==null) {
			entreprise.setFollowers(new ArrayList<>());
		}
		entreprise.getFollowers().removeIf(e-> e.getId().equals(appUser.getId()));
		entrepriseRepository.save(entreprise);
	}
	
	public boolean isFollowed(Long idEntreprise, Long idUser) {
		Entreprise entreprise =  getEntrepriseById(idEntreprise);
		for(AppUser user:entreprise.getFollowers()) {
			if(user.getId().equals(idUser)) {
				return true;
			}
		}
		return false;
	}

	public void shareWIthMyFollowers(Long idEntreprise, Long idOffer) {
		Offre offre= offreService.getByIdOffre(idOffer);
		Entreprise entreprise =  getEntrepriseById(idEntreprise);
		String emails= new String();
		for(AppUser user:entreprise.getFollowers()) {
			emails+=user.getUsername()+",";
		}
		emails=emails.substring(0,emails.length()-1);
		String[] listEmail = emails.split(",");
		if(listEmail.length>=1) {
			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setTo(listEmail);
			mailMessage.setSubject(offre.getNomEntreprise()+SUJET_NOUVEL_OFFRE+offre.getTypeOffre());
			mailMessage.setFrom(CITY_SKILL_MAIL);
			mailMessage.setText(MESSAGE_NOUVEL_OFFRE + BASE_URL_OF_FRONT+SPECIFIC_OFFRE+offre.getLink());
			emailSenderService.sendEmail(mailMessage);
			offre.setShared(true);
			this.offreService.save(offre); 
		}
	}
	
}