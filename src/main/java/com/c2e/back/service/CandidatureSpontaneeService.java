package com.c2e.back.service;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.c2e.back.dao.CandidatureSpontaneeRepository;
import com.c2e.back.entity.CandidatureSpontanee;
import com.c2e.back.entity.Etudiant;
import com.c2e.back.entity.Offre;

@Service
@Transactional
public class CandidatureSpontaneeService {

	private CandidatureSpontaneeRepository candidatureSpontaneeRepository;
	private EtudiantService etudiantService;
	private OffreService offreService;
	
	public CandidatureSpontaneeService(CandidatureSpontaneeRepository candidatureSpontaneeRepository,
			EtudiantService etudiantService,OffreService offreService) {
		this.candidatureSpontaneeRepository = candidatureSpontaneeRepository;
		this.etudiantService = etudiantService;
		this.offreService = offreService;
	}
	
	public CandidatureSpontanee getById(Long id) {
		return this.candidatureSpontaneeRepository.getOne(id);
	}

	public List<CandidatureSpontanee> findAll() {
		return candidatureSpontaneeRepository.findAll();
	}

	public CandidatureSpontanee creerCandidature(CandidatureSpontanee candidatureSpontanee, Long idOffre, Long idEtudiant) {
		
		Etudiant etudiant = etudiantService.getEtudiantById(idEtudiant);
		candidatureSpontanee.setDateCandidature(LocalDateTime.now());
		candidatureSpontanee.setEtudiant(etudiant);
		
		candidatureSpontaneeRepository.save(candidatureSpontanee);
		Offre offre = offreService.getByIdOffre(idOffre);
		offre.getCandidatures().add(candidatureSpontanee);
		offreService.save(offre);
		return candidatureSpontanee;
	}

	public CandidatureSpontanee getCandidatureById(Long idCandidature) {
		return candidatureSpontaneeRepository.getOne(idCandidature);
	}

	public void deleteCandidatureByIdEtudiant(Long idEtudiant) {
		// TODO Auto-generated method stub
		
	}
	
	
}
